import 'package:alzheimer_flutter/app/dto/user/logged_user_dto.dart';
import 'package:get_storage/get_storage.dart';

import '../../app/constants/urls.dart';

abstract class PrefKey {
  static const selectedUserDto = 'selected_user_dto';
  static const accessToken = "access_token";
  static const baseUrl = "base_url";
}

class AZPreference {
  final String? baseUrl;
  final getStorage = GetStorage();
  dynamic userDto;
  AZPreference({this.baseUrl}): super() {
    if(baseUrl != null) {
      getStorage.write(PrefKey.baseUrl, baseUrl);
    }
    userDto = getStorage.read(PrefKey.selectedUserDto);
  }
  
  int? get userId => userDto != null ? LoggedUserDTO.fromJson(userDto!).loggedUserProfile!.userId : null;
  String? get fullName => userDto !=null ? LoggedUserDTO.fromJson(userDto!).getFullName() : null;
  String? get phone => userDto !=null ? LoggedUserDTO.fromJson(userDto!).phone : null;
  String? get profilePicture => userDto != null ? LoggedUserDTO.fromJson(userDto!).loggedUserProfile!.getProfilePhotoUrl() : null;
  String? get token => getStorage.read(PrefKey.accessToken);
  String get getBaseUrl => getStorage.read(PrefKey.baseUrl)??Urls.baseUrl;
}
