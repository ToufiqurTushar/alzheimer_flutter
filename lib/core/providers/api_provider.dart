import 'dart:async';
import 'dart:convert';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import '../connect/connect.dart';

class ApiProvider extends Connect {
  var headers = <String, String>{};
  var response = Future.value(const Response(
    statusCode: 502,
  ));

  Future<Response> GET(String path, {
      Map<String, dynamic>? query,
      bool requireHeaderToken = false,
      bool requireBearerToken = false,
  }) {
      headers['token'] = '$requireHeaderToken';
      headers['Authorization'] = '$requireBearerToken';
      return  get(path, query: query, headers: headers);
  }

  Future<Response> POST(String path, dynamic data, {
        Map<String, dynamic>? query,
        bool requireHeaderToken = false,
        bool requireBearerToken = false,
   }) {
    headers['token'] = '$requireHeaderToken';
    headers['Authorization'] = '$requireBearerToken';
    return post(path, data, query: query, headers: headers);
  }

  Future<Response> PUT(String path, dynamic data, {
    Map<String, dynamic>? query,
    bool requireHeaderToken = false,
    bool requireBearerToken = false,
  }) {
    headers['token'] = '$requireHeaderToken';
    headers['Authorization'] = '$requireBearerToken';
    return put(path, data, query: query, headers: headers);
  }

  Future<Response> DELETE(String path, {
    Map<String, dynamic>? query,
    bool requireHeaderToken = false,
    bool requireBearerToken = false,
  }) {
    headers['token'] = '$requireHeaderToken';
    headers['Authorization'] = '$requireBearerToken';
    return delete(path, query: query, headers: headers);
  }

  Future<dynamic> getJsonFromAsset({required String fileName}) async{
     //await DefaultAssetBundle.of(Get.context!).loadString("assets/json/$fileName.json");
     String data = await rootBundle.loadString('assets/json/$fileName.json');
     return jsonDecode(data);
  }


}

