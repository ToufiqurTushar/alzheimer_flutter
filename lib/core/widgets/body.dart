import 'package:flutter/material.dart';
import 'package:get/get.dart';

Widget EmptyBody() {
  return Center(
      child:Text('az_no_data_found'.tr)
  );
}
Widget LoadingBody() {
  return const Center(
      child: CircularProgressIndicator()
  );
}

Widget ReloadBody(Function onPressedMethod) {
  return Center(
      child: OutlinedButton(child: Text('az_reload_page'.tr, style: const TextStyle(color: Colors.black),),
          onPressed: () {
            onPressedMethod();
          }
      )
  );
}