import 'dart:math';

import 'package:get/get.dart';

import '../../app/constants/constant.dart';

const english = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9','AM', 'PM', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec', OptionConstant.DONT_KNOW];
const bangla = ['০', '১', '২', '৩', '৪', '৫', '৬', '৭', '৮', '৯','এ.এম','পি.এম', 'জানুয়ারী', 'ফেব্রুয়ারী' , 'মার্চ', 'এপ্রিল', 'মে', 'জুন', 'জুলাই', 'আগস্ট', 'সেপ্টেম্বর', 'অক্টোবর', 'নভেম্বর', 'ডিসেম্বর', OptionConstant.DONT_KNOW_BN];

String engToBng(String input) {
  for (int i = 0; i < english.length; i++) {
    input = input.replaceAll(english[i], bangla[i]);
  }
  return input;
}
String bngToEng(String input) {
  for (int i = 0; i < english.length; i++) {
    input = input.replaceAll(bangla[i], english[i]);
  }

  return input;
}

String trNmuber(dynamic value) {
  if(value == null) return '';
  if(value.runtimeType == int || value.runtimeType == double) {
    value = value.toString();
  }
  return isLocaleBn() ? engToBng(value) : value;
}

bool isLocaleBn() {
  return Get.locale.toString() == 'bn_BD';
}

List<String>? shuffle(List<String>? items) {
  if(items == null) return null;
  var random = Random();
  for (var i = items.length - 1; i > 0; i--) {
    var n = random.nextInt(i + 1);
    var temp = items[i];
    items[i] = items[n];
    items[n] = temp;
  }
  return items;
}