import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DialogUtil {
  static void showOverlayLoading({bool barrierDismissible = false}) {
    Get.dialog(
      const Center(
        child: CircularProgressIndicator(),
      ),
      barrierDismissible: barrierDismissible,
    );
  }

  static void closeOverlayLoading() {
    //Get.back(closeOverlays: false);
    Get.until((_) => !Get.isDialogOpen!);
  }
}
