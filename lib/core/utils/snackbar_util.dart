import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SnackbarUtil {
  static void showSuccess({required String message}) {
    Get.closeAllSnackbars();
    Get.rawSnackbar(
      title: 'az_success'.tr,
      icon: const Icon(Icons.thumb_up, color: Colors.white),
      message: message,
      backgroundColor: Colors.green.shade600,
      duration: const Duration(seconds: 2),
      snackPosition: SnackPosition.TOP,
      animationDuration: const Duration(milliseconds: 100)
    );
  }

  static void showWarning({required String message}) {
    Get.closeAllSnackbars();
    Get.rawSnackbar(
      title: 'az_warning'.tr,
      icon: const Icon(Icons.warning, color: Colors.white),
      message: message,
      backgroundColor: Colors.orange.shade900,
      duration: const Duration(seconds: 2),
      snackPosition: SnackPosition.TOP,
      animationDuration: const Duration(milliseconds: 100)
    );
  }

  static void showError({required String message}) {
    Get.closeAllSnackbars();
    Get.rawSnackbar(
      title: 'az_error'.tr,
      icon: const Icon(Icons.error, color: Colors.white),
      message: message,
      backgroundColor: Colors.redAccent.shade700,
      duration: const Duration(seconds: 2),
      snackPosition: SnackPosition.TOP,
      animationDuration: const Duration(milliseconds: 100)
    );
  }

  static void showErrorToast({required String message}) {
    Get.closeAllSnackbars();
    Get.snackbar(
      'az_error'.tr,
      message,
      icon: const Icon(Icons.close, color: Colors.white),
      snackPosition: SnackPosition.TOP,
      backgroundColor: Colors.red,
      borderRadius: 20,
      margin: const EdgeInsets.all(15),
      colorText: Colors.white,
      duration: const Duration(seconds: 2),
      isDismissible: true,
      forwardAnimationCurve: Curves.easeOutBack,
    );
  }

  static void showToast({required String title, required String message}) {
    Get.closeAllSnackbars();
    Get.snackbar(
      title,
      message,
      icon: const Icon(Icons.close, color: Colors.white),
      snackPosition: SnackPosition.TOP,
      backgroundColor: Colors.green,
      borderRadius: 20,
      margin: const EdgeInsets.all(15),
      colorText: Colors.white,
      duration: const Duration(seconds: 2),
      isDismissible: true,
      forwardAnimationCurve: Curves.easeOutBack,
    );
  }
}
