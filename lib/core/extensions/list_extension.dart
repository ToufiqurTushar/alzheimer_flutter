import 'dart:math';

extension ListExtensionToShuffle on List {
  toShuffle() {
    var random = Random();
    for (var i = length - 1; i > 0; i--) {
      var n = random.nextInt(i + 1);
      var temp = [i];
      this[i] = [n];
      this[n] = temp;
    }
    return this;
  }
}