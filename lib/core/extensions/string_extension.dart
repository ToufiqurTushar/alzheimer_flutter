import 'package:flutter/material.dart';

extension StringExtensionLastChars on String {
  String lastChars(int n) => substring(length - n);
}

extension ColorExtensionToColor on String {
  toColor() {
    var hexColor = replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF$hexColor";
    }
    if (hexColor.length == 8) {
      return Color(int.parse("0x$hexColor"));
    }
  }
}