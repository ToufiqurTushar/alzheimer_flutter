import 'package:logger/logger.dart';

import '../utils/snackbar_util.dart';

class InternetFailedException implements Exception {
  final String message;

  InternetFailedException({
    this.message = 'Network Error !',
  }) {
    Logger().w(message);
    SnackbarUtil.showError(message: message);
  }

  @override
  String toString() => message;
}
