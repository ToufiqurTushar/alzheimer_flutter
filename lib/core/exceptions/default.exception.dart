import 'package:logger/logger.dart';

import '../utils/snackbar_util.dart';

class DefaultException implements Exception {
  final String message;
  final StackTrace? stackTrace;
  DefaultException({
    required this.message,
    this.stackTrace,
  }) {
    Logger().e(message);
    SnackbarUtil.showError(message: message);
  }

  @override
  String toString() => message;
}
