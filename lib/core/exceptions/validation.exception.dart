import 'package:logger/logger.dart';

import '../utils/snackbar_util.dart';

class ValidationException implements Exception {
  final String response;
  final StackTrace? stackTrace;
  ValidationException({
    required this.response,
    this.stackTrace,
  }) {
    Logger().e(response);
  }
}
