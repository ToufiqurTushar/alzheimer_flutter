import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:logger/logger.dart';

class PaginationController extends GetxController {
  final scrollController = ScrollController();

  int currentPageNo = 1;
  int lastPageNo = 1;
  int totalPage = 1;
  bool get isScrollToLast => scrollController.offset >= scrollController.position.maxScrollExtent && !scrollController.position.outOfRange;
  bool get canLoadMore => isScrollToLast && currentPageNo < lastPageNo;

  @override
  void onInit() {
    super.onInit();
    scrollController.addListener(scrollListener);
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}


  scrollListener(){
    if(canLoadMore){
      //currentPageNo = currentPageNo++;
      //Logger().i(currentPageNo);
    }
  }
}
