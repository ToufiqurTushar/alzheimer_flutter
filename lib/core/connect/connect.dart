import 'package:alzheimer_flutter/app/constants/urls.dart';
import 'package:get/get.dart';
import 'package:logger/logger.dart';
import 'request_interceptor.dart';
import 'response_interceptor.dart';

class Connect extends GetConnect {
  @override
  void onInit() {
    super.onInit();

    baseUrl = Urls.getBaseUrl();
    Logger().e('$baseUrl is set as baseurl');

    httpClient.timeout = const Duration(seconds: 30);
    httpClient.defaultContentType = "application/json";
    httpClient.addRequestModifier(requestInterceptor);
    httpClient.addResponseModifier(responseInterceptor);
  }
}


