import 'dart:async';
import 'package:alzheimer_flutter/core/exceptions/default.exception.dart';
import 'package:alzheimer_flutter/core/preference/preference.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:get/get.dart';
import 'package:get/get_connect/http/src/request/request.dart';
import 'package:get_storage/get_storage.dart';
import 'package:logger/logger.dart';



FutureOr<Request> requestInterceptor(request) async {
  final AZPreference pref = Get.find();
  final token = pref.token??'';
  if (token.isEmpty) {
    throw DefaultException(message: 'az_unauthorized_user'.tr);
  }

  final isRequiredToken =  request.headers['token'] == 'true';
  final isRequiredBearerToken =  request.headers['Authorization'] == 'true';

  if(isRequiredToken) {
    request.headers['token'] = token;
    request.headers.removeWhere((key, value) => key == 'Authorization');
  }
  else if(isRequiredBearerToken) {
    request.headers['Authorization'] =  'Bearer $token';
    request.headers.removeWhere((key, value) => key == 'token');
  }
  else {
    request.headers.removeWhere((key, value) => key == 'token');
    request.headers.removeWhere((key, value) => key == 'Authorization');
  }

  Logger().i(request.url.toString());

  var connectivityResult = await (Connectivity().checkConnectivity());
  if (connectivityResult == ConnectivityResult.mobile || connectivityResult == ConnectivityResult.wifi) {
    return request;
  } else {
    throw DefaultException(message: 'az_please_connect_to_wifi_or_mobile_network'.tr);
  }
}
