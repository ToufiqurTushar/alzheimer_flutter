import 'dart:async';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:get/get_connect/http/src/request/request.dart';
import 'package:logger/logger.dart';
import '../exceptions/default.exception.dart';
import '../exceptions/internal.exception.dart';
import '../exceptions/invalid_body.exception.dart';

FutureOr<dynamic> responseInterceptor(Request request, Response response) async {
  Logger().d(response.body);
  EasyLoading.dismiss();
  if (response.statusCode == 200) {
    return response;
  } else if(response.body == null) {
    throw InvalidBodyException(message: 'No data Found !');
  } else {
    handleErrorStatus(response);
  }

  return response;
}

dynamic handleErrorStatus(Response response) async {
  switch (response.statusCode) {
    case 400:
      return DefaultException(message: response.body);
    case 401:
      //logout
      return DefaultException(message: response.body);
    case 422:
      //validation
      return DefaultException(message: response.body);
    case 500:
      //bad server response
      return InternalException();
    default:
      return DefaultException(message: response.body);
  }

}
