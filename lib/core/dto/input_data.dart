import 'package:flutter/material.dart';
import 'package:get/get.dart';
class InputDataImage {
  final String imageUrl;
  final String id;
  InputDataImage({
    required this.imageUrl,
    required this.id,
  });
}

class InputDataOption {
  final dynamic id;
  late String name;
  final String? nameBn;
  InputDataOption({this.id = true, required this.name, this.nameBn});
}

class InputData {
  static const empty = '';
  static const text = 'text';
  static const number = 'number';
  static const date = 'date';
  static const age = 'age';
  static const radio = 'radio';
  static const checkbox = 'checkbox';
  static const select = 'az_select';
  static const selectSearch = 'selectSearch';
  static const multipleSelect = 'multipleSelect';
  static const file = 'file';
  static const image = 'image';
  static const audio = 'audio';
  static const multipleFile = 'multipleFile';
  static const hidden = 'hidden';
  static const view = 'view';
  static List<InputDataOption> yesNoOptions = [InputDataOption(id: true, name: 'az_yes'.tr), InputDataOption(id: false, name: 'no'.tr)];

  String key;
  String type;
  String? label;
  bool labelAtCenter;
  String? figure;
  String hint;
  IconData? icon;
  dynamic value;
  List<dynamic>? options;
  bool isEdit;
  bool isRequired;
  bool isReadOnly;
  bool isEmail;
  bool isVisible;
  bool isOutputText;

  InputData({this.key = 'key', this.type = text, this.label,this.labelAtCenter = false, this.figure, this.hint = empty, this.icon, this.value, this.options, this.isEdit = false, this.isRequired = true, this.isReadOnly = false, this.isEmail = false, this.isVisible = true, this.isOutputText = false});

}