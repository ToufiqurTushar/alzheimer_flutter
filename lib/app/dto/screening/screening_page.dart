import 'input_content.dart';

class ScreeningPage {
  ScreeningPage({
      this.page, 
      this.pageTitle, 
      this.inputContents,});

  int? page;
  String? pageTitle;
  List<InputContent>? inputContents;
}