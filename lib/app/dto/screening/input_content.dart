import '../../enum/Input_type_enum.dart';

class InputContent {
  InputContent({
      this.type, 
      this.key, 
      this.label, 
      this.figure, 
      this.hint = "",
      this.options,
      this.readOnly = false,
      this.hidden = false,
      this.labelAtCenter = false,
      this.answer,
      this.child,
      this.secondChild
  });

  InputTypeEnum? type;
  String? key;
  String? label;
  String? figure;
  String hint = "";
  List<String>? options;
  bool readOnly = false;
  bool hidden = false;
  bool labelAtCenter = false;
  dynamic answer;
  InputContent? child;
  InputContent? secondChild;
}