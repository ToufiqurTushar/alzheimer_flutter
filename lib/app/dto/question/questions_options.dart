class QuestionsAndOptions {
  QuestionsAndOptions({
      this.azs1orientationa, 
      this.azs1orientationb, 
      this.azs1orientationc, 
      this.azs1orientationd, 
      this.azs1orientatione, 
      this.azs1orientationf, 
      this.azs2attention, 
      this.azs3question, 
      this.azs3forward, 
      this.azs3backward, 
      this.azs4attention, 
      this.azs5memory1, 
      this.azs5memory2, 
      this.azs5memory3, 
      this.azs5memory4, 
      this.azs5memory5, 
      this.azs6memory1, 
      this.azs6memory2, 
      this.azs6memory3, 
      this.azs6memory4, 
      this.azs6memory5, 
      this.azs6memory6, 
      this.azs6memory7, 
      this.azs6memory8, 
      this.azs6memory9, 
      this.azs6memory10, 
      this.azs7attention1, 
      this.azs7attention2, 
      this.azs7attention3, 
      this.azs8naming1, 
      this.azs8naming2, 
      this.azs8naming3, 
      this.azs8naming4, 
      this.azs8naming5, 
      this.azs8naming6, 
      this.azs8naming7, 
      this.azs8naming8, 
      this.azs8naming9, 
      this.azs8naming10, 
      this.azs8naming11, 
      this.azs8naming12, 
      this.azs9naming1, 
      this.azs9naming2, 
      this.azs9naming3, 
      this.azs9naming4, 
      this.azs9naming5, 
      this.azs9naming6, 
      this.azs9naming7, 
      this.azs9naming8, 
      this.azs9naming9, 
      this.azs9naming10, 
      this.azs9naming11, 
      this.azs9naming12, 
      this.azs9naming13, 
      this.azs9naming14, 
      this.azs9naming15, 
      this.azs9naming16, 
      this.azs9naming17, 
      this.azs9naming18, 
      this.azs9naming19, 
      this.azs9naming20, 
      this.azs9naming21, 
      this.azs10language1a, 
      this.azs10language1b, 
      this.azs10language2a, 
      this.azs10language2b, 
      this.azs10language3a, 
      this.azs10language3b, 
      this.azs10language4a, 
      this.azs10language4b, 
      this.azs11language1, 
      this.azs11language2, 
      this.azs11language3, 
      this.azs11language4, 
      this.azs11language5, 
      this.azs12language1, 
      this.azs12language2, 
      this.azs12language3, 
      this.azs12language4, 
      this.azs12language5, 
      this.azs12language6, 
      this.azs12language7, 
      this.azs12language8, 
      this.azs12language9, 
      this.azs12language10, 
      this.azs12language11, 
      this.azs13fluencya1, 
      this.azs13fluencya2, 
      this.azs13fluencya3, 
      this.azs13fluencya4, 
      this.azs13fluencya5, 
      this.azs13fluencya6, 
      this.azs13fluencya7, 
      this.azs13fluencya8, 
      this.azs13fluencya9, 
      this.azs13fluencya10, 
      this.azs13fluencya11, 
      this.azs13fluencya12, 
      this.azs13fluencya13, 
      this.azs13fluencya14, 
      this.azs13fluencyb, 
      this.azs14fluency, 
      this.azs15visuospatial1, 
      this.azs15visuospatial2, 
      this.azs16visuospatial1, 
      this.azs16visuospatial2, 
      this.azs16visuospatial3, 
      this.azs16visuospatial4, 
      this.azs16visuospatial5, 
      this.azs16visuospatial6, 
      this.azs16visuospatial7, 
      this.azs16visuospatial8, 
      this.azs16visuospatial9, 
      this.azs16visuospatial10, 
      this.azs16visuospatial11,});

  QuestionsAndOptions.fromJson(dynamic json) {
    azs1orientationa = json['AZ_S1_ORIENTATION_A'] != null ? json['AZ_S1_ORIENTATION_A'].cast<String>() : [];
    azs1orientationb = json['AZ_S1_ORIENTATION_B'] != null ? json['AZ_S1_ORIENTATION_B'].cast<String>() : [];
    azs1orientationc = json['AZ_S1_ORIENTATION_C'] != null ? json['AZ_S1_ORIENTATION_C'].cast<String>() : [];
    azs1orientationd = json['AZ_S1_ORIENTATION_D'] != null ? json['AZ_S1_ORIENTATION_D'].cast<String>() : [];
    azs1orientatione = json['AZ_S1_ORIENTATION_E'] != null ? json['AZ_S1_ORIENTATION_E'].cast<String>() : [];
    azs1orientationf = json['AZ_S1_ORIENTATION_F'] != null ? json['AZ_S1_ORIENTATION_F'].cast<String>() : [];
    azs2attention = json['AZ_S2_ATTENTION'] != null ? json['AZ_S2_ATTENTION'].cast<String>() : [];
    azs3question = json['AZ_S3_QUESTION'] != null ? json['AZ_S3_QUESTION'].cast<String>() : [];
    azs3forward = json['AZ_S3_FORWARD'] != null ? json['AZ_S3_FORWARD'].cast<String>() : [];
    azs3backward = json['AZ_S3_BACKWARD'] != null ? json['AZ_S3_BACKWARD'].cast<String>() : [];
    azs4attention = json['AZ_S4_ATTENTION'] != null ? json['AZ_S4_ATTENTION'].cast<String>() : [];
    azs5memory1 = json['AZ_S5_MEMORY_1'] != null ? json['AZ_S5_MEMORY_1'].cast<String>() : [];
    azs5memory2 = json['AZ_S5_MEMORY_2'] != null ? json['AZ_S5_MEMORY_2'].cast<String>() : [];
    azs5memory3 = json['AZ_S5_MEMORY_3'] != null ? json['AZ_S5_MEMORY_3'].cast<String>() : [];
    azs5memory4 = json['AZ_S5_MEMORY_4'] != null ? json['AZ_S5_MEMORY_4'].cast<String>() : [];
    azs5memory5 = json['AZ_S5_MEMORY_5'] != null ? json['AZ_S5_MEMORY_5'].cast<String>() : [];
    azs6memory1 = json['AZ_S6_MEMORY_1'] != null ? json['AZ_S6_MEMORY_1'].cast<String>() : [];
    azs6memory2 = json['AZ_S6_MEMORY_2'] != null ? json['AZ_S6_MEMORY_2'].cast<String>() : [];
    azs6memory3 = json['AZ_S6_MEMORY_3'] != null ? json['AZ_S6_MEMORY_3'].cast<String>() : [];
    azs6memory4 = json['AZ_S6_MEMORY_4'] != null ? json['AZ_S6_MEMORY_4'].cast<String>() : [];
    azs6memory5 = json['AZ_S6_MEMORY_5'] != null ? json['AZ_S6_MEMORY_5'].cast<String>() : [];
    azs6memory6 = json['AZ_S6_MEMORY_6'] != null ? json['AZ_S6_MEMORY_6'].cast<String>() : [];
    azs6memory7 = json['AZ_S6_MEMORY_7'] != null ? json['AZ_S6_MEMORY_7'].cast<String>() : [];
    azs6memory8 = json['AZ_S6_MEMORY_8'] != null ? json['AZ_S6_MEMORY_8'].cast<String>() : [];
    azs6memory9 = json['AZ_S6_MEMORY_9'] != null ? json['AZ_S6_MEMORY_9'].cast<String>() : [];
    azs6memory10 = json['AZ_S6_MEMORY_10'] != null ? json['AZ_S6_MEMORY_10'].cast<String>() : [];
    azs7attention1 = json['AZ_S7_ATTENTION_1'] != null ? json['AZ_S7_ATTENTION_1'].cast<String>() : [];
    azs7attention2 = json['AZ_S7_ATTENTION_2'] != null ? json['AZ_S7_ATTENTION_2'].cast<String>() : [];
    azs7attention3 = json['AZ_S7_ATTENTION_3'] != null ? json['AZ_S7_ATTENTION_3'].cast<String>() : [];
    azs8naming1 = json['AZ_S8_NAMING_1'] != null ? json['AZ_S8_NAMING_1'].cast<String>() : [];
    azs8naming2 = json['AZ_S8_NAMING_2'] != null ? json['AZ_S8_NAMING_2'].cast<String>() : [];
    azs8naming3 = json['AZ_S8_NAMING_3'] != null ? json['AZ_S8_NAMING_3'].cast<String>() : [];
    azs8naming4 = json['AZ_S8_NAMING_4'] != null ? json['AZ_S8_NAMING_4'].cast<String>() : [];
    azs8naming5 = json['AZ_S8_NAMING_5'] != null ? json['AZ_S8_NAMING_5'].cast<String>() : [];
    azs8naming6 = json['AZ_S8_NAMING_6'] != null ? json['AZ_S8_NAMING_6'].cast<String>() : [];
    azs8naming7 = json['AZ_S8_NAMING_7'] != null ? json['AZ_S8_NAMING_7'].cast<String>() : [];
    azs8naming8 = json['AZ_S8_NAMING_8'] != null ? json['AZ_S8_NAMING_8'].cast<String>() : [];
    azs8naming9 = json['AZ_S8_NAMING_9'] != null ? json['AZ_S8_NAMING_9'].cast<String>() : [];
    azs8naming10 = json['AZ_S8_NAMING_10'] != null ? json['AZ_S8_NAMING_10'].cast<String>() : [];
    azs8naming11 = json['AZ_S8_NAMING_11'] != null ? json['AZ_S8_NAMING_11'].cast<String>() : [];
    azs8naming12 = json['AZ_S8_NAMING_12'] != null ? json['AZ_S8_NAMING_12'].cast<String>() : [];
    azs9naming1 = json['AZ_S9_NAMING_1'] != null ? json['AZ_S9_NAMING_1'].cast<String>() : [];
    azs9naming2 = json['AZ_S9_NAMING_2'] != null ? json['AZ_S9_NAMING_2'].cast<String>() : [];
    azs9naming3 = json['AZ_S9_NAMING_3'] != null ? json['AZ_S9_NAMING_3'].cast<String>() : [];
    azs9naming4 = json['AZ_S9_NAMING_4'] != null ? json['AZ_S9_NAMING_4'].cast<String>() : [];
    azs9naming5 = json['AZ_S9_NAMING_5'] != null ? json['AZ_S9_NAMING_5'].cast<String>() : [];
    azs9naming6 = json['AZ_S9_NAMING_6'] != null ? json['AZ_S9_NAMING_6'].cast<String>() : [];
    azs9naming7 = json['AZ_S9_NAMING_7'] != null ? json['AZ_S9_NAMING_7'].cast<String>() : [];
    azs9naming8 = json['AZ_S9_NAMING_8'] != null ? json['AZ_S9_NAMING_8'].cast<String>() : [];
    azs9naming9 = json['AZ_S9_NAMING_9'] != null ? json['AZ_S9_NAMING_9'].cast<String>() : [];
    azs9naming10 = json['AZ_S9_NAMING_10'] != null ? json['AZ_S9_NAMING_10'].cast<String>() : [];
    azs9naming11 = json['AZ_S9_NAMING_11'] != null ? json['AZ_S9_NAMING_11'].cast<String>() : [];
    azs9naming12 = json['AZ_S9_NAMING_12'] != null ? json['AZ_S9_NAMING_12'].cast<String>() : [];
    azs9naming13 = json['AZ_S9_NAMING_13'] != null ? json['AZ_S9_NAMING_13'].cast<String>() : [];
    azs9naming14 = json['AZ_S9_NAMING_14'] != null ? json['AZ_S9_NAMING_14'].cast<String>() : [];
    azs9naming15 = json['AZ_S9_NAMING_15'] != null ? json['AZ_S9_NAMING_15'].cast<String>() : [];
    azs9naming16 = json['AZ_S9_NAMING_16'] != null ? json['AZ_S9_NAMING_16'].cast<String>() : [];
    azs9naming17 = json['AZ_S9_NAMING_17'] != null ? json['AZ_S9_NAMING_17'].cast<String>() : [];
    azs9naming18 = json['AZ_S9_NAMING_18'] != null ? json['AZ_S9_NAMING_18'].cast<String>() : [];
    azs9naming19 = json['AZ_S9_NAMING_19'] != null ? json['AZ_S9_NAMING_19'].cast<String>() : [];
    azs9naming20 = json['AZ_S9_NAMING_20'] != null ? json['AZ_S9_NAMING_20'].cast<String>() : [];
    azs9naming21 = json['AZ_S9_NAMING_21'] != null ? json['AZ_S9_NAMING_21'].cast<String>() : [];
    azs10language1a = json['AZ_S10_LANGUAGE_1_A'] != null ? json['AZ_S10_LANGUAGE_1_A'].cast<String>() : [];
    azs10language1b = json['AZ_S10_LANGUAGE_1_B'] != null ? json['AZ_S10_LANGUAGE_1_B'].cast<String>() : [];
    azs10language2a = json['AZ_S10_LANGUAGE_2_A'] != null ? json['AZ_S10_LANGUAGE_2_A'].cast<String>() : [];
    azs10language2b = json['AZ_S10_LANGUAGE_2_B'] != null ? json['AZ_S10_LANGUAGE_2_B'].cast<String>() : [];
    azs10language3a = json['AZ_S10_LANGUAGE_3_A'] != null ? json['AZ_S10_LANGUAGE_3_A'].cast<String>() : [];
    azs10language3b = json['AZ_S10_LANGUAGE_3_B'] != null ? json['AZ_S10_LANGUAGE_3_B'].cast<String>() : [];
    azs10language4a = json['AZ_S10_LANGUAGE_4_A'] != null ? json['AZ_S10_LANGUAGE_4_A'].cast<String>() : [];
    azs10language4b = json['AZ_S10_LANGUAGE_4_B'] != null ? json['AZ_S10_LANGUAGE_4_B'].cast<String>() : [];
    azs11language1 = json['AZ_S11_LANGUAGE_1'] != null ? json['AZ_S11_LANGUAGE_1'].cast<String>() : [];
    azs11language2 = json['AZ_S11_LANGUAGE_2'] != null ? json['AZ_S11_LANGUAGE_2'].cast<String>() : [];
    azs11language3 = json['AZ_S11_LANGUAGE_3'] != null ? json['AZ_S11_LANGUAGE_3'].cast<String>() : [];
    azs11language4 = json['AZ_S11_LANGUAGE_4'] != null ? json['AZ_S11_LANGUAGE_4'].cast<String>() : [];
    azs11language5 = json['AZ_S11_LANGUAGE_5'] != null ? json['AZ_S11_LANGUAGE_5'].cast<String>() : [];
    azs12language1 = json.containsKey('AZ_S12_LANGUAGE_1') && json['AZ_S12_LANGUAGE_1'] != null ? json['AZ_S12_LANGUAGE_1'].cast<String>()  : json.containsKey('AZ_S12_LANGUAGE_1') ? [] : null;
    azs12language2 = json.containsKey('AZ_S12_LANGUAGE_2') && json['AZ_S12_LANGUAGE_2'] != null ? json['AZ_S12_LANGUAGE_2'].cast<String>()  : json.containsKey('AZ_S12_LANGUAGE_2') ? [] : null;
    azs12language3 = json.containsKey('AZ_S12_LANGUAGE_3') && json['AZ_S12_LANGUAGE_3'] != null ? json['AZ_S12_LANGUAGE_3'].cast<String>()  : json.containsKey('AZ_S12_LANGUAGE_3') ? [] : null;
    azs12language4 = json.containsKey('AZ_S12_LANGUAGE_4') && json['AZ_S12_LANGUAGE_4'] != null ? json['AZ_S12_LANGUAGE_4'].cast<String>()  : json.containsKey('AZ_S12_LANGUAGE_4') ? [] : null;
    azs12language5 = json.containsKey('AZ_S12_LANGUAGE_5') && json['AZ_S12_LANGUAGE_5'] != null ? json['AZ_S12_LANGUAGE_5'].cast<String>()  : json.containsKey('AZ_S12_LANGUAGE_5') ? [] : null;
    azs12language6 = json.containsKey('AZ_S12_LANGUAGE_6') && json['AZ_S12_LANGUAGE_6'] != null ? json['AZ_S12_LANGUAGE_6'].cast<String>()  : json.containsKey('AZ_S12_LANGUAGE_6') ? [] : null;
    azs12language7 = json.containsKey('AZ_S12_LANGUAGE_7') && json['AZ_S12_LANGUAGE_7'] != null ? json['AZ_S12_LANGUAGE_7'].cast<String>()  : json.containsKey('AZ_S12_LANGUAGE_7') ? [] : null;
    azs12language8 = json.containsKey('AZ_S12_LANGUAGE_8') && json['AZ_S12_LANGUAGE_8'] != null ? json['AZ_S12_LANGUAGE_8'].cast<String>()  : json.containsKey('AZ_S12_LANGUAGE_8') ? [] : null;
    azs12language9 = json.containsKey('AZ_S12_LANGUAGE_9') && json['AZ_S12_LANGUAGE_9'] != null ? json['AZ_S12_LANGUAGE_9'].cast<String>()  : json.containsKey('AZ_S12_LANGUAGE_9') ? [] : null;
    azs12language10 = json.containsKey('AZ_S12_LANGUAGE_10') && json['AZ_S12_LANGUAGE_10'] != null ? json['AZ_S12_LANGUAGE_10'].cast<String>()  : json.containsKey('AZ_S12_LANGUAGE_10') ? [] : null;
    azs12language11 = json.containsKey('AZ_S12_LANGUAGE_11') && json['AZ_S12_LANGUAGE_11'] != null ? json['AZ_S12_LANGUAGE_11'].cast<String>()  : json.containsKey('AZ_S12_LANGUAGE_11') ? [] : null;
    azs13fluencya1 = json['AZ_S13_FLUENCY_A_1'] != null ? json['AZ_S13_FLUENCY_A_1'].cast<String>() : [];
    azs13fluencya2 = json['AZ_S13_FLUENCY_A_2'] != null ? json['AZ_S13_FLUENCY_A_2'].cast<String>() : [];
    azs13fluencya3 = json['AZ_S13_FLUENCY_A_3'] != null ? json['AZ_S13_FLUENCY_A_3'].cast<String>() : [];
    azs13fluencya4 = json['AZ_S13_FLUENCY_A_4'] != null ? json['AZ_S13_FLUENCY_A_4'].cast<String>() : [];
    azs13fluencya5 = json['AZ_S13_FLUENCY_A_5'] != null ? json['AZ_S13_FLUENCY_A_5'].cast<String>() : [];
    azs13fluencya6 = json['AZ_S13_FLUENCY_A_6'] != null ? json['AZ_S13_FLUENCY_A_6'].cast<String>() : [];
    azs13fluencya7 = json['AZ_S13_FLUENCY_A_7'] != null ? json['AZ_S13_FLUENCY_A_7'].cast<String>() : [];
    azs13fluencya8 = json['AZ_S13_FLUENCY_A_8'] != null ? json['AZ_S13_FLUENCY_A_8'].cast<String>() : [];
    azs13fluencya9 = json['AZ_S13_FLUENCY_A_9'] != null ? json['AZ_S13_FLUENCY_A_9'].cast<String>() : [];
    azs13fluencya10 = json['AZ_S13_FLUENCY_A_10'] != null ? json['AZ_S13_FLUENCY_A_10'].cast<String>() : [];
    azs13fluencya11 = json['AZ_S13_FLUENCY_A_11'] != null ? json['AZ_S13_FLUENCY_A_11'].cast<String>() : [];
    azs13fluencya12 = json['AZ_S13_FLUENCY_A_12'] != null ? json['AZ_S13_FLUENCY_A_12'].cast<String>() : [];
    azs13fluencya13 = json['AZ_S13_FLUENCY_A_13'] != null ? json['AZ_S13_FLUENCY_A_13'].cast<String>() : [];
    azs13fluencya14 = json['AZ_S13_FLUENCY_A_14'] != null ? json['AZ_S13_FLUENCY_A_14'].cast<String>() : [];
    azs13fluencyb = json['AZ_S13_FLUENCY_B'] != null ? json['AZ_S13_FLUENCY_B'].cast<String>() : [];
    azs14fluency = json['AZ_S14_FLUENCY'] != null ? json['AZ_S14_FLUENCY'].cast<String>() : [];
    azs15visuospatial1 = json['AZ_S15_VISUOSPATIAL_1'] != null ? json['AZ_S15_VISUOSPATIAL_1'].cast<String>() : [];
    azs15visuospatial2 = json['AZ_S15_VISUOSPATIAL_2'] != null ? json['AZ_S15_VISUOSPATIAL_2'].cast<String>() : [];
    azs16visuospatial1 = json['AZ_S16_VISUOSPATIAL_1'] != null ? json['AZ_S16_VISUOSPATIAL_1'].cast<String>() : [];
    azs16visuospatial2 = json['AZ_S16_VISUOSPATIAL_2'] != null ? json['AZ_S16_VISUOSPATIAL_2'].cast<String>() : [];
    azs16visuospatial3 = json['AZ_S16_VISUOSPATIAL_3'] != null ? json['AZ_S16_VISUOSPATIAL_3'].cast<String>() : [];
    azs16visuospatial4 = json['AZ_S16_VISUOSPATIAL_4'] != null ? json['AZ_S16_VISUOSPATIAL_4'].cast<String>() : [];
    azs16visuospatial5 = json['AZ_S16_VISUOSPATIAL_5'] != null ? json['AZ_S16_VISUOSPATIAL_5'].cast<String>() : [];
    azs16visuospatial6 = json['AZ_S16_VISUOSPATIAL_6'] != null ? json['AZ_S16_VISUOSPATIAL_6'].cast<String>() : [];
    azs16visuospatial7 = json['AZ_S16_VISUOSPATIAL_7'] != null ? json['AZ_S16_VISUOSPATIAL_7'].cast<String>() : [];
    azs16visuospatial8 = json['AZ_S16_VISUOSPATIAL_8'] != null ? json['AZ_S16_VISUOSPATIAL_8'].cast<String>() : [];
    azs16visuospatial9 = json['AZ_S16_VISUOSPATIAL_9'] != null ? json['AZ_S16_VISUOSPATIAL_9'].cast<String>() : [];
    azs16visuospatial10 = json['AZ_S16_VISUOSPATIAL_10'] != null ? json['AZ_S16_VISUOSPATIAL_10'].cast<String>() : [];
    azs16visuospatial11 = json['AZ_S16_VISUOSPATIAL_11'] != null ? json['AZ_S16_VISUOSPATIAL_11'].cast<String>() : [];
  }
  List<String>? azs1orientationa;
  List<String>? azs1orientationb;
  List<String>? azs1orientationc;
  List<String>? azs1orientationd;
  List<String>? azs1orientatione;
  List<String>? azs1orientationf;
  List<String>? azs2attention;
  List<String>? azs3question;
  List<String>? azs3forward;
  List<String>? azs3backward;
  List<String>? azs4attention;
  List<String>? azs5memory1;
  List<String>? azs5memory2;
  List<String>? azs5memory3;
  List<String>? azs5memory4;
  List<String>? azs5memory5;
  List<String>? azs6memory1;
  List<String>? azs6memory2;
  List<String>? azs6memory3;
  List<String>? azs6memory4;
  List<String>? azs6memory5;
  List<String>? azs6memory6;
  List<String>? azs6memory7;
  List<String>? azs6memory8;
  List<String>? azs6memory9;
  List<String>? azs6memory10;
  List<String>? azs7attention1;
  List<String>? azs7attention2;
  List<String>? azs7attention3;
  List<String>? azs8naming1;
  List<String>? azs8naming2;
  List<String>? azs8naming3;
  List<String>? azs8naming4;
  List<String>? azs8naming5;
  List<String>? azs8naming6;
  List<String>? azs8naming7;
  List<String>? azs8naming8;
  List<String>? azs8naming9;
  List<String>? azs8naming10;
  List<String>? azs8naming11;
  List<String>? azs8naming12;
  List<String>? azs9naming1;
  List<String>? azs9naming2;
  List<String>? azs9naming3;
  List<String>? azs9naming4;
  List<String>? azs9naming5;
  List<String>? azs9naming6;
  List<String>? azs9naming7;
  List<String>? azs9naming8;
  List<String>? azs9naming9;
  List<String>? azs9naming10;
  List<String>? azs9naming11;
  List<String>? azs9naming12;
  List<String>? azs9naming13;
  List<String>? azs9naming14;
  List<String>? azs9naming15;
  List<String>? azs9naming16;
  List<String>? azs9naming17;
  List<String>? azs9naming18;
  List<String>? azs9naming19;
  List<String>? azs9naming20;
  List<String>? azs9naming21;
  List<String>? azs10language1a;
  List<String>? azs10language1b;
  List<String>? azs10language2a;
  List<String>? azs10language2b;
  List<String>? azs10language3a;
  List<String>? azs10language3b;
  List<String>? azs10language4a;
  List<String>? azs10language4b;
  List<String>? azs11language1;
  List<String>? azs11language2;
  List<String>? azs11language3;
  List<String>? azs11language4;
  List<String>? azs11language5;
  List<String>? azs12language1;
  List<String>? azs12language2;
  List<String>? azs12language3;
  List<String>? azs12language4;
  List<String>? azs12language5;
  List<String>? azs12language6;
  List<String>? azs12language7;
  List<String>? azs12language8;
  List<String>? azs12language9;
  List<String>? azs12language10;
  List<String>? azs12language11;
  List<String>? azs13fluencya1;
  List<String>? azs13fluencya2;
  List<String>? azs13fluencya3;
  List<String>? azs13fluencya4;
  List<String>? azs13fluencya5;
  List<String>? azs13fluencya6;
  List<String>? azs13fluencya7;
  List<String>? azs13fluencya8;
  List<String>? azs13fluencya9;
  List<String>? azs13fluencya10;
  List<String>? azs13fluencya11;
  List<String>? azs13fluencya12;
  List<String>? azs13fluencya13;
  List<String>? azs13fluencya14;
  List<String>? azs13fluencyb;
  List<String>? azs14fluency;
  List<String>? azs15visuospatial1;
  List<String>? azs15visuospatial2;
  List<String>? azs16visuospatial1;
  List<String>? azs16visuospatial2;
  List<String>? azs16visuospatial3;
  List<String>? azs16visuospatial4;
  List<String>? azs16visuospatial5;
  List<String>? azs16visuospatial6;
  List<String>? azs16visuospatial7;
  List<String>? azs16visuospatial8;
  List<String>? azs16visuospatial9;
  List<String>? azs16visuospatial10;
  List<String>? azs16visuospatial11;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['AZ_S1_ORIENTATION_A'] = azs1orientationa;
    map['AZ_S1_ORIENTATION_B'] = azs1orientationb;
    map['AZ_S1_ORIENTATION_C'] = azs1orientationc;
    map['AZ_S1_ORIENTATION_D'] = azs1orientationd;
    map['AZ_S1_ORIENTATION_E'] = azs1orientatione;
    map['AZ_S1_ORIENTATION_F'] = azs1orientationf;
    map['AZ_S2_ATTENTION'] = azs2attention;
    map['AZ_S3_QUESTION'] = azs3question;
    map['AZ_S3_FORWARD'] = azs3forward;
    map['AZ_S3_BACKWARD'] = azs3backward;
    map['AZ_S4_ATTENTION'] = azs4attention;
    map['AZ_S5_MEMORY_1'] = azs5memory1;
    map['AZ_S5_MEMORY_2'] = azs5memory2;
    map['AZ_S5_MEMORY_3'] = azs5memory3;
    map['AZ_S5_MEMORY_4'] = azs5memory4;
    map['AZ_S5_MEMORY_5'] = azs5memory5;
    map['AZ_S6_MEMORY_1'] = azs6memory1;
    map['AZ_S6_MEMORY_2'] = azs6memory2;
    map['AZ_S6_MEMORY_3'] = azs6memory3;
    map['AZ_S6_MEMORY_4'] = azs6memory4;
    map['AZ_S6_MEMORY_5'] = azs6memory5;
    map['AZ_S6_MEMORY_6'] = azs6memory6;
    map['AZ_S6_MEMORY_7'] = azs6memory7;
    map['AZ_S6_MEMORY_8'] = azs6memory8;
    map['AZ_S6_MEMORY_9'] = azs6memory9;
    map['AZ_S6_MEMORY_10'] = azs6memory10;
    map['AZ_S7_ATTENTION_1'] = azs7attention1;
    map['AZ_S7_ATTENTION_2'] = azs7attention2;
    map['AZ_S7_ATTENTION_3'] = azs7attention3;
    map['AZ_S8_NAMING_1'] = azs8naming1;
    map['AZ_S8_NAMING_2'] = azs8naming2;
    map['AZ_S8_NAMING_3'] = azs8naming3;
    map['AZ_S8_NAMING_4'] = azs8naming4;
    map['AZ_S8_NAMING_5'] = azs8naming5;
    map['AZ_S8_NAMING_6'] = azs8naming6;
    map['AZ_S8_NAMING_7'] = azs8naming7;
    map['AZ_S8_NAMING_8'] = azs8naming8;
    map['AZ_S8_NAMING_9'] = azs8naming9;
    map['AZ_S8_NAMING_10'] = azs8naming10;
    map['AZ_S8_NAMING_11'] = azs8naming11;
    map['AZ_S8_NAMING_12'] = azs8naming12;
    map['AZ_S9_NAMING_1'] = azs9naming1;
    map['AZ_S9_NAMING_2'] = azs9naming2;
    map['AZ_S9_NAMING_3'] = azs9naming3;
    map['AZ_S9_NAMING_4'] = azs9naming4;
    map['AZ_S9_NAMING_5'] = azs9naming5;
    map['AZ_S9_NAMING_6'] = azs9naming6;
    map['AZ_S9_NAMING_7'] = azs9naming7;
    map['AZ_S9_NAMING_8'] = azs9naming8;
    map['AZ_S9_NAMING_9'] = azs9naming9;
    map['AZ_S9_NAMING_10'] = azs9naming10;
    map['AZ_S9_NAMING_11'] = azs9naming11;
    map['AZ_S9_NAMING_12'] = azs9naming12;
    map['AZ_S9_NAMING_13'] = azs9naming13;
    map['AZ_S9_NAMING_14'] = azs9naming14;
    map['AZ_S9_NAMING_15'] = azs9naming15;
    map['AZ_S9_NAMING_16'] = azs9naming16;
    map['AZ_S9_NAMING_17'] = azs9naming17;
    map['AZ_S9_NAMING_18'] = azs9naming18;
    map['AZ_S9_NAMING_19'] = azs9naming19;
    map['AZ_S9_NAMING_20'] = azs9naming20;
    map['AZ_S9_NAMING_21'] = azs9naming21;
    map['AZ_S10_LANGUAGE_1_A'] = azs10language1a;
    map['AZ_S10_LANGUAGE_1_B'] = azs10language1b;
    map['AZ_S10_LANGUAGE_2_A'] = azs10language2a;
    map['AZ_S10_LANGUAGE_2_B'] = azs10language2b;
    map['AZ_S10_LANGUAGE_3_A'] = azs10language3a;
    map['AZ_S10_LANGUAGE_3_B'] = azs10language3b;
    map['AZ_S10_LANGUAGE_4_A'] = azs10language4a;
    map['AZ_S10_LANGUAGE_4_B'] = azs10language4b;
    map['AZ_S11_LANGUAGE_1'] = azs11language1;
    map['AZ_S11_LANGUAGE_2'] = azs11language2;
    map['AZ_S11_LANGUAGE_3'] = azs11language3;
    map['AZ_S11_LANGUAGE_4'] = azs11language4;
    map['AZ_S11_LANGUAGE_5'] = azs11language5;
    if(azs12language1 != null) map['AZ_S12_LANGUAGE_1'] = azs12language1;
    if(azs12language2 != null) map['AZ_S12_LANGUAGE_2'] = azs12language2;
    if(azs12language3 != null) map['AZ_S12_LANGUAGE_3'] = azs12language3;
    if(azs12language4 != null) map['AZ_S12_LANGUAGE_4'] = azs12language4;
    if(azs12language5 != null) map['AZ_S12_LANGUAGE_5'] = azs12language5;
    if(azs12language6 != null) map['AZ_S12_LANGUAGE_6'] = azs12language6;
    if(azs12language7 != null) map['AZ_S12_LANGUAGE_7'] = azs12language7;
    if(azs12language8 != null) map['AZ_S12_LANGUAGE_8'] = azs12language8;
    if(azs12language9 != null) map['AZ_S12_LANGUAGE_9'] = azs12language9;
    if(azs12language10 != null) map['AZ_S12_LANGUAGE_10'] = azs12language10;
    if(azs12language11 != null) map['AZ_S12_LANGUAGE_11'] = azs12language11;
    map['AZ_S13_FLUENCY_A_1'] = azs13fluencya1;
    map['AZ_S13_FLUENCY_A_2'] = azs13fluencya2;
    map['AZ_S13_FLUENCY_A_3'] = azs13fluencya3;
    map['AZ_S13_FLUENCY_A_4'] = azs13fluencya4;
    map['AZ_S13_FLUENCY_A_5'] = azs13fluencya5;
    map['AZ_S13_FLUENCY_A_6'] = azs13fluencya6;
    map['AZ_S13_FLUENCY_A_7'] = azs13fluencya7;
    map['AZ_S13_FLUENCY_A_8'] = azs13fluencya8;
    map['AZ_S13_FLUENCY_A_9'] = azs13fluencya9;
    map['AZ_S13_FLUENCY_A_10'] = azs13fluencya10;
    map['AZ_S13_FLUENCY_A_11'] = azs13fluencya11;
    map['AZ_S13_FLUENCY_A_12'] = azs13fluencya12;
    map['AZ_S13_FLUENCY_A_13'] = azs13fluencya13;
    map['AZ_S13_FLUENCY_A_14'] = azs13fluencya14;
    map['AZ_S13_FLUENCY_B'] = azs13fluencyb;
    map['AZ_S14_FLUENCY'] = azs14fluency;
    map['AZ_S15_VISUOSPATIAL_1'] = azs15visuospatial1;
    map['AZ_S15_VISUOSPATIAL_2'] = azs15visuospatial2;
    map['AZ_S16_VISUOSPATIAL_1'] = azs16visuospatial1;
    map['AZ_S16_VISUOSPATIAL_2'] = azs16visuospatial2;
    map['AZ_S16_VISUOSPATIAL_3'] = azs16visuospatial3;
    map['AZ_S16_VISUOSPATIAL_4'] = azs16visuospatial4;
    map['AZ_S16_VISUOSPATIAL_5'] = azs16visuospatial5;
    map['AZ_S16_VISUOSPATIAL_6'] = azs16visuospatial6;
    map['AZ_S16_VISUOSPATIAL_7'] = azs16visuospatial7;
    map['AZ_S16_VISUOSPATIAL_8'] = azs16visuospatial8;
    map['AZ_S16_VISUOSPATIAL_9'] = azs16visuospatial9;
    map['AZ_S16_VISUOSPATIAL_10'] = azs16visuospatial10;
    map['AZ_S16_VISUOSPATIAL_11'] = azs16visuospatial11;
    return map;
  }


  //key
  String azs1orientationaKey = 'AZ_S1_ORIENTATION_A';
  String azs1orientationbKey = 'AZ_S1_ORIENTATION_B';
  String azs1orientationcKey = 'AZ_S1_ORIENTATION_C';
  String azs1orientationdKey = 'AZ_S1_ORIENTATION_D';
  String azs1orientationeKey = 'AZ_S1_ORIENTATION_E';
  String azs1orientationfKey = 'AZ_S1_ORIENTATION_F';
  String azs2attentionReadonlyKey = 'AZ_S2_ATTENTION_READONLY';
  String azs2attentionKey = 'AZ_S2_ATTENTION';
  String azs3questionKey = 'AZ_S3_QUESTION';
  String azs3forwardKey = 'AZ_S3_FORWARD';
  String azs3backwardKey = 'AZ_S3_BACKWARD';
  String azs4attentionKey = 'AZ_S4_ATTENTION';
  String azs5memory1Key = 'AZ_S5_MEMORY_1';
  String azs5memory2Key = 'AZ_S5_MEMORY_2';
  String azs5memory3Key = 'AZ_S5_MEMORY_3';
  String azs5memory4Key = 'AZ_S5_MEMORY_4';
  String azs5memory5Key = 'AZ_S5_MEMORY_5';
  String azs6memory1Key = 'AZ_S6_MEMORY_1';
  String azs6memory2Key = 'AZ_S6_MEMORY_2';
  String azs6memory3Key = 'AZ_S6_MEMORY_3';
  String azs6memory4Key = 'AZ_S6_MEMORY_4';
  String azs6memory5Key = 'AZ_S6_MEMORY_5';
  String azs6memory6Key = 'AZ_S6_MEMORY_6';
  String azs6memory7Key = 'AZ_S6_MEMORY_7';
  String azs6memory8Key = 'AZ_S6_MEMORY_8';
  String azs6memory9Key = 'AZ_S6_MEMORY_9';
  String azs6memory10Key = 'AZ_S6_MEMORY_10';
  String azs7attention1Key = 'AZ_S7_ATTENTION_1';
  String azs7attention2Key = 'AZ_S7_ATTENTION_2';
  String azs7attention3Key = 'AZ_S7_ATTENTION_3' ;
  String azs8naming1Key = 'AZ_S8_NAMING_1';
  String azs8naming2Key = 'AZ_S8_NAMING_2';
  String azs8naming3Key = 'AZ_S8_NAMING_3';
  String azs8naming4Key = 'AZ_S8_NAMING_4';
  String azs8naming5Key = 'AZ_S8_NAMING_5';
  String azs8naming6Key = 'AZ_S8_NAMING_6';
  String azs8naming7Key = 'AZ_S8_NAMING_7';
  String azs8naming8Key = 'AZ_S8_NAMING_8';
  String azs8naming9Key = 'AZ_S8_NAMING_9';
  String azs8naming10Key = 'AZ_S8_NAMING_10';
  String azs8naming11Key = 'AZ_S8_NAMING_11';
  String azs8naming12Key = 'AZ_S8_NAMING_12';
  String azs9naming1Key = 'AZ_S9_NAMING_1';
  String azs9naming2Key = 'AZ_S9_NAMING_2';
  String azs9naming3Key = 'AZ_S9_NAMING_3';
  String azs9naming4Key = 'AZ_S9_NAMING_4';
  String azs9naming5Key = 'AZ_S9_NAMING_5';
  String azs9naming6Key = 'AZ_S9_NAMING_6';
  String azs9naming7Key = 'AZ_S9_NAMING_7';
  String azs9naming8Key = 'AZ_S9_NAMING_8';
  String azs9naming9Key = 'AZ_S9_NAMING_9';
  String azs9naming10Key = 'AZ_S9_NAMING_10';
  String azs9naming11Key = 'AZ_S9_NAMING_11';
  String azs9naming12Key = 'AZ_S9_NAMING_12' ;
  String azs9naming13Key = 'AZ_S9_NAMING_13';
  String azs9naming14Key = 'AZ_S9_NAMING_14';
  String azs9naming15Key = 'AZ_S9_NAMING_15';
  String azs9naming16Key = 'AZ_S9_NAMING_16';
  String azs9naming17Key = 'AZ_S9_NAMING_17';
  String azs9naming18Key = 'AZ_S9_NAMING_18';
  String azs9naming19Key = 'AZ_S9_NAMING_19';
  String azs9naming20Key = 'AZ_S9_NAMING_20';
  String azs9naming21Key = 'AZ_S9_NAMING_21';
  String azs10language1aKey = 'AZ_S10_LANGUAGE_1_A';
  String azs10language1bKey = 'AZ_S10_LANGUAGE_1_B';
  String azs10language2aKey = 'AZ_S10_LANGUAGE_2_A';
  String azs10language2bKey = 'AZ_S10_LANGUAGE_2_B';
  String azs10language3aKey = 'AZ_S10_LANGUAGE_3_A';
  String azs10language3bKey = 'AZ_S10_LANGUAGE_3_B';
  String azs10language4aKey = 'AZ_S10_LANGUAGE_4_A';
  String azs10language4bKey = 'AZ_S10_LANGUAGE_4_B';
  String azs11language1Key = 'AZ_S11_LANGUAGE_1';
  String azs11language2Key = 'AZ_S11_LANGUAGE_2';
  String azs11language3Key = 'AZ_S11_LANGUAGE_3';
  String azs11language4Key = 'AZ_S11_LANGUAGE_4';
  String azs11language5Key = 'AZ_S11_LANGUAGE_5';
  String azs12language1Key = 'AZ_S12_LANGUAGE_1';
  String azs12language2Key = 'AZ_S12_LANGUAGE_2';
  String azs12language3Key = 'AZ_S12_LANGUAGE_3';
  String azs12language4Key = 'AZ_S12_LANGUAGE_4';
  String azs12language5Key = 'AZ_S12_LANGUAGE_5';
  String azs12language6Key = 'AZ_S12_LANGUAGE_6';
  String azs12language7Key = 'AZ_S12_LANGUAGE_7';
  String azs12language8Key = 'AZ_S12_LANGUAGE_8';
  String azs12language9Key = 'AZ_S12_LANGUAGE_9';
  String azs12language10Key = 'AZ_S12_LANGUAGE_10';
  String azs12language11Key = 'AZ_S12_LANGUAGE_11';
  String azs13fluencyKey = 'AZ_S13_FLUENCY';
  String azs13fluencya1Key = 'AZ_S13_FLUENCY_A_1';
  String azs13fluencya2Key = 'AZ_S13_FLUENCY_A_2';
  String azs13fluencya3Key = 'AZ_S13_FLUENCY_A_3';
  String azs13fluencya4Key = 'AZ_S13_FLUENCY_A_4';
  String azs13fluencya5Key = 'AZ_S13_FLUENCY_A_5';
  String azs13fluencya6Key = 'AZ_S13_FLUENCY_A_6';
  String azs13fluencya7Key = 'AZ_S13_FLUENCY_A_7';
  String azs13fluencya8Key = 'AZ_S13_FLUENCY_A_8';
  String azs13fluencya9Key = 'AZ_S13_FLUENCY_A_9';
  String azs13fluencya10Key = 'AZ_S13_FLUENCY_A_10';
  String azs13fluencya11Key = 'AZ_S13_FLUENCY_A_11';
  String azs13fluencya12Key = 'AZ_S13_FLUENCY_A_12';
  String azs13fluencya13Key = 'AZ_S13_FLUENCY_A_13';
  String azs13fluencya14Key = 'AZ_S13_FLUENCY_A_14';
  String azs13fluencybKey = 'AZ_S13_FLUENCY_B';
  String azs14fluencyKey = 'AZ_S14_FLUENCY';
  String azs15visuospatial1Key = 'AZ_S15_VISUOSPATIAL_1';
  String azs15visuospatial2Key = 'AZ_S15_VISUOSPATIAL_2';
  String azs16visuospatial1Key = 'AZ_S16_VISUOSPATIAL_1';
  String azs16visuospatial2Key = 'AZ_S16_VISUOSPATIAL_2';
  String azs16visuospatial3Key = 'AZ_S16_VISUOSPATIAL_3';
  String azs16visuospatial4Key = 'AZ_S16_VISUOSPATIAL_4';
  String azs16visuospatial5Key = 'AZ_S16_VISUOSPATIAL_5';
  String azs16visuospatial6Key = 'AZ_S16_VISUOSPATIAL_6';
  String azs16visuospatial7Key = 'AZ_S16_VISUOSPATIAL_7';
  String azs16visuospatial8Key = 'AZ_S16_VISUOSPATIAL_8';
  String azs16visuospatial9Key = 'AZ_S16_VISUOSPATIAL_9';
  String azs16visuospatial10Key = 'AZ_S16_VISUOSPATIAL_10';
  String azs16visuospatial11Key = 'AZ_S16_VISUOSPATIAL_11';

  //Label
  String azs6memory1Label = '1000 taka';
  String azs6memory2Label = '500 taka';
  String azs6memory3Label = '200 taka';
  String azs6memory4Label = '100 taka';
  String azs6memory5Label = '50 taka';
  String azs6memory6Label = '20 taka';
  String azs6memory7Label = '10 taka';
  String azs6memory8Label = '5 taka';
  String azs6memory9Label = '2 taka';
  String azs6memory10Label = '1 taka';

  String azs8naming1Label = 'Teeth';
  String azs8naming2Label = 'Eye';
  String azs8naming3Label = 'Ear';
  String azs8naming4Label = 'Nose';
  String azs8naming5Label = 'Arm/Elbow';
  String azs8naming6Label = 'Leg/Knee';
  String azs8naming7Label = 'Forehead';
  String azs8naming8Label = 'Neck';
  String azs8naming9Label = 'Hand';
  String azs8naming10Label = 'Feet';
  String azs8naming11Label = 'Finger';
  String azs8naming12Label = 'Hair';

  String azs9naming1Label = 'Bicycle';
  String azs9naming2Label = 'Boat';
  String azs9naming3Label = 'Book';
  String azs9naming4Label = 'Boti';
  String azs9naming5Label = 'Bus';
  String azs9naming6Label = 'Cow';
  String azs9naming7Label = 'Dog';
  String azs9naming8Label = 'Duck';
  String azs9naming9Label = 'Goat';
  String azs9naming10Label = 'Guava';
  String azs9naming11Label = 'Hen';
  String azs9naming12Label = 'Jackfruit';
  String azs9naming13Label = 'Jug';
  String azs9naming14Label = 'Mango';
  String azs9naming15Label = 'Patil';
  String azs9naming16Label = 'Pen';
  String azs9naming17Label = 'Plate';
  String azs9naming18Label = 'Rickshaw';
  String azs9naming19Label = 'Train';
  String azs9naming20Label = 'Watch';
  String azs9naming21Label = 'Banana';

  String azs11language1Label = 'সুস্বাস্থ্য';
  String azs11language2Label = 'নাতিশীতোষ্ণ';
  String azs11language3Label = 'নিরবিচ্ছিন্ন';
  String azs11language4Label = 'উদ্ভাবনী';
  String azs11language5Label = 'প্রদর্শন';

  String azs12language1Label = 'চকচক করলেই সোনা হয় না';
  String azs12language2Label = 'কষ্ট না করলে কেষ্ট মেলে না';
  String azs12language3Label = 'যেমন কর্ম তেমন ফল';
  String azs12language4Label = 'কারও পৌষ মাস, কারও সর্বনাশ';
  String azs12language5Label = 'চোর পালালে বুদ্ধি বাড়ে';
  String azs12language6Label = 'অতি চালাকের গলায় দড়ি';
  String azs12language7Label = 'দশের লার্ঠি একের বোঝা';
  String azs12language8Label = 'সময়ের এক ফোঁড় অসময়ের দশ ফোঁড়';
  String azs12language9Label = 'একবার না পারিলে দেখ শতবার';
  String azs12language10Label = 'ইচ্ছা থাকলে উপায় হয়';
  String azs12language11Label = 'ঢিলটি মারলে পাটকেলটি খেতে হয়';

  String azs13fluencya1Label = 'ক';
  String azs13fluencya2Label = 'গ';
  String azs13fluencya3Label = 'চ';
  String azs13fluencya4Label = 'জ';
  String azs13fluencya5Label = 'আ';
  String azs13fluencya6Label = 'ই';
  String azs13fluencya7Label = 'ম';
  String azs13fluencya8Label = 'ত';
  String azs13fluencya9Label = 'ধ';
  String azs13fluencya10Label = 'দ';
  String azs13fluencya11Label = 'ন';
  String azs13fluencya12Label = 'প';
  String azs13fluencya13Label = 'ব';
  String azs13fluencya14Label = 'র';

  String azs15visuospatial1Label = 'triangles';
  String azs15visuospatial2Label = 'hexagon';

  String azs16visuospatial1Label = 'dot 5';
  String azs16visuospatial2Label = 'dot 6';
  String azs16visuospatial3Label = 'dot 7';
  String azs16visuospatial4Label = 'dot 8';
  String azs16visuospatial5Label = 'dot 9';
  String azs16visuospatial6Label = 'dot 10';
  String azs16visuospatial7Label = 'dot 11';
  String azs16visuospatial8Label = 'dot 12';
  String azs16visuospatial9Label = 'dot 13';
  String azs16visuospatial10Label = 'dot 14';
  String azs16visuospatial11Label = 'dot 15';



}