import 'package:alzheimer_flutter/app/dto/question/questions_options.dart';

class QuestionsOptionsResponse {
  QuestionsOptionsResponse({
      this.questionsAndOptions,});

  QuestionsOptionsResponse.fromJson(dynamic json) {
    questionsAndOptions = json['questionsAndOptions'] != null ? QuestionsAndOptions.fromJson(json['questionsAndOptions']) : null;
  }
  QuestionsAndOptions? questionsAndOptions;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (questionsAndOptions != null) {
      map['questionsAndOptions'] = questionsAndOptions?.toJson();
    }
    return map;
  }

}