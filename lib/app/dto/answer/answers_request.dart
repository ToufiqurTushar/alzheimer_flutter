import 'answers.dart';

class AnswersRequest {
  AnswersRequest({
      this.answers, 
      this.measuredAt, 
      this.userId,});

  AnswersRequest.fromJson(dynamic json) {
    answers = json['answers'] != null ? Answers.fromJson(json['answers']) : null;
    measuredAt = json['measuredAt'];
    userId = json['userId'];
  }
  Answers? answers;
  num? measuredAt;
  num? userId;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (answers != null) {
      map['answers'] = answers?.toJson();
    }
    map['measuredAt'] = measuredAt;
    map['userId'] = userId;
    return map;
  }

}