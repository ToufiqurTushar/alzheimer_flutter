import 'package:alzheimer_flutter/app/enum/option_enum.dart';
import 'package:alzheimer_flutter/core/utils/methods.dart';


class Answers {
  Answers({
    this.azs1orientationa,
    this.azs1orientationb,
    this.azs1orientationc,
    this.azs1orientationd,
    this.azs1orientatione,
    this.azs1orientationf,
    this.azs2attention,
    this.azs3question,
    this.azs3forward,
    this.azs3backward,
    this.azs4attention,
    this.azs5memory1,
    this.azs5memory2,
    this.azs5memory3,
    this.azs5memory4,
    this.azs5memory5,
    this.azs6memory1,
    this.azs6memory2,
    this.azs6memory3,
    this.azs6memory4,
    this.azs6memory5,
    this.azs6memory6,
    this.azs6memory7,
    this.azs6memory8,
    this.azs6memory9,
    this.azs6memory10,
    this.azs7attention1,
    this.azs7attention2,
    this.azs7attention3,
    this.azs8naming1,
    this.azs8naming2,
    this.azs8naming3,
    this.azs8naming4,
    this.azs8naming5,
    this.azs8naming6,
    this.azs8naming7,
    this.azs8naming8,
    this.azs8naming9,
    this.azs8naming10,
    this.azs8naming11,
    this.azs8naming12,
    this.azs9naming1,
    this.azs9naming2,
    this.azs9naming3,
    this.azs9naming4,
    this.azs9naming5,
    this.azs9naming6,
    this.azs9naming7,
    this.azs9naming8,
    this.azs9naming9,
    this.azs9naming10,
    this.azs9naming11,
    this.azs9naming12,
    this.azs9naming13,
    this.azs9naming14,
    this.azs9naming15,
    this.azs9naming16,
    this.azs9naming17,
    this.azs9naming18,
    this.azs9naming19,
    this.azs9naming20,
    this.azs9naming21,
    this.azs10language1a,
    this.azs10language1b,
    this.azs10language2a,
    this.azs10language2b,
    this.azs10language3a,
    this.azs10language3b,
    this.azs10language4a,
    this.azs10language4b,
    this.azs11language1,
    this.azs11language2,
    this.azs11language3,
    this.azs11language4,
    this.azs11language5,
    this.azs12language1,
    this.azs12language2,
    this.azs12language3,
    this.azs12language4,
    this.azs12language5,
    this.azs12language6,
    this.azs12language7,
    this.azs12language8,
    this.azs12language9,
    this.azs12language10,
    this.azs12language11,
    this.azs13fluencya1,
    this.azs13fluencya2,
    this.azs13fluencya3,
    this.azs13fluencya4,
    this.azs13fluencya5,
    this.azs13fluencya6,
    this.azs13fluencya7,
    this.azs13fluencya8,
    this.azs13fluencya9,
    this.azs13fluencya10,
    this.azs13fluencya11,
    this.azs13fluencya12,
    this.azs13fluencya13,
    this.azs13fluencya14,
    this.azs13fluencyb,
    this.azs14fluency,
    this.azs15visuospatial1,
    this.azs15visuospatial2,
    this.azs16visuospatial1,
    this.azs16visuospatial2,
    this.azs16visuospatial3,
    this.azs16visuospatial4,
    this.azs16visuospatial5,
    this.azs16visuospatial6,
    this.azs16visuospatial7,
    this.azs16visuospatial8,
    this.azs16visuospatial9,
    this.azs16visuospatial10,
    this.azs16visuospatial11,});

  Answers.fromJson(dynamic json) {
    azs1orientationa = json['AZ_S1_ORIENTATION_A'] != null ? [OptionEnum.getDateTextByLabel(json['AZ_S1_ORIENTATION_A'])] : [];
    azs1orientationb = json['AZ_S1_ORIENTATION_B'] != null ? [OptionEnum.getMonthNumberByKeyAndLabel('AZ_S1_ORIENTATION_B', json['AZ_S1_ORIENTATION_B'])] : [];
    azs1orientationc = json['AZ_S1_ORIENTATION_C'] != null ? [bngToEng(json['AZ_S1_ORIENTATION_C'])] : [];
    azs1orientationd = json['AZ_S1_ORIENTATION_D'] != null ? [OptionEnum.getLabelEnByKeyAndLabel('AZ_S1_ORIENTATION_D', json['AZ_S1_ORIENTATION_D'])] : [];
    azs1orientatione = json['AZ_S1_ORIENTATION_E'] != null ? [OptionEnum.getBooleanTextByLabel(json['AZ_S1_ORIENTATION_E'])] : [];
    azs1orientationf = json['AZ_S1_ORIENTATION_F'] != null ? [OptionEnum.getBooleanTextByLabel(json['AZ_S1_ORIENTATION_F'])] : [];
    azs2attention = json['AZ_S2_ATTENTION'] != null ? OptionEnum.getNameArrayByKeyAndLabels('AZ_S2_ATTENTION', json['AZ_S2_ATTENTION']) : [];
    azs3question = json['AZ_S3_QUESTION'] != null ? json['AZ_S3_QUESTION'].split('') : [];
    azs3forward = json['AZ_S3_FORWARD'] != null ? json['AZ_S3_FORWARD'].split('') : [];
    azs3backward = json['AZ_S3_BACKWARD'] != null ? json['AZ_S3_BACKWARD'].split('') : [];
    azs4attention = json['AZ_S4_ATTENTION'] != null ? OptionEnum.getNameArrayByKeyAndLabels('AZ_S4_ATTENTION', json['AZ_S4_ATTENTION']) : [];
    azs5memory1 = json['AZ_S5_MEMORY_1'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S5_MEMORY_1', json['AZ_S5_MEMORY_1'])] : [];
    azs5memory2 = json['AZ_S5_MEMORY_2'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S5_MEMORY_2', json['AZ_S5_MEMORY_2'])] : [];
    azs5memory3 = json['AZ_S5_MEMORY_3'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S5_MEMORY_3', json['AZ_S5_MEMORY_3'])] : [];
    azs5memory4 = json['AZ_S5_MEMORY_4'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S5_MEMORY_4', json['AZ_S5_MEMORY_4'])] : [];
    azs5memory5 = json['AZ_S5_MEMORY_5'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S5_MEMORY_5', json['AZ_S5_MEMORY_5'])] : [];
    azs6memory1 = json['AZ_S6_MEMORY_1'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S6_MEMORY_1', json['AZ_S6_MEMORY_1'])] : [];
    azs6memory2 = json['AZ_S6_MEMORY_2'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S6_MEMORY_2', json['AZ_S6_MEMORY_2'])] : [];
    azs6memory3 = json['AZ_S6_MEMORY_3'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S6_MEMORY_3', json['AZ_S6_MEMORY_3'])] : [];
    azs6memory4 = json['AZ_S6_MEMORY_4'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S6_MEMORY_4', json['AZ_S6_MEMORY_4'])] : [];
    azs6memory5 = json['AZ_S6_MEMORY_5'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S6_MEMORY_5', json['AZ_S6_MEMORY_5'])] : [];
    azs6memory6 = json['AZ_S6_MEMORY_6'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S6_MEMORY_6', json['AZ_S6_MEMORY_6'])] : [];
    azs6memory7 = json['AZ_S6_MEMORY_7'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S6_MEMORY_7', json['AZ_S6_MEMORY_7'])] : [];
    azs6memory8 = json['AZ_S6_MEMORY_8'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S6_MEMORY_8', json['AZ_S6_MEMORY_8'])] : [];
    azs6memory9 = json['AZ_S6_MEMORY_9'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S6_MEMORY_9', json['AZ_S6_MEMORY_9'])] : [];
    azs6memory10 = json['AZ_S6_MEMORY_10'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S6_MEMORY_10', json['AZ_S6_MEMORY_10'])] : [];
    azs7attention1 = json['AZ_S7_ATTENTION_1'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S7_ATTENTION_1', json['AZ_S7_ATTENTION_1'])] : [];
    azs7attention2 = json['AZ_S7_ATTENTION_2'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S7_ATTENTION_2', json['AZ_S7_ATTENTION_2'])] : [];
    azs7attention3 = json['AZ_S7_ATTENTION_3'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S7_ATTENTION_3', json['AZ_S7_ATTENTION_3'])] : [];
    azs8naming1 = json['AZ_S8_NAMING_1'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S8_NAMING_1', json['AZ_S8_NAMING_1'])] : [];
    azs8naming2 = json['AZ_S8_NAMING_2'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S8_NAMING_2', json['AZ_S8_NAMING_2'])] : [];
    azs8naming3 = json['AZ_S8_NAMING_3'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S8_NAMING_3', json['AZ_S8_NAMING_3'])] : [];
    azs8naming4 = json['AZ_S8_NAMING_4'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S8_NAMING_4', json['AZ_S8_NAMING_4'])] : [];
    azs8naming5 = json['AZ_S8_NAMING_5'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S8_NAMING_5', json['AZ_S8_NAMING_5'])] : [];
    azs8naming6 = json['AZ_S8_NAMING_6'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S8_NAMING_6', json['AZ_S8_NAMING_6'])] : [];
    azs8naming7 = json['AZ_S8_NAMING_7'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S8_NAMING_7', json['AZ_S8_NAMING_7'])] : [];
    azs8naming8 = json['AZ_S8_NAMING_8'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S8_NAMING_8', json['AZ_S8_NAMING_8'])] : [];
    azs8naming9 = json['AZ_S8_NAMING_9'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S8_NAMING_9', json['AZ_S8_NAMING_9'])] : [];
    azs8naming10 = json['AZ_S8_NAMING_10'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S8_NAMING_10', json['AZ_S8_NAMING_10'])] : [];
    azs8naming11 = json['AZ_S8_NAMING_11'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S8_NAMING_11', json['AZ_S8_NAMING_11'])] : [];
    azs8naming12 = json['AZ_S8_NAMING_12'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S8_NAMING_12', json['AZ_S8_NAMING_12'])] : [];
    azs9naming1 = json['AZ_S9_NAMING_1'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S9_NAMING_1', json['AZ_S9_NAMING_1'])] : [];
    azs9naming2 = json['AZ_S9_NAMING_2'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S9_NAMING_2', json['AZ_S9_NAMING_2'])] : [];
    azs9naming3 = json['AZ_S9_NAMING_3'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S9_NAMING_3', json['AZ_S9_NAMING_3'])] : [];
    azs9naming4 = json['AZ_S9_NAMING_4'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S9_NAMING_4', json['AZ_S9_NAMING_4'])] : [];
    azs9naming5 = json['AZ_S9_NAMING_5'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S9_NAMING_5', json['AZ_S9_NAMING_5'])] : [];
    azs9naming6 = json['AZ_S9_NAMING_6'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S9_NAMING_6', json['AZ_S9_NAMING_6'])] : [];
    azs9naming7 = json['AZ_S9_NAMING_7'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S9_NAMING_7', json['AZ_S9_NAMING_7'])] : [];
    azs9naming8 = json['AZ_S9_NAMING_8'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S9_NAMING_8', json['AZ_S9_NAMING_8'])] : [];
    azs9naming9 = json['AZ_S9_NAMING_9'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S9_NAMING_9', json['AZ_S9_NAMING_9'])] : [];
    azs9naming10 = json['AZ_S9_NAMING_10'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S9_NAMING_10', json['AZ_S9_NAMING_10'])] : [];
    azs9naming11 = json['AZ_S9_NAMING_11'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S9_NAMING_11', json['AZ_S9_NAMING_11'])] : [];
    azs9naming12 = json['AZ_S9_NAMING_12'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S9_NAMING_12', json['AZ_S9_NAMING_12'])] : [];
    azs9naming13 = json['AZ_S9_NAMING_13'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S9_NAMING_13', json['AZ_S9_NAMING_13'])] : [];
    azs9naming14 = json['AZ_S9_NAMING_14'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S9_NAMING_14', json['AZ_S9_NAMING_14'])] : [];
    azs9naming15 = json['AZ_S9_NAMING_15'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S9_NAMING_15', json['AZ_S9_NAMING_15'])] : [];
    azs9naming16 = json['AZ_S9_NAMING_16'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S9_NAMING_16', json['AZ_S9_NAMING_16'])] : [];
    azs9naming17 = json['AZ_S9_NAMING_17'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S9_NAMING_17', json['AZ_S9_NAMING_17'])] : [];
    azs9naming18 = json['AZ_S9_NAMING_18'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S9_NAMING_18', json['AZ_S9_NAMING_18'])] : [];
    azs9naming19 = json['AZ_S9_NAMING_19'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S9_NAMING_19', json['AZ_S9_NAMING_19'])] : [];
    azs9naming20 = json['AZ_S9_NAMING_20'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S9_NAMING_20', json['AZ_S9_NAMING_20'])] : [];
    azs9naming21 = json['AZ_S9_NAMING_21'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S9_NAMING_21', json['AZ_S9_NAMING_21'])] : [];
    azs10language1a = json['AZ_S10_LANGUAGE_1_A'] != null ? [OptionEnum.getBooleanTextByLabel(json['AZ_S10_LANGUAGE_1_A'])] : [];
    azs10language1b = json['AZ_S10_LANGUAGE_1_B'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S10_LANGUAGE_1_B', json['AZ_S10_LANGUAGE_1_B'])] : [];
    azs10language2a = json['AZ_S10_LANGUAGE_2_A'] != null ? [OptionEnum.getBooleanTextByLabel(json['AZ_S10_LANGUAGE_2_A'])] : [];
    azs10language2b = json['AZ_S10_LANGUAGE_2_B'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S10_LANGUAGE_2_B', json['AZ_S10_LANGUAGE_2_B'])] : [];
    azs10language3a = json['AZ_S10_LANGUAGE_3_A'] != null ? [OptionEnum.getBooleanTextByLabel(json['AZ_S10_LANGUAGE_3_A'])] : [];
    azs10language3b = json['AZ_S10_LANGUAGE_3_B'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S10_LANGUAGE_3_B', json['AZ_S10_LANGUAGE_3_B'])] : [];
    azs10language4a = json['AZ_S10_LANGUAGE_4_A'] != null ? [OptionEnum.getBooleanTextByLabel(json['AZ_S10_LANGUAGE_4_A'])] : [];
    azs10language4b = json['AZ_S10_LANGUAGE_4_B'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S10_LANGUAGE_4_B', json['AZ_S10_LANGUAGE_4_B'])] : [];
    azs11language1 = json['AZ_S11_LANGUAGE_1'] != null ? [OptionEnum.getBooleanTextByLabel(json['AZ_S11_LANGUAGE_1'])] : [];
    azs11language2 = json['AZ_S11_LANGUAGE_2'] != null ? [OptionEnum.getBooleanTextByLabel(json['AZ_S11_LANGUAGE_2'])] : [];
    azs11language3 = json['AZ_S11_LANGUAGE_3'] != null ? [OptionEnum.getBooleanTextByLabel(json['AZ_S11_LANGUAGE_3'])] : [];
    azs11language4 = json['AZ_S11_LANGUAGE_4'] != null ? [OptionEnum.getBooleanTextByLabel(json['AZ_S11_LANGUAGE_4'])] : [];
    azs11language5 = json['AZ_S11_LANGUAGE_5'] != null ? [OptionEnum.getBooleanTextByLabel(json['AZ_S11_LANGUAGE_5'])] : [];
    azs12language1 = json['AZ_S12_LANGUAGE_1'] != null ? [OptionEnum.getBooleanTextByLabel(json['AZ_S12_LANGUAGE_1'])] : [];
    azs12language2 = json['AZ_S12_LANGUAGE_2'] != null ? [OptionEnum.getBooleanTextByLabel(json['AZ_S12_LANGUAGE_2'])] : [];
    azs12language3 = json['AZ_S12_LANGUAGE_3'] != null ? [OptionEnum.getBooleanTextByLabel(json['AZ_S12_LANGUAGE_3'])] : [];
    azs12language4 = json['AZ_S12_LANGUAGE_4'] != null ? [OptionEnum.getBooleanTextByLabel(json['AZ_S12_LANGUAGE_4'])] : [];
    azs12language5 = json['AZ_S12_LANGUAGE_5'] != null ? [OptionEnum.getBooleanTextByLabel(json['AZ_S12_LANGUAGE_5'])] : [];
    azs12language6 = json['AZ_S12_LANGUAGE_6'] != null ? [OptionEnum.getBooleanTextByLabel(json['AZ_S12_LANGUAGE_6'])] : [];
    azs12language7 = json['AZ_S12_LANGUAGE_7'] != null ? [OptionEnum.getBooleanTextByLabel(json['AZ_S12_LANGUAGE_7'])] : [];
    azs12language8 = json['AZ_S12_LANGUAGE_8'] != null ? [OptionEnum.getBooleanTextByLabel(json['AZ_S12_LANGUAGE_8'])] : [];
    azs12language9 = json['AZ_S12_LANGUAGE_9'] != null ? [OptionEnum.getBooleanTextByLabel(json['AZ_S12_LANGUAGE_9'])] : [];
    azs12language10 = json['AZ_S12_LANGUAGE_10'] != null ? [OptionEnum.getBooleanTextByLabel(json['AZ_S12_LANGUAGE_10'])] : [];
    azs12language11 = json['AZ_S12_LANGUAGE_11'] != null ? [OptionEnum.getBooleanTextByLabel(json['AZ_S12_LANGUAGE_11'])] : [];
    azs13fluencya1 = json['AZ_S13_FLUENCY_A_1'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S13_FLUENCY', json['AZ_S13_FLUENCY_A_1'])] : [];
    azs13fluencya2 = json['AZ_S13_FLUENCY_A_2'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S13_FLUENCY', json['AZ_S13_FLUENCY_A_2'])] : [];
    azs13fluencya3 = json['AZ_S13_FLUENCY_A_3'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S13_FLUENCY', json['AZ_S13_FLUENCY_A_3'])] : [];
    azs13fluencya4 = json['AZ_S13_FLUENCY_A_4'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S13_FLUENCY', json['AZ_S13_FLUENCY_A_4'])] : [];
    azs13fluencya5 = json['AZ_S13_FLUENCY_A_5'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S13_FLUENCY', json['AZ_S13_FLUENCY_A_5'])] : [];
    azs13fluencya6 = json['AZ_S13_FLUENCY_A_6'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S13_FLUENCY', json['AZ_S13_FLUENCY_A_6'])] : [];
    azs13fluencya7 = json['AZ_S13_FLUENCY_A_7'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S13_FLUENCY', json['AZ_S13_FLUENCY_A_7'])] : [];
    azs13fluencya8 = json['AZ_S13_FLUENCY_A_8'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S13_FLUENCY', json['AZ_S13_FLUENCY_A_8'])] : [];
    azs13fluencya9 = json['AZ_S13_FLUENCY_A_9'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S13_FLUENCY', json['AZ_S13_FLUENCY_A_9'])] : [];
    azs13fluencya10 = json['AZ_S13_FLUENCY_A_10'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S13_FLUENCY', json['AZ_S13_FLUENCY_A_10'])] : [];
    azs13fluencya11 = json['AZ_S13_FLUENCY_A_11'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S13_FLUENCY', json['AZ_S13_FLUENCY_A_11'])] : [];
    azs13fluencya12 = json['AZ_S13_FLUENCY_A_12'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S13_FLUENCY', json['AZ_S13_FLUENCY_A_12'])] : [];
    azs13fluencya13 = json['AZ_S13_FLUENCY_A_13'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S13_FLUENCY', json['AZ_S13_FLUENCY_A_13'])] : [];
    azs13fluencya14 = json['AZ_S13_FLUENCY_A_14'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S13_FLUENCY', json['AZ_S13_FLUENCY_A_14'])] : [];
    azs13fluencyb = json['AZ_S13_FLUENCY_B'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S13_FLUENCY', json['AZ_S13_FLUENCY_B'])] : [];
    azs14fluency = json['AZ_S14_FLUENCY'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S14_FLUENCY', json['AZ_S14_FLUENCY'])] : [];
    azs15visuospatial1 = json['AZ_S15_VISUOSPATIAL_1'] != null ? [OptionEnum.getBooleanTextByLabel(json['AZ_S15_VISUOSPATIAL_1'])] : [];
    azs15visuospatial2 = json['AZ_S15_VISUOSPATIAL_2'] != null ? [OptionEnum.getBooleanTextByLabel(json['AZ_S15_VISUOSPATIAL_2'])] : [];
    azs16visuospatial1 = json['AZ_S16_VISUOSPATIAL_1'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S16_VISUOSPATIAL_1', json['AZ_S16_VISUOSPATIAL_1'])] : [];
    azs16visuospatial2 = json['AZ_S16_VISUOSPATIAL_2'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S16_VISUOSPATIAL_2', json['AZ_S16_VISUOSPATIAL_2'])] : [];
    azs16visuospatial3 = json['AZ_S16_VISUOSPATIAL_3'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S16_VISUOSPATIAL_3', json['AZ_S16_VISUOSPATIAL_3'])] : [];
    azs16visuospatial4 = json['AZ_S16_VISUOSPATIAL_4'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S16_VISUOSPATIAL_4', json['AZ_S16_VISUOSPATIAL_4'])] : [];
    azs16visuospatial5 = json['AZ_S16_VISUOSPATIAL_5'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S16_VISUOSPATIAL_5', json['AZ_S16_VISUOSPATIAL_5'])] : [];
    azs16visuospatial6 = json['AZ_S16_VISUOSPATIAL_6'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S16_VISUOSPATIAL_6', json['AZ_S16_VISUOSPATIAL_6'])] : [];
    azs16visuospatial7 = json['AZ_S16_VISUOSPATIAL_7'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S16_VISUOSPATIAL_7', json['AZ_S16_VISUOSPATIAL_7'])] : [];
    azs16visuospatial8 = json['AZ_S16_VISUOSPATIAL_8'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S16_VISUOSPATIAL_8', json['AZ_S16_VISUOSPATIAL_8'])] : [];
    azs16visuospatial9 = json['AZ_S16_VISUOSPATIAL_9'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S16_VISUOSPATIAL_9', json['AZ_S16_VISUOSPATIAL_9'])] : [];
    azs16visuospatial10 = json['AZ_S16_VISUOSPATIAL_10'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S16_VISUOSPATIAL_10', json['AZ_S16_VISUOSPATIAL_10'])] : [];
    azs16visuospatial11 = json['AZ_S16_VISUOSPATIAL_11'] != null ? [OptionEnum.getNameByKeyAndLabel('AZ_S16_VISUOSPATIAL_11', json['AZ_S16_VISUOSPATIAL_11'])] : [];
  }

  List<String>? azs1orientationa;
  List<String>? azs1orientationb;
  List<String>? azs1orientationc;
  List<String>? azs1orientationd;
  List<String>? azs1orientatione;
  List<String>? azs1orientationf;
  List<String>? azs2attention;
  List<String>? azs3question;
  List<String>? azs3forward;
  List<String>? azs3backward;
  List<String>? azs4attention;
  List<String>? azs5memory1;
  List<String>? azs5memory2;
  List<String>? azs5memory3;
  List<String>? azs5memory4;
  List<String>? azs5memory5;
  List<String>? azs6memory1;
  List<String>? azs6memory2;
  List<String>? azs6memory3;
  List<String>? azs6memory4;
  List<String>? azs6memory5;
  List<String>? azs6memory6;
  List<String>? azs6memory7;
  List<String>? azs6memory8;
  List<String>? azs6memory9;
  List<String>? azs6memory10;
  List<String>? azs7attention1;
  List<String>? azs7attention2;
  List<String>? azs7attention3;
  List<String>? azs8naming1;
  List<String>? azs8naming2;
  List<String>? azs8naming3;
  List<String>? azs8naming4;
  List<String>? azs8naming5;
  List<String>? azs8naming6;
  List<String>? azs8naming7;
  List<String>? azs8naming8;
  List<String>? azs8naming9;
  List<String>? azs8naming10;
  List<String>? azs8naming11;
  List<String>? azs8naming12;
  List<String>? azs9naming1;
  List<String>? azs9naming2;
  List<String>? azs9naming3;
  List<String>? azs9naming4;
  List<String>? azs9naming5;
  List<String>? azs9naming6;
  List<String>? azs9naming7;
  List<String>? azs9naming8;
  List<String>? azs9naming9;
  List<String>? azs9naming10;
  List<String>? azs9naming11;
  List<String>? azs9naming12;
  List<String>? azs9naming13;
  List<String>? azs9naming14;
  List<String>? azs9naming15;
  List<String>? azs9naming16;
  List<String>? azs9naming17;
  List<String>? azs9naming18;
  List<String>? azs9naming19;
  List<String>? azs9naming20;
  List<String>? azs9naming21;
  List<String>? azs10language1a;
  List<String>? azs10language1b;
  List<String>? azs10language2a;
  List<String>? azs10language2b;
  List<String>? azs10language3a;
  List<String>? azs10language3b;
  List<String>? azs10language4a;
  List<String>? azs10language4b;
  List<String>? azs11language1;
  List<String>? azs11language2;
  List<String>? azs11language3;
  List<String>? azs11language4;
  List<String>? azs11language5;
  List<String>? azs12language1;
  List<String>? azs12language2;
  List<String>? azs12language3;
  List<String>? azs12language4;
  List<String>? azs12language5;
  List<String>? azs12language6;
  List<String>? azs12language7;
  List<String>? azs12language8;
  List<String>? azs12language9;
  List<String>? azs12language10;
  List<String>? azs12language11;
  List<String>? azs13fluencya1;
  List<String>? azs13fluencya2;
  List<String>? azs13fluencya3;
  List<String>? azs13fluencya4;
  List<String>? azs13fluencya5;
  List<String>? azs13fluencya6;
  List<String>? azs13fluencya7;
  List<String>? azs13fluencya8;
  List<String>? azs13fluencya9;
  List<String>? azs13fluencya10;
  List<String>? azs13fluencya11;
  List<String>? azs13fluencya12;
  List<String>? azs13fluencya13;
  List<String>? azs13fluencya14;
  List<String>? azs13fluencyb;
  List<String>? azs14fluency;
  List<String>? azs15visuospatial1;
  List<String>? azs15visuospatial2;
  List<String>? azs16visuospatial1;
  List<String>? azs16visuospatial2;
  List<String>? azs16visuospatial3;
  List<String>? azs16visuospatial4;
  List<String>? azs16visuospatial5;
  List<String>? azs16visuospatial6;
  List<String>? azs16visuospatial7;
  List<String>? azs16visuospatial8;
  List<String>? azs16visuospatial9;
  List<String>? azs16visuospatial10;
  List<String>? azs16visuospatial11;



  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['AZ_S1_ORIENTATION_A'] = azs1orientationa;
    map['AZ_S1_ORIENTATION_B'] = azs1orientationb;
    map['AZ_S1_ORIENTATION_C'] = azs1orientationc;
    map['AZ_S1_ORIENTATION_D'] = azs1orientationd;
    map['AZ_S1_ORIENTATION_E'] = azs1orientatione;
    map['AZ_S1_ORIENTATION_F'] = azs1orientationf;
    map['AZ_S2_ATTENTION'] = azs2attention;
    map['AZ_S3_QUESTION'] = azs3question;
    map['AZ_S3_FORWARD'] = azs3forward;
    map['AZ_S3_BACKWARD'] = azs3backward;
    map['AZ_S4_ATTENTION'] = azs4attention;
    map['AZ_S5_MEMORY_1'] = azs5memory1;
    map['AZ_S5_MEMORY_2'] = azs5memory2;
    map['AZ_S5_MEMORY_3'] = azs5memory3;
    map['AZ_S5_MEMORY_4'] = azs5memory4;
    map['AZ_S5_MEMORY_5'] = azs5memory5;

    if(azs6memory1!.isNotEmpty) map['AZ_S6_MEMORY_1'] = azs6memory1;
    if(azs6memory2!.isNotEmpty) map['AZ_S6_MEMORY_2'] = azs6memory2;
    if(azs6memory3!.isNotEmpty) map['AZ_S6_MEMORY_3'] = azs6memory3;
    if(azs6memory4!.isNotEmpty) map['AZ_S6_MEMORY_4'] = azs6memory4;
    if(azs6memory5!.isNotEmpty) map['AZ_S6_MEMORY_5'] = azs6memory5;
    if(azs6memory6!.isNotEmpty) map['AZ_S6_MEMORY_6'] = azs6memory6;
    if(azs6memory7!.isNotEmpty) map['AZ_S6_MEMORY_7'] = azs6memory7;
    if(azs6memory8!.isNotEmpty) map['AZ_S6_MEMORY_8'] = azs6memory8;
    if(azs6memory9!.isNotEmpty) map['AZ_S6_MEMORY_9'] = azs6memory9;
    if(azs6memory10!.isNotEmpty) map['AZ_S6_MEMORY_10'] = azs6memory10;

    map['AZ_S7_ATTENTION_1'] = azs7attention1;
    map['AZ_S7_ATTENTION_2'] = azs7attention2;
    map['AZ_S7_ATTENTION_3'] = azs7attention3;

    if(azs8naming1!.isNotEmpty) map['AZ_S8_NAMING_1'] = azs8naming1;
    if(azs8naming2!.isNotEmpty) map['AZ_S8_NAMING_2'] = azs8naming2;
    if(azs8naming3!.isNotEmpty) map['AZ_S8_NAMING_3'] = azs8naming3;
    if(azs8naming4!.isNotEmpty) map['AZ_S8_NAMING_4'] = azs8naming4;
    if(azs8naming5!.isNotEmpty) map['AZ_S8_NAMING_5'] = azs8naming5;
    if(azs8naming6!.isNotEmpty) map['AZ_S8_NAMING_6'] = azs8naming6;
    if(azs8naming7!.isNotEmpty) map['AZ_S8_NAMING_7'] = azs8naming7;
    if(azs8naming8!.isNotEmpty) map['AZ_S8_NAMING_8'] = azs8naming8;
    if(azs8naming9!.isNotEmpty) map['AZ_S8_NAMING_9'] = azs8naming9;
    if(azs8naming10!.isNotEmpty) map['AZ_S8_NAMING_10'] = azs8naming10;
    if(azs8naming11!.isNotEmpty) map['AZ_S8_NAMING_11'] = azs8naming11;
    if(azs8naming12!.isNotEmpty) map['AZ_S8_NAMING_12'] = azs8naming12;

    if(azs9naming1!.isNotEmpty) map['AZ_S9_NAMING_1'] = azs9naming1;
    if(azs9naming2!.isNotEmpty) map['AZ_S9_NAMING_2'] = azs9naming2;
    if(azs9naming3!.isNotEmpty) map['AZ_S9_NAMING_3'] = azs9naming3;
    if(azs9naming4!.isNotEmpty) map['AZ_S9_NAMING_4'] = azs9naming4;
    if(azs9naming5!.isNotEmpty) map['AZ_S9_NAMING_5'] = azs9naming5;
    if(azs9naming6!.isNotEmpty) map['AZ_S9_NAMING_6'] = azs9naming6;
    if(azs9naming7!.isNotEmpty) map['AZ_S9_NAMING_7'] = azs9naming7;
    if(azs9naming8!.isNotEmpty) map['AZ_S9_NAMING_8'] = azs9naming8;
    if(azs9naming9!.isNotEmpty) map['AZ_S9_NAMING_9'] = azs9naming9;
    if(azs9naming10!.isNotEmpty) map['AZ_S9_NAMING_10'] = azs9naming10;
    if(azs9naming11!.isNotEmpty) map['AZ_S9_NAMING_11'] = azs9naming11;
    if(azs9naming12!.isNotEmpty) map['AZ_S9_NAMING_12'] = azs9naming12;
    if(azs9naming13!.isNotEmpty) map['AZ_S9_NAMING_13'] = azs9naming13;
    if(azs9naming14!.isNotEmpty) map['AZ_S9_NAMING_14'] = azs9naming14;
    if(azs9naming15!.isNotEmpty) map['AZ_S9_NAMING_15'] = azs9naming15;
    if(azs9naming16!.isNotEmpty) map['AZ_S9_NAMING_16'] = azs9naming16;
    if(azs9naming17!.isNotEmpty) map['AZ_S9_NAMING_17'] = azs9naming17;
    if(azs9naming18!.isNotEmpty) map['AZ_S9_NAMING_18'] = azs9naming18;
    if(azs9naming19!.isNotEmpty) map['AZ_S9_NAMING_19'] = azs9naming19;
    if(azs9naming20!.isNotEmpty) map['AZ_S9_NAMING_20'] = azs9naming20;
    if(azs9naming21!.isNotEmpty) map['AZ_S9_NAMING_21'] = azs9naming21;

    map['AZ_S10_LANGUAGE_1_A'] = azs10language1a;
    if(azs10language1b!.isNotEmpty) map['AZ_S10_LANGUAGE_1_B'] = azs10language1b;
    map['AZ_S10_LANGUAGE_2_A'] = azs10language2a;
    if(azs10language2b!.isNotEmpty) map['AZ_S10_LANGUAGE_2_B'] = azs10language2b;
    map['AZ_S10_LANGUAGE_3_A'] = azs10language3a;
    if(azs10language3b!.isNotEmpty) map['AZ_S10_LANGUAGE_3_B'] = azs10language3b;
    map['AZ_S10_LANGUAGE_4_A'] = azs10language4a;
    if(azs10language4b!.isNotEmpty) map['AZ_S10_LANGUAGE_4_B'] = azs10language4b;

    map['AZ_S11_LANGUAGE_1'] = azs11language1;
    map['AZ_S11_LANGUAGE_2'] = azs11language2;
    map['AZ_S11_LANGUAGE_3'] = azs11language3;
    map['AZ_S11_LANGUAGE_4'] = azs11language4;
    map['AZ_S11_LANGUAGE_5'] = azs11language5;

    if(azs12language1!.isNotEmpty) map['AZ_S12_LANGUAGE_1'] = azs12language1;
    if(azs12language2!.isNotEmpty) map['AZ_S12_LANGUAGE_2'] = azs12language2;
    if(azs12language3!.isNotEmpty) map['AZ_S12_LANGUAGE_3'] = azs12language3;
    if(azs12language4!.isNotEmpty) map['AZ_S12_LANGUAGE_4'] = azs12language4;
    if(azs12language5!.isNotEmpty) map['AZ_S12_LANGUAGE_5'] = azs12language5;
    if(azs12language6!.isNotEmpty) map['AZ_S12_LANGUAGE_6'] = azs12language6;
    if(azs12language7!.isNotEmpty) map['AZ_S12_LANGUAGE_7'] = azs12language7;
    if(azs12language8!.isNotEmpty) map['AZ_S12_LANGUAGE_8'] = azs12language8;
    if(azs12language9!.isNotEmpty) map['AZ_S12_LANGUAGE_9'] = azs12language9;
    if(azs12language10!.isNotEmpty) map['AZ_S12_LANGUAGE_10'] = azs12language10;
    if(azs12language11!.isNotEmpty) map['AZ_S12_LANGUAGE_11'] = azs12language11;

    if(azs13fluencya1!.isNotEmpty) map['AZ_S13_FLUENCY_A_1'] = azs13fluencya1;
    if(azs13fluencya2!.isNotEmpty) map['AZ_S13_FLUENCY_A_2'] = azs13fluencya2;
    if(azs13fluencya3!.isNotEmpty) map['AZ_S13_FLUENCY_A_3'] = azs13fluencya3;
    if(azs13fluencya4!.isNotEmpty) map['AZ_S13_FLUENCY_A_4'] = azs13fluencya4;
    if(azs13fluencya5!.isNotEmpty) map['AZ_S13_FLUENCY_A_5'] = azs13fluencya5;
    if(azs13fluencya6!.isNotEmpty) map['AZ_S13_FLUENCY_A_6'] = azs13fluencya6;
    if(azs13fluencya7!.isNotEmpty) map['AZ_S13_FLUENCY_A_7'] = azs13fluencya7;
    if(azs13fluencya8!.isNotEmpty) map['AZ_S13_FLUENCY_A_8'] = azs13fluencya8;
    if(azs13fluencya9!.isNotEmpty) map['AZ_S13_FLUENCY_A_9'] = azs13fluencya9;
    if(azs13fluencya10!.isNotEmpty) map['AZ_S13_FLUENCY_A_10'] = azs13fluencya10;
    if(azs13fluencya11!.isNotEmpty) map['AZ_S13_FLUENCY_A_11'] = azs13fluencya11;
    if(azs13fluencya12!.isNotEmpty) map['AZ_S13_FLUENCY_A_12'] = azs13fluencya12;
    if(azs13fluencya13!.isNotEmpty) map['AZ_S13_FLUENCY_A_13'] = azs13fluencya13;
    if(azs13fluencya14!.isNotEmpty) map['AZ_S13_FLUENCY_A_14'] = azs13fluencya14;
    if(azs13fluencyb!.isNotEmpty) map['AZ_S13_FLUENCY_B'] = azs13fluencyb;

    map['AZ_S14_FLUENCY'] = azs14fluency;

    if(azs15visuospatial1!.isNotEmpty) map['AZ_S15_VISUOSPATIAL_1'] = azs15visuospatial1;
    if(azs15visuospatial2!.isNotEmpty) map['AZ_S15_VISUOSPATIAL_2'] = azs15visuospatial2;
    if(azs16visuospatial1!.isNotEmpty)  map['AZ_S16_VISUOSPATIAL_1'] = azs16visuospatial1;
    if(azs16visuospatial2!.isNotEmpty)  map['AZ_S16_VISUOSPATIAL_2'] = azs16visuospatial2;
    if(azs16visuospatial3!.isNotEmpty)  map['AZ_S16_VISUOSPATIAL_3'] = azs16visuospatial3;
    if(azs16visuospatial4!.isNotEmpty)  map['AZ_S16_VISUOSPATIAL_4'] = azs16visuospatial4;
    if(azs16visuospatial5!.isNotEmpty)  map['AZ_S16_VISUOSPATIAL_5'] = azs16visuospatial5;
    if(azs16visuospatial6!.isNotEmpty)  map['AZ_S16_VISUOSPATIAL_6'] = azs16visuospatial6;
    if(azs16visuospatial7!.isNotEmpty)  map['AZ_S16_VISUOSPATIAL_7'] = azs16visuospatial7;
    if(azs16visuospatial8!.isNotEmpty)  map['AZ_S16_VISUOSPATIAL_8'] = azs16visuospatial8;
    if(azs16visuospatial9!.isNotEmpty)  map['AZ_S16_VISUOSPATIAL_9'] = azs16visuospatial9;
    if(azs16visuospatial10!.isNotEmpty)   map['AZ_S16_VISUOSPATIAL_10'] = azs16visuospatial10;
    if(azs16visuospatial11!.isNotEmpty)  map['AZ_S16_VISUOSPATIAL_11'] = azs16visuospatial11;

    return map;
  }

}