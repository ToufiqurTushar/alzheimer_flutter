import '../question/questions_options.dart';
import 'measurement.dart';

class MeasurementResponse {
  MeasurementResponse({
      this.measurement, 
      this.details, 
      this.id, 
      this.uuid, 
      this.createdAt, 
      this.lastUpdated, 
      this.createdById, 
      this.updatedById,});

  MeasurementResponse.fromJson(dynamic json) {
    measurement = json['measurement'] != null ? Measurement.fromJson(json['measurement']) : null;
    details = json['details'] != null ? QuestionsAndOptions.fromJson(json['details']) : null;
    id = json['id'];
    uuid = json['uuid'];
    createdAt = json['created_at'];
    lastUpdated = json['last_updated'];
    createdById = json['created_by_id'];
    updatedById = json['updated_by_id'];
  }
  Measurement? measurement;
  QuestionsAndOptions? details;
  num? id;
  String? uuid;
  num? createdAt;
  num? lastUpdated;
  num? createdById;
  num? updatedById;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (measurement != null) {
      map['measurement'] = measurement?.toJson();
    }
    if (details != null) {
      map['details'] = details?.toJson();
    }
    map['id'] = id;
    map['uuid'] = uuid;
    map['created_at'] = createdAt;
    map['last_updated'] = lastUpdated;
    map['created_by_id'] = createdById;
    map['updated_by_id'] = updatedById;
    return map;
  }

  static List<MeasurementResponse> fromJsonList(List json) {
    return json.map((e) => MeasurementResponse.fromJson(e as Map)).toList();
  }
}