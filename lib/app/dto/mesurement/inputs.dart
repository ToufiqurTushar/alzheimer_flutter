class Inputs {
  Inputs({
      this.azs12language7, 
      this.azs2attention, 
      this.azs10language1b, 
      this.azs10language3a, 
      this.azs10language3b, 
      this.azs9naming12, 
      this.azs7attention3, 
      this.azs9naming20, 
      this.azs7attention2, 
      this.azs7attention1, 
      this.azs1orientationb, 
      this.azs9naming5, 
      this.azs1orientationc, 
      this.azs1orientationd, 
      this.azs1orientatione, 
      this.azs9naming6, 
      this.azs1orientationf, 
      this.azs9naming9, 
      this.azs9naming8, 
      this.azs11language3, 
      this.azs11language4, 
      this.azs11language1, 
      this.azs11language2, 
      this.azs13fluencya11, 
      this.azs1orientationa, 
      this.azs10language2a, 
      this.azs10language2b, 
      this.azs10language4a, 
      this.azs10language4b, 
      this.azs4attention, 
      this.azs3backward, 
      this.azs10language1a, 
      this.azs6memory3, 
      this.azs8naming2, 
      this.azs8naming1, 
      this.azs6memory1, 
      this.azs16visuospatial7, 
      this.azs3forward, 
      this.azs16visuospatial3, 
      this.azs8naming10, 
      this.azs5memory2, 
      this.azs6memory9, 
      this.azs5memory1, 
      this.azs8naming7, 
      this.azs5memory4, 
      this.azs12language2, 
      this.azs5memory3, 
      this.azs14fluency, 
      this.azs8naming4, 
      this.azs11language5, 
      this.azs5memory5, 
      this.azs12language5, 
      this.azs15visuospatial2, 
      this.azs15visuospatial1,});

  Inputs.fromJson(dynamic json) {
    azs12language7 = json['AZ_S12_LANGUAGE_7'];
    azs2attention = json['AZ_S2_ATTENTION'];
    azs10language1b = json['AZ_S10_LANGUAGE_1_B'];
    azs10language3a = json['AZ_S10_LANGUAGE_3_A'];
    azs10language3b = json['AZ_S10_LANGUAGE_3_B'];
    azs9naming12 = json['AZ_S9_NAMING_12'];
    azs7attention3 = json['AZ_S7_ATTENTION_3'];
    azs9naming20 = json['AZ_S9_NAMING_20'];
    azs7attention2 = json['AZ_S7_ATTENTION_2'];
    azs7attention1 = json['AZ_S7_ATTENTION_1'];
    azs1orientationb = json['AZ_S1_ORIENTATION_B'];
    azs9naming5 = json['AZ_S9_NAMING_5'];
    azs1orientationc = json['AZ_S1_ORIENTATION_C'];
    azs1orientationd = json['AZ_S1_ORIENTATION_D'];
    azs1orientatione = json['AZ_S1_ORIENTATION_E'];
    azs9naming6 = json['AZ_S9_NAMING_6'];
    azs1orientationf = json['AZ_S1_ORIENTATION_F'];
    azs9naming9 = json['AZ_S9_NAMING_9'];
    azs9naming8 = json['AZ_S9_NAMING_8'];
    azs11language3 = json['AZ_S11_LANGUAGE_3'];
    azs11language4 = json['AZ_S11_LANGUAGE_4'];
    azs11language1 = json['AZ_S11_LANGUAGE_1'];
    azs11language2 = json['AZ_S11_LANGUAGE_2'];
    azs13fluencya11 = json['AZ_S13_FLUENCY_A_11'];
    azs1orientationa = json['AZ_S1_ORIENTATION_A'];
    azs10language2a = json['AZ_S10_LANGUAGE_2_A'];
    azs10language2b = json['AZ_S10_LANGUAGE_2_B'];
    azs10language4a = json['AZ_S10_LANGUAGE_4_A'];
    azs10language4b = json['AZ_S10_LANGUAGE_4_B'];
    azs4attention = json['AZ_S4_ATTENTION'];
    azs3backward = json['AZ_S3_BACKWARD'];
    azs10language1a = json['AZ_S10_LANGUAGE_1_A'];
    azs6memory3 = json['AZ_S6_MEMORY_3'];
    azs8naming2 = json['AZ_S8_NAMING_2'];
    azs8naming1 = json['AZ_S8_NAMING_1'];
    azs6memory1 = json['AZ_S6_MEMORY_1'];
    azs16visuospatial7 = json['AZ_S16_VISUOSPATIAL_7'];
    azs3forward = json['AZ_S3_FORWARD'];
    azs16visuospatial3 = json['AZ_S16_VISUOSPATIAL_3'];
    azs8naming10 = json['AZ_S8_NAMING_10'];
    azs5memory2 = json['AZ_S5_MEMORY_2'];
    azs6memory9 = json['AZ_S6_MEMORY_9'];
    azs5memory1 = json['AZ_S5_MEMORY_1'];
    azs8naming7 = json['AZ_S8_NAMING_7'];
    azs5memory4 = json['AZ_S5_MEMORY_4'];
    azs12language2 = json['AZ_S12_LANGUAGE_2'];
    azs5memory3 = json['AZ_S5_MEMORY_3'];
    azs14fluency = json['AZ_S14_FLUENCY'];
    azs8naming4 = json['AZ_S8_NAMING_4'];
    azs11language5 = json['AZ_S11_LANGUAGE_5'];
    azs5memory5 = json['AZ_S5_MEMORY_5'];
    azs12language5 = json['AZ_S12_LANGUAGE_5'];
    azs15visuospatial2 = json['AZ_S15_VISUOSPATIAL_2'];
    azs15visuospatial1 = json['AZ_S15_VISUOSPATIAL_1'];
  }
  num? azs12language7;
  num? azs2attention;
  num? azs10language1b;
  num? azs10language3a;
  num? azs10language3b;
  num? azs9naming12;
  num? azs7attention3;
  num? azs9naming20;
  num? azs7attention2;
  num? azs7attention1;
  num? azs1orientationb;
  num? azs9naming5;
  num? azs1orientationc;
  num? azs1orientationd;
  num? azs1orientatione;
  num? azs9naming6;
  num? azs1orientationf;
  num? azs9naming9;
  num? azs9naming8;
  num? azs11language3;
  num? azs11language4;
  num? azs11language1;
  num? azs11language2;
  num? azs13fluencya11;
  num? azs1orientationa;
  num? azs10language2a;
  num? azs10language2b;
  num? azs10language4a;
  num? azs10language4b;
  num? azs4attention;
  num? azs3backward;
  num? azs10language1a;
  num? azs6memory3;
  num? azs8naming2;
  num? azs8naming1;
  num? azs6memory1;
  num? azs16visuospatial7;
  num? azs3forward;
  num? azs16visuospatial3;
  num? azs8naming10;
  num? azs5memory2;
  num? azs6memory9;
  num? azs5memory1;
  num? azs8naming7;
  num? azs5memory4;
  num? azs12language2;
  num? azs5memory3;
  num? azs14fluency;
  num? azs8naming4;
  num? azs11language5;
  num? azs5memory5;
  num? azs12language5;
  num? azs15visuospatial2;
  num? azs15visuospatial1;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['AZ_S12_LANGUAGE_7'] = azs12language7;
    map['AZ_S2_ATTENTION'] = azs2attention;
    map['AZ_S10_LANGUAGE_1_B'] = azs10language1b;
    map['AZ_S10_LANGUAGE_3_A'] = azs10language3a;
    map['AZ_S10_LANGUAGE_3_B'] = azs10language3b;
    map['AZ_S9_NAMING_12'] = azs9naming12;
    map['AZ_S7_ATTENTION_3'] = azs7attention3;
    map['AZ_S9_NAMING_20'] = azs9naming20;
    map['AZ_S7_ATTENTION_2'] = azs7attention2;
    map['AZ_S7_ATTENTION_1'] = azs7attention1;
    map['AZ_S1_ORIENTATION_B'] = azs1orientationb;
    map['AZ_S9_NAMING_5'] = azs9naming5;
    map['AZ_S1_ORIENTATION_C'] = azs1orientationc;
    map['AZ_S1_ORIENTATION_D'] = azs1orientationd;
    map['AZ_S1_ORIENTATION_E'] = azs1orientatione;
    map['AZ_S9_NAMING_6'] = azs9naming6;
    map['AZ_S1_ORIENTATION_F'] = azs1orientationf;
    map['AZ_S9_NAMING_9'] = azs9naming9;
    map['AZ_S9_NAMING_8'] = azs9naming8;
    map['AZ_S11_LANGUAGE_3'] = azs11language3;
    map['AZ_S11_LANGUAGE_4'] = azs11language4;
    map['AZ_S11_LANGUAGE_1'] = azs11language1;
    map['AZ_S11_LANGUAGE_2'] = azs11language2;
    map['AZ_S13_FLUENCY_A_11'] = azs13fluencya11;
    map['AZ_S1_ORIENTATION_A'] = azs1orientationa;
    map['AZ_S10_LANGUAGE_2_A'] = azs10language2a;
    map['AZ_S10_LANGUAGE_2_B'] = azs10language2b;
    map['AZ_S10_LANGUAGE_4_A'] = azs10language4a;
    map['AZ_S10_LANGUAGE_4_B'] = azs10language4b;
    map['AZ_S4_ATTENTION'] = azs4attention;
    map['AZ_S3_BACKWARD'] = azs3backward;
    map['AZ_S10_LANGUAGE_1_A'] = azs10language1a;
    map['AZ_S6_MEMORY_3'] = azs6memory3;
    map['AZ_S8_NAMING_2'] = azs8naming2;
    map['AZ_S8_NAMING_1'] = azs8naming1;
    map['AZ_S6_MEMORY_1'] = azs6memory1;
    map['AZ_S16_VISUOSPATIAL_7'] = azs16visuospatial7;
    map['AZ_S3_FORWARD'] = azs3forward;
    map['AZ_S16_VISUOSPATIAL_3'] = azs16visuospatial3;
    map['AZ_S8_NAMING_10'] = azs8naming10;
    map['AZ_S5_MEMORY_2'] = azs5memory2;
    map['AZ_S6_MEMORY_9'] = azs6memory9;
    map['AZ_S5_MEMORY_1'] = azs5memory1;
    map['AZ_S8_NAMING_7'] = azs8naming7;
    map['AZ_S5_MEMORY_4'] = azs5memory4;
    map['AZ_S12_LANGUAGE_2'] = azs12language2;
    map['AZ_S5_MEMORY_3'] = azs5memory3;
    map['AZ_S14_FLUENCY'] = azs14fluency;
    map['AZ_S8_NAMING_4'] = azs8naming4;
    map['AZ_S11_LANGUAGE_5'] = azs11language5;
    map['AZ_S5_MEMORY_5'] = azs5memory5;
    map['AZ_S12_LANGUAGE_5'] = azs12language5;
    map['AZ_S15_VISUOSPATIAL_2'] = azs15visuospatial2;
    map['AZ_S15_VISUOSPATIAL_1'] = azs15visuospatial1;
    return map;
  }

}