class Location {
  Location({
      this.latitude, 
      this.longitude, 
      this.altitude,});

  Location.fromJson(dynamic json) {
    latitude = json['latitude'] != null ? json['latitude'].cast<String>() : [];
    longitude = json['longitude'] != null ? json['longitude'].cast<String>() : [];
    altitude = json['altitude'] != null ? json['altitude'].cast<String>() : [];
  }
  List<String>? latitude;
  List<String>? longitude;
  List<String>? altitude;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['latitude'] = latitude;
    map['longitude'] = longitude;
    map['altitude'] = altitude;
    return map;
  }

}