class UserBloodGroup {
  UserBloodGroup({
      this.id, 
      this.labelEn, 
      this.labelBn, 
      this.colorCode,});

  UserBloodGroup.fromJson(dynamic json) {
    id = json['id'];
    labelEn = json['labelEn'];
    labelBn = json['labelBn'];
    colorCode = json['colorCode'];
  }
  num? id;
  String? labelEn;
  String? labelBn;
  String? colorCode;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['labelEn'] = labelEn;
    map['labelBn'] = labelBn;
    map['colorCode'] = colorCode;
    return map;
  }

}