import 'inputs.dart';
import 'user_blood_group.dart';
import 'result.dart';
import 'inputs_with_unit.dart';
import 'location.dart';

class Measurement {
  Measurement({
      this.inputs, 
      this.meta, 
      this.ecgsymps, 
      this.outcome, 
      this.id, 
      this.uuid, 
      this.createdAt, 
      this.lastUpdated, 
      this.createdById, 
      this.updatedById, 
      this.memberId, 
      this.appId, 
      this.clientId, 
      this.userId, 
      this.userUuid, 
      this.userFullName, 
      this.userFullNameBn, 
      this.userPhoneNumber, 
      this.userDateOfBirth, 
      this.userBloodGroup, 
      this.gender, 
      this.userEmail, 
      this.userNid, 
      this.measurementTypeCodeId, 
      this.measurementTypeName, 
      this.measurementTypeCode, 
      this.bundleId, 
      this.result, 
      this.measuredAt, 
      this.inputsWithUnit, 
      this.ecgGraphValue, 
      this.tag, 
      this.location, 
      this.offline, 
      this.createdByUuid, 
      this.companyId,});

  Measurement.fromJson(dynamic json) {
    inputs = json['inputs'] != null ? Inputs.fromJson(json['inputs']) : null;
    meta = json['meta'] != null ? json['meta'].cast<String>() : [];
    ecgsymps = json['ecgsymps'] != null ? json['ecgsymps'].cast<String>() : [];
    outcome = json['outcome'];
    id = json['id'];
    uuid = json['uuid'];
    createdAt = json['created_at'];
    lastUpdated = json['last_updated'];
    createdById = json['created_by_id'];
    updatedById = json['updated_by_id'] != null ? json['updated_by_id'].cast<String>() : [];
    memberId = json['member_id'] != null ? json['member_id'].cast<String>() : [];
    appId = json['app_id'] != null ? json['app_id'].cast<String>() : [];
    clientId = json['client_id'] != null ? json['client_id'].cast<String>() : [];
    userId = json['user_id'];
    userUuid = json['user_uuid'];
    userFullName = json['user_full_name'];
    userFullNameBn = json['user_full_name_bn'];
    userPhoneNumber = json['user_phone_number'];
    userDateOfBirth = json['user_date_of_birth'];
    userBloodGroup = json['user_blood_group'] != null ? UserBloodGroup.fromJson(json['user_blood_group']) : null;
    gender = json['gender'];
    userEmail = json['user_email'];
    userNid = json['user_nid'] != null ? json['user_nid'].cast<String>() : [];
    measurementTypeCodeId = json['measurement_type_code_id'];
    measurementTypeName = json['measurement_type_name'];
    measurementTypeCode = json['measurement_type_code'];
    bundleId = json['bundle_id'] != null ? json['bundle_id'].cast<String>() : [];
    result = json['result'] != null ? Result.fromJson(json['result']) : null;
    measuredAt = json['measured_at'];
    inputsWithUnit = json['inputs_with_unit'] != null ? InputsWithUnit.fromJson(json['inputs_with_unit']) : null;
    ecgGraphValue = json['ecg_graph_value'] != null ? json['ecg_graph_value'].cast<String>() : [];
    tag = json['tag'] != null ? json['tag'].cast<String>() : [];
    location = json['location'] != null ? Location.fromJson(json['location']) : null;
    offline = json['offline'];
    createdByUuid = json['created_by_uuid'];
    companyId = json['company_id'] != null ? json['company_id'].cast<String>() : [];
  }
  Inputs? inputs;
  List<String>? meta;
  List<String>? ecgsymps;
  num? outcome;
  num? id;
  String? uuid;
  num? createdAt;
  num? lastUpdated;
  num? createdById;
  List<String>? updatedById;
  List<String>? memberId;
  List<String>? appId;
  List<String>? clientId;
  num? userId;
  String? userUuid;
  String? userFullName;
  String? userFullNameBn;
  String? userPhoneNumber;
  num? userDateOfBirth;
  UserBloodGroup? userBloodGroup;
  num? gender;
  String? userEmail;
  List<String>? userNid;
  num? measurementTypeCodeId;
  String? measurementTypeName;
  String? measurementTypeCode;
  List<String>? bundleId;
  Result? result;
  num? measuredAt;
  InputsWithUnit? inputsWithUnit;
  List<String>? ecgGraphValue;
  List<String>? tag;
  Location? location;
  bool? offline;
  String? createdByUuid;
  List<String>? companyId;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (inputs != null) {
      map['inputs'] = inputs?.toJson();
    }
    map['meta'] = meta;
    map['ecgsymps'] = ecgsymps;
    map['outcome'] = outcome;
    map['id'] = id;
    map['uuid'] = uuid;
    map['created_at'] = createdAt;
    map['last_updated'] = lastUpdated;
    map['created_by_id'] = createdById;
    map['updated_by_id'] = updatedById;
    map['member_id'] = memberId;
    map['app_id'] = appId;
    map['client_id'] = clientId;
    map['user_id'] = userId;
    map['user_uuid'] = userUuid;
    map['user_full_name'] = userFullName;
    map['user_full_name_bn'] = userFullNameBn;
    map['user_phone_number'] = userPhoneNumber;
    map['user_date_of_birth'] = userDateOfBirth;
    if (userBloodGroup != null) {
      map['user_blood_group'] = userBloodGroup?.toJson();
    }
    map['gender'] = gender;
    map['user_email'] = userEmail;
    map['user_nid'] = userNid;
    map['measurement_type_code_id'] = measurementTypeCodeId;
    map['measurement_type_name'] = measurementTypeName;
    map['measurement_type_code'] = measurementTypeCode;
    map['bundle_id'] = bundleId;
    if (result != null) {
      map['result'] = result?.toJson();
    }
    map['measured_at'] = measuredAt;
    if (inputsWithUnit != null) {
      map['inputs_with_unit'] = inputsWithUnit?.toJson();
    }
    map['ecg_graph_value'] = ecgGraphValue;
    map['tag'] = tag;
    if (location != null) {
      map['location'] = location?.toJson();
    }
    map['offline'] = offline;
    map['created_by_uuid'] = createdByUuid;
    map['company_id'] = companyId;
    return map;
  }

}