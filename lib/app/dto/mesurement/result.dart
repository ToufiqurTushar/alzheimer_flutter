class Result {
  Result({
      this.value, 
      this.status, 
      this.severity, 
      this.remarks, 
      this.suggestion, 
      this.suggestionBn, 
      this.engAdvice, 
      this.bnAdvice, 
      this.colorCode, 
      this.statusBn,});

  Result.fromJson(dynamic json) {
    value = json['value'];
    status = json['status'];
    severity = json['severity'];
    remarks = json['remarks'] != null ? json['remarks'].cast<String>() : [];
    suggestion = json['suggestion'];
    suggestionBn = json['suggestion_bn'];
    engAdvice = json['eng_advice'];
    bnAdvice = json['bn_advice'];
    colorCode = json['color_code'];
    statusBn = json['status_bn'];
  }
  num? value;
  String? status;
  String? severity;
  List<String>? remarks;
  String? suggestion;
  String? suggestionBn;
  String? engAdvice;
  String? bnAdvice;
  String? colorCode;
  String? statusBn;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['value'] = value;
    map['status'] = status;
    map['severity'] = severity;
    map['remarks'] = remarks;
    map['suggestion'] = suggestion;
    map['suggestion_bn'] = suggestionBn;
    map['eng_advice'] = engAdvice;
    map['bn_advice'] = bnAdvice;
    map['color_code'] = colorCode;
    map['status_bn'] = statusBn;
    return map;
  }

}