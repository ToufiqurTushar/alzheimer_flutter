class LoggedUserDTO {
  List<String>? aud;
  String? phone;
  String? userName;
  List<String>? scope;
  String? lastName;
  int? id;
  int? exp;
  String? firstName;
  List<String>? authorities;
  String? jti;
  String? clientId;
  String? username;
  UserProfile? loggedUserProfile;


  LoggedUserDTO({this.aud,
    this.phone,
    this.userName,
    this.scope,
    this.lastName,
    this.id,
    this.exp,
    this.firstName,
    this.authorities,
    this.jti,
    this.clientId,
    this.username,
    this.loggedUserProfile});

  LoggedUserDTO.fromJson(Map<String, dynamic> json) {
    if (json['aud'] != null) {
      aud = json['aud'].cast<String>();
    }
    phone = json['phone'];
    userName = json['user_name'];
    if (json['scope'] != null) {
      scope = json['scope'].cast<String>();
    }

    lastName = json['last_name'];
    id = json['id'];
    exp = json['exp'];
    firstName = json['first_name'];
    authorities = json['authorities'] != null
        ? json['authorities'].cast<String>()
        : [];
    jti = json['jti'];
    clientId = json['client_id'];
    username = json['username'];
    loggedUserProfile = json['user_profile'] != null
        ? UserProfile.fromJson(json['user_profile'])
        : null;

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['aud'] = aud;
    data['phone'] = phone;
    data['user_name'] = userName;
    data['scope'] = scope;
    data['last_name'] = lastName;
    data['id'] = id;
    data['exp'] = exp;
    data['first_name'] = firstName;
    data['authorities'] = authorities;
    data['jti'] = jti;
    data['client_id'] = clientId;
    data['username'] = username;
    data['user_profile'] = loggedUserProfile?.toJson();
    return data;
  }

  String getFullName() {
    return "$firstName ${lastName ?? ''} ";
  }
}


class UserProfile {
  String? additionalNote;
  String? birthCertificateNumber;
  String? birthPlace;
  int? birthday;
  int? bloodGroup;
  String? companyEmployeeId;
  String? companyJobTitle;
  bool? coronaAffectedStatus;
  String? dateOfConception;
  String? dateOfDeath;
  bool? dead;
  bool? deadDueToCorona;
  bool? diabetic;
  List<DisabilitiesDTO>? disabilities;
  bool? disable;
  bool? doesGoToProsperitySchool;
  int? eddDate;
  int? educationInstitute;
  int? educationQualification;
  String? email;
  bool? familyDiabetes;
  bool? familyHyperTension;
  String? fatherName;
  String? fatherNameBn;
  String? firstName;
  String? firstNameBn;
  bool? freedomFighter;
  int? gender;
  bool? govtOfficial;
  bool? hadStroke;
  bool? hasAsthma;
  bool? hasCancer;
  bool? hasCardiovascularDisease;
  bool? hasCopd;
  bool? hasElectricity;
  bool? hasHealthCard;
  bool? hasKidneyDisease;
  double? healthCardCost;
  String? healthCardIssueDate;
  String? healthCardNumber;
  double? height;
  bool? hypertensive;
  int? id;
  bool? isDeadDueToCorona;
  bool? isGovtOfficial;
  bool? isPoor;
  String? lastName;
  String? lastNameBn;
  int? lmpDate;
  int? maritalStatus;
  String? motherName;
  String? motherNameBn;
  String? nameBn;
  String? nationalId;
  String? nationality;
  int? numberOfTeeth;
  bool? oldAgePension;
  String? organizationName;
  String? passportNumber;
  int? permanentAddressId;
  String? phone;
  String? photo;
  bool? poor;
  int? pregnancyIdentificationDate;
  bool? pregnant;
  int? presentAddressId;
  int? primaryOccupation;
  String? prosperitySchoolCenterNumber;
  int? relationWithHouseholdHead;
  int? religion;
  bool? schoolGoingChildren;
  int? secondaryOccupation;
  List<SymptomsAfterTakenVaccineDTO>? symptomsAfterTakenVaccine;
  int? userId;
  String? uuid;
  bool? vaccineTakenStatus;
  double? weight;

  UserProfile(
      {this.additionalNote,
        this.relationWithHouseholdHead,
        this.birthCertificateNumber,
        this.birthPlace,
        this.birthday,
        this.bloodGroup,
        this.companyEmployeeId,
        this.companyJobTitle,
        this.coronaAffectedStatus,
        this.dateOfConception,
        this.dateOfDeath,
        this.dead,
        this.deadDueToCorona,
        this.diabetic,
        this.disabilities,
        this.disable,
        this.doesGoToProsperitySchool,
        this.eddDate,
        this.educationInstitute,
        this.educationQualification,
        this.email,
        this.familyDiabetes,
        this.familyHyperTension,
        this.fatherName,
        this.fatherNameBn,
        this.firstName,
        this.firstNameBn,
        this.freedomFighter,
        this.gender,
        this.govtOfficial,
        this.hadStroke,
        this.hasAsthma,
        this.hasCancer,
        this.hasCardiovascularDisease,
        this.hasCopd,
        this.hasElectricity,
        this.hasHealthCard,
        this.hasKidneyDisease,
        this.healthCardCost,
        this.healthCardIssueDate,
        this.healthCardNumber,
        this.height,
        this.hypertensive,
        this.id,
        this.isDeadDueToCorona,
        this.isGovtOfficial,
        this.isPoor,
        this.lastName,
        this.lastNameBn,
        this.lmpDate,
        this.maritalStatus,
        this.motherName,
        this.motherNameBn,
        this.nameBn,
        this.nationalId,
        this.nationality,
        this.numberOfTeeth,
        this.oldAgePension,
        this.organizationName,
        this.passportNumber,
        this.permanentAddressId,
        this.phone,
        this.photo,
        this.poor,
        this.pregnancyIdentificationDate,
        this.pregnant,
        this.presentAddressId,
        this.primaryOccupation,
        this.prosperitySchoolCenterNumber,
        this.religion,
        this.schoolGoingChildren,
        this.secondaryOccupation,
        this.symptomsAfterTakenVaccine,
        this.userId,
        this.uuid,
        this.vaccineTakenStatus,
        this.weight});

  UserProfile.fromJson(Map<String, dynamic> json) {
    additionalNote = json['additional_note'];
    birthCertificateNumber = json['birth_certificate_number'];
    birthPlace = json['birth_place'];
    birthday = json['birthday'];
    bloodGroup = json['blood_group'];
    companyEmployeeId = json['company_employee_id'];
    companyJobTitle = json['company_job_title'];
    coronaAffectedStatus = json['corona_affected_status'];
    dateOfConception = json['date_of_conception'];
    dateOfDeath = json['date_of_death'];
    dead = json['dead'];
    deadDueToCorona = json['deadDueToCorona'];
    diabetic = json['diabetic'];
    if (json['disabilities'] != null) {
      disabilities = <DisabilitiesDTO>[];
      json['disabilities'].forEach((v) {
        disabilities!.add(DisabilitiesDTO.fromJson(v));
      });
    }
    disable = json['disable'];
    doesGoToProsperitySchool = json['does_go_to_prosperity_school'];
    eddDate = json['edd_date'];
    educationInstitute = json['education_institute'];
    educationQualification = json['education_qualification'];
    email = json['email'];
    familyDiabetes = json['family_diabetes'];
    familyHyperTension = json['family_hyper_tension'];
    fatherName = json['father_name'];
    fatherNameBn = json['father_name_bn'];
    firstName = json['first_name'];
    firstNameBn = json['first_name_bn'];
    freedomFighter = json['freedom_fighter'];
    gender = json['gender'];
    govtOfficial = json['govtOfficial'];
    hadStroke = json['had_stroke'];
    hasAsthma = json['has_asthma'];
    hasCancer = json['has_cancer'];
    hasCardiovascularDisease = json['has_cardiovascular_disease'];
    hasCopd = json['has_copd'];
    hasElectricity = json['has_electricity'];
    hasHealthCard = json['has_health_card'];
    hasKidneyDisease = json['has_kidney_disease'];
    healthCardCost = json['health_card_cost'];
    healthCardIssueDate = json['health_card_issue_date'];
    healthCardNumber = json['health_card_number'];
    height = json['height'];
    hypertensive = json['hypertensive'];
    id = json['id'];
    isDeadDueToCorona = json['is_dead_due_to_corona'];
    isGovtOfficial = json['is_govt_official'];
    isPoor = json['is_poor'];
    lastName = json['last_name'];
    lastNameBn = json['last_name_bn'];
    lmpDate = json['lmp_date'];
    maritalStatus = json['marital_status'];

    motherName = json['mother_name'];
    motherNameBn = json['mother_name_bn'];
    nameBn = json['name_bn'];
    nationalId = json['national_id'];
    nationality = json['nationality'];
    numberOfTeeth = json['number_of_teeth'];
    oldAgePension = json['old_age_pension'];
    organizationName = json['organization'];
    passportNumber = json['passport_number'];
    permanentAddressId = json['permanent_address_id'];
    phone = json['phone'];
    photo = json['photo'];
    poor = json['poor'];
    pregnancyIdentificationDate = json['pregnancy_identification_date'];
    pregnant = json['pregnant'];
    presentAddressId = json['present_address_id'];
    primaryOccupation = json['primary_occupation'];
    prosperitySchoolCenterNumber = json['prosperity_school_center_number'];
    relationWithHouseholdHead = json['relation_with_household_head'];
    religion = json['religion'];
    schoolGoingChildren = json['school_going_children'];
    secondaryOccupation = json['secondary_occupation'];
    if (json['symptoms_after_taken_vaccine'] != null) {
      symptomsAfterTakenVaccine = <SymptomsAfterTakenVaccineDTO>[];
      json['symptoms_after_taken_vaccine'].forEach((v) {
        symptomsAfterTakenVaccine!
            .add( SymptomsAfterTakenVaccineDTO.fromJson(v));
      });
    }
    userId = json['user_id'];
    uuid = json['uuid'];
    vaccineTakenStatus = json['vaccine_taken_status'];
    weight = json['weight'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['additional_note'] = additionalNote;
    data['birth_certificate_number'] = birthCertificateNumber;
    data['birth_place'] = birthPlace;
    data['birthday'] = birthday;
    data['company_employee_id'] = companyEmployeeId;
    data['company_job_title'] = companyJobTitle;
    data['corona_affected_status'] = coronaAffectedStatus;
    data['date_of_conception'] = dateOfConception;
    data['date_of_death'] = dateOfDeath;
    data['dead'] = dead;
    data['deadDueToCorona'] = deadDueToCorona;
    data['diabetic'] = diabetic;
    if (disabilities != null) {
      data['disabilities'] = disabilities!.map((v) => v.toJson()).toList();
    }
    data['disable'] = disable;
    data['relation_with_household_head'] = relationWithHouseholdHead;
    data['does_go_to_prosperity_school'] = doesGoToProsperitySchool;
    data['edd_date'] = eddDate;
    data['education_institute'] = educationInstitute;
    data['education_qualification'] = educationQualification;
    data['email'] = email;
    data['family_diabetes'] = familyDiabetes;
    data['family_hyper_tension'] = familyHyperTension;
    data['father_name'] = fatherName;
    data['father_name_bn'] = fatherNameBn;
    data['first_name'] = firstName;
    data['first_name_bn'] = firstNameBn;
    data['freedom_fighter'] = freedomFighter;
    data['gender'] = gender;
    data['govtOfficial'] = govtOfficial;
    data['had_stroke'] = hadStroke;
    data['has_asthma'] = hasAsthma;
    data['has_cancer'] = hasCancer;
    data['has_cardiovascular_disease'] = hasCardiovascularDisease;
    data['has_copd'] = hasCopd;
    data['has_electricity'] = hasElectricity;
    data['has_health_card'] = hasHealthCard;
    data['has_kidney_disease'] = hasKidneyDisease;
    data['health_card_cost'] = healthCardCost;
    data['health_card_issue_date'] = healthCardIssueDate;
    data['health_card_number'] = healthCardNumber;
    data['height'] = height;
    data['hypertensive'] = hypertensive;
    data['id'] = id;
    data['is_dead_due_to_corona'] = isDeadDueToCorona;
    data['is_govt_official'] = isGovtOfficial;
    data['is_poor'] = isPoor;
    data['last_name'] = lastName;
    data['last_name_bn'] = lastNameBn;
    data['lmp_date'] = lmpDate;
    data['mother_name'] = motherName;
    data['mother_name_bn'] = motherNameBn;
    data['name_bn'] = nameBn;
    data['national_id'] = nationalId;
    data['nationality'] = nationality;
    data['number_of_teeth'] = numberOfTeeth;
    data['old_age_pension'] = oldAgePension;
    data['organization'] = organizationName;
    data['passport_number'] = passportNumber;
    data['permanent_address_id'] = permanentAddressId;
    data['phone'] = phone;
    data['photo'] = photo;
    data['poor'] = poor;
    data['pregnancy_identification_date'] = pregnancyIdentificationDate;
    data['pregnant'] = pregnant;
    data['present_address_id'] = presentAddressId;
    data['primary_occupation'] = primaryOccupation;
    data['prosperity_school_center_number'] = prosperitySchoolCenterNumber;
    data['religion'] = religion;
    data['school_going_children'] = schoolGoingChildren;
    data['secondary_occupation'] = secondaryOccupation;
    if (symptomsAfterTakenVaccine != null) {
      data['symptoms_after_taken_vaccine'] =
          symptomsAfterTakenVaccine!.map((v) => v.toJson()).toList();
    }
    data['user_id'] = userId;
    data['uuid'] = uuid;
    data['vaccine_taken_status'] = vaccineTakenStatus;
    data['weight'] = weight;
    return data;
  }
  String getProfilePhotoUrl(){
    return photo??'';
    // return "${Environment.environment?.baseUrl}$photo";
    // return "https://ask-mortgage-question.sarojroy.com/files/questions/image-picker-96af9434-ee13-4f50-81bb-2a4521cbd754-46982-00003ca5dcc6c835-630c46872c28b610610174.jpg";
  }

  String getNationalId(){
    return (nationalId!=null && nationalId!.trim().isNotEmpty )?nationalId!:"N/A";
  }

  String getLastName(){
    return (lastName!=null && lastName!.trim().isNotEmpty )?lastName!:"N/A";
  }
}



class DisabilitiesDTO {
  String? name;

  DisabilitiesDTO({this.name});

  DisabilitiesDTO.fromJson(Map<String, dynamic> json) {
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['name'] = name;
    return data;
  }
}


class SymptomsAfterTakenVaccineDTO {
  String? description;
  String? name;

  SymptomsAfterTakenVaccineDTO({this.description, this.name});

  SymptomsAfterTakenVaccineDTO.fromJson(Map<String, dynamic> json) {
    description = json['description'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['description'] = description;
    data['name'] = name;
    return data;
  }
}

