import '../../../dto/mesurement/measurement_response.dart';

class ToScreeningListItem {
  ToScreeningListItem({
      required this.mesurementItem
  });

  MeasurementResponse mesurementItem;

}