import 'package:alzheimer_flutter/app/modules/screening_detail_result/dto/to_screening_details_result.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:responsive_grid/responsive_grid.dart';

import '../../../widgets/common_widget.dart';
import '../../../routes/app_pages.dart';
import '../../screening_final_result/dto/to_screening_final_result.dart';
import '../controllers/screening_list_item_controller.dart';

class AZScreeningListItemView extends GetView<AZScreeningListItemController> {
  static const PATH = '/screening-list-item';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFFf5eaf0),
      appBar: AppBar(
        title: Text('az_alzheimer'.tr),
        centerTitle: true,
        backgroundColor: Colors.pink,
      ),
      body: Column(
        children: [
          ProfileHeader(context: context, image: controller.pref.profilePicture, name: controller.pref.fullName??'User', mobile: controller.pref.phone??'Phone'),
          Expanded(
              child:  SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.only(left:10, right:10),
                  child: _buildGridLayout(context),
                ),
              )
          )
        ],
      ),
    );
  }

  Widget _buildGridLayout(context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        ResponsiveGridRow(
          children: [
            PageImage(image: 'assets/images/alzheimer/home_screen.png', height: 180, margin: const EdgeInsets.only(bottom: 20, top: 20)),
            CardWithImageText(image: 'assets/images/alzheimer/details_result.png', text:'az_detail_result'.tr, size:6, onTap:(){
              Get.toNamed(Routes.SCREENING_DETAIL_RESULT, arguments: ToScreeningDetailsResult(mesurementItem: controller.measurement));
            }),
            CardWithImageText(image: 'assets/images/alzheimer/final_result.png', text:'az_final_result'.tr, size:6, onTap:(){
              Get.toNamed(Routes.SCREENING_FINAL_RESULT, arguments: ToScreeningFinalResult(mesurementItem: controller.measurement));
            }),

            CardWithImageText(image: 'assets/images/alzheimer/home.png', text:'az_home'.tr, size:12, onTap:(){
              Get.back();
              Get.back();
            }),
          ],
        ),
      ],
    );
  }
}
