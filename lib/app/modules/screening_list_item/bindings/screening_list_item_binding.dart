import 'package:get/get.dart';

import '../controllers/screening_list_item_controller.dart';

class AZScreeningListItemBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<AZScreeningListItemController>(
      () => AZScreeningListItemController(),
    );
  }
}
