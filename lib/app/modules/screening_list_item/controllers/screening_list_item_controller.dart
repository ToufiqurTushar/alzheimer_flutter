import 'package:alzheimer_flutter/app/dto/mesurement/measurement_response.dart';
import 'package:alzheimer_flutter/app/modules/screening_list_item/dto/to_screening_list_item.dart';
import 'package:get/get.dart';

import '../../../../core/preference/preference.dart';

class AZScreeningListItemController extends GetxController {
  ToScreeningListItem args = Get.arguments;
  final AZPreference pref = Get.find();

  late MeasurementResponse measurement;

  @override
  void onInit() {
    super.onInit();
    measurement = args.mesurementItem;
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

}
