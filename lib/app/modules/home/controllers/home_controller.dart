
import 'package:alzheimer_flutter/core/preference/preference.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class AZHomeController extends GetxController {
  final AZPreference pref = Get.find();

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

}
