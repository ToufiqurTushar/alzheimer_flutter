import 'package:alzheimer_flutter/core/preference/preference.dart';
import 'package:get/get.dart';

import '../../../../core/providers/api_provider.dart';
import '../controllers/home_controller.dart';

class AZHomeBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<AZHomeController>(
      () => AZHomeController(),
    );
    Get.lazyPut<ApiProvider>(
      () => ApiProvider(), fenix: true
    );
    Get.lazyPut<AZPreference>(
      () => AZPreference(), fenix: true
    );
  }
}
