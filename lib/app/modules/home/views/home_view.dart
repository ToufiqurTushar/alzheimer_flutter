import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../controllers/home_controller.dart';
import 'package:responsive_grid/responsive_grid.dart';
import '../../../widgets/common_widget.dart';
import '../../../routes/app_pages.dart';

class AZHomeView extends GetView<AZHomeController> {
  static const PATH = '/alzheimer-home';
  const AZHomeView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: const Color(0xFFf5eaf0),
        appBar: AppBar(
          title: const Text('Alzheimer'),
          centerTitle: true,
          backgroundColor: Colors.pink,
        ),
        body: Column(
          children: [
            ProfileHeader(context: context, image: controller.pref.profilePicture, name: controller.pref.fullName??'User', mobile: controller.pref.phone??'Phone'),
            Expanded(
                child:  SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.only(left:10, right:10),
                    child: _buildGridLayout(context),
                  ),
                )
            )
          ],
        )
    );
  }

  Widget _buildGridLayout(context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        ResponsiveGridRow(
          children: [
            PageImage(image: 'assets/images/alzheimer/home_screen.png', height: 180, margin: const EdgeInsets.only(bottom: 20, top: 20)),
            CardWithImageText(image: 'assets/images/alzheimer/screening.png',
                text: 'Screening',
                size: 6,
                onTap: () {
                  Get.toNamed(Routes.ADD_SCREENING);
                }),
            CardWithImageText(image: 'assets/images/alzheimer/screening_list.png',
                text: 'Screening List',
                size: 6,
                onTap: () {
                  Get.toNamed(Routes.SCREENING_LIST);
                }),
            CardWithImageText(image: 'assets/images/alzheimer/refer.png',
                text: 'Refer',
                size: 6,
                onTap: () {
                  //Get.toNamed(Routes.REFER);
                }),

            CardWithImageText(image: 'assets/images/alzheimer/awareness_video.png',
                text: 'Awareness Video',
                size: 6,
                onTap: () {
                  //Get.toNamed(Routes.AWARENESS_VIDEO);
                }),

          ],
        ),
      ],
    );
  }
}
