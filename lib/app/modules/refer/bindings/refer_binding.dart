import 'package:get/get.dart';

import '../../../../core/preference/preference.dart';
import '../controllers/refer_controller.dart';

class AZReferBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ReferController>(
      () => ReferController(),
    );
    Get.lazyPut<AZPreference>(
       () => AZPreference(), fenix: true
    );
  }
}
