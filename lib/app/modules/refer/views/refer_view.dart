import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/refer_controller.dart';

class AZReferView extends GetView<ReferController> {
  static const PATH = '/refer';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Refer'),
        centerTitle: true,
      ),
      body: Center(
        child: Text(
          'AZReferView is working',
          style: TextStyle(fontSize: 20),
        ),
      ),
    );
  }
}
