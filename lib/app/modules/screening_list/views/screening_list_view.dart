import 'package:alzheimer_flutter/app/modules/screening_list_item/dto/to_screening_list_item.dart';
import 'package:alzheimer_flutter/core/extensions/string_extension.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../../../../core/utils/methods.dart';
import '../../../../core/widgets/body.dart';
import '../../../constants/constant.dart';
import '../../../dto/mesurement/measurement_response.dart';
import '../../../enum/data_state.dart';
import '../../../enum/severity_enum.dart';
import '../../../routes/app_pages.dart';
import '../../../widgets/common_widget.dart';
import '../controllers/screening_list_controller.dart';

class AZScreeningListView extends GetView<AZScreeningListController> {
  static const PATH = '/screening-list';
  @override
  Widget build(BuildContext context) {
    return Obx(()=>Scaffold(
      backgroundColor: const Color(0xFFf5eaf0),
      appBar: AppBar(
        title: Text('az_screening_list'.tr),
        centerTitle: true,
        backgroundColor: Colors.pink,
      ),
      body: buildBody(context),
    ));
  }

  buildBody(BuildContext context) {
    switch(controller.dataState.value) {
      case DataState.LOADING: return LoadingBody();
      case DataState.ERROR: return ReloadBody(()=> controller.getData());
      case DataState.EMPTY: return EmptyBody();
      case DataState.LOADED: return builListContainer(context);
      default: return ReloadBody(()=> controller.getData());
    }
  }

  builListContainer(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: ListView.builder(
          itemCount:controller.measurementList.length,
          itemBuilder: (context, index) {
            MeasurementResponse mesurementItem = controller.measurementList[index];
            var title = SeverityEnum.getLocaleLabelByLabel(mesurementItem.measurement!.result!.severity!);
            var subtitle = trNmuber(DateFormat(DateConstant.hma).format(DateTime.fromMillisecondsSinceEpoch(mesurementItem.measurement!.measuredAt!.toInt())));
            var date = trNmuber(DateFormat(DateConstant.dMy).format(DateTime.fromMillisecondsSinceEpoch(mesurementItem.measurement!.measuredAt!.toInt())));
            return ScreeningListItem(context: context, title: title, subtitle: subtitle, color: mesurementItem.measurement!.result!.colorCode!.toColor(), date: date , onTap:(){
              Get.toNamed(Routes.SCREENING_LIST_ITEM, arguments: ToScreeningListItem(mesurementItem: mesurementItem));
            });
          }
      ),
    );
  }
}
