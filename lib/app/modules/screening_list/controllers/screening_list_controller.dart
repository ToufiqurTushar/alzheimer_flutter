import 'package:alzheimer_flutter/app/dto/mesurement/measurement_response.dart';
import 'package:get/get.dart';

import '../../../../core/preference/preference.dart';
import '../../../enum/data_state.dart';
import '../../../repositories/alzheimer_repository.dart';

class AZScreeningListController extends GetxController {
  final AlzheimerRepository alzheimerRepository = Get.find();
  final AZPreference pref = Get.find();

  final measurementList = RxList<MeasurementResponse>();

  @override
  void onInit() {
    super.onInit();
    getData();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  final dataState = DataState.LOADING.obs;
  getData() async {
    try {
      dataState(DataState.LOADING);
      List<MeasurementResponse>? responseDto = await alzheimerRepository.getAllAlzheimerMesurement(userId: pref.userId);
      if(responseDto != null && responseDto.isNotEmpty) {
        measurementList.clear();
        measurementList.addAll(responseDto.reversed.toList());
        dataState(DataState.LOADED);
      } else if(responseDto != null && responseDto.isEmpty){
        dataState(DataState.EMPTY);
      } else {
        dataState(DataState.ERROR);
      }
    } catch(e){
      dataState(DataState.ERROR);
    }
  }
}
