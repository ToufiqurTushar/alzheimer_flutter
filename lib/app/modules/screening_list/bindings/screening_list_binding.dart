import 'package:get/get.dart';

import '../../../../core/preference/preference.dart';
import '../../../../core/providers/api_provider.dart';
import '../../../repositories/alzheimer_repository.dart';
import '../controllers/screening_list_controller.dart';

class AZScreeningListBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<AZScreeningListController>(
      () => AZScreeningListController(),
    );
    Get.lazyPut<AlzheimerRepository>(
      () => AlzheimerRepository(apiProvider: Get.put(ApiProvider())),
    );
    Get.lazyPut<AZPreference>(
      () => AZPreference(), fenix: true
    );
  }
}
