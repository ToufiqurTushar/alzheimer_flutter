import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:responsive_grid/responsive_grid.dart';

import '../../../widgets/common_widget.dart';
import '../controllers/screening_final_result_controller.dart';

class AZScreeningFinalResultView extends GetView<AZScreeningFinalResultController> {
  static const PATH = '/screening-final-result';
  @override
  AZScreeningFinalResultController get controller => super.controller;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        fontFamily: 'roboto',
      ),
      home: Scaffold(
        backgroundColor: const Color(0xFFf5eaf0),
        appBar: AppBar(
          title: Text('az_result'.tr),
          centerTitle: true,
          backgroundColor: Colors.pink,
          leading: BackButton(onPressed:()=> Get.back()),
        ),
        body: Column(
          children: [
            ProfileHeader(context: context, image: controller.pref.profilePicture, name: controller.pref.fullName??'User', mobile: controller.pref.phone??'Phone'),
            Expanded(
                child:  SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.only(left:10, right:10),
                    child: _buildGridLayout(context),
                  ),
                )
            )
          ],
        ),
      ),
    );
  }



  Widget _buildGridLayout(context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        ResponsiveGridRow(
          children: [
            PageImage(image: 'assets/images/alzheimer/home_screen.png', height: 180, margin: const EdgeInsets.only(bottom: 20, top: 20)),
            ResponsiveGridCol(
              xs:12,
              child: Card(
                color: controller.colorCode,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    controller.advice,
                    textAlign:TextAlign.center,
                    style: TextStyle(fontSize: 22, color: controller.textColorCode, fontWeight: FontWeight.bold,),
                  )
                ),
              ),
            ),
            ResponsiveGridCol(
              xs:12,
              child: Card(
                color: Colors.purple,
                child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      controller.remarks,
                      textAlign:TextAlign.center,
                      style: const TextStyle(fontSize: 22, color: Colors.white, fontWeight: FontWeight.bold,),
                    )
                ),
              ),
            ),
            ResponsiveGridCol(
              xs:6,
              child: Card(
                color: Colors.pink,
                child: Padding(
                    padding: const EdgeInsets.all(15),
                    child: Text(
                      'az_refer_to_doctor'.tr,
                      textAlign:TextAlign.center,
                      style: const TextStyle(fontSize: 22, color: Colors.white, fontWeight: FontWeight.bold,),
                    )
                ),
              ),
            ),
            ResponsiveGridCol(
              xs:6,
              child: Card(
                color: Colors.pink,
                child: Padding(
                    padding: const EdgeInsets.all(15),
                    child: Text(
                      'az_refer_to_caregiver'.tr,
                      textAlign:TextAlign.center,
                      style: const TextStyle(fontSize: 22, color: Colors.white, fontWeight: FontWeight.bold,),
                    )
                ),
              ),
            ),
            ResponsiveGridCol(
              xs:12,
              child: GestureDetector(
                onTap: ()=> Get.back(),
                child: Card(
                  color: Colors.red,
                  child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        'az_finish'.tr,
                        textAlign:TextAlign.center,
                        style: const TextStyle(fontSize: 22, color: Colors.white, fontWeight: FontWeight.bold,),
                      )
                  ),
                ),
              ),
            )
          ],
        ),
      ],
    );
  }
}
