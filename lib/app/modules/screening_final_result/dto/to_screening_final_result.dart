import '../../../dto/mesurement/measurement_response.dart';

class ToScreeningFinalResult {
  ToScreeningFinalResult({
      required this.mesurementItem
  });

  MeasurementResponse mesurementItem;

}