import 'package:get/get.dart';

import '../controllers/screening_final_result_controller.dart';

class AZScreeningFinalResultBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<AZScreeningFinalResultController>(
      () => AZScreeningFinalResultController(),
    );
  }
}
