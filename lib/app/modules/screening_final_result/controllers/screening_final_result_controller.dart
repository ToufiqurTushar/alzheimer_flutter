import 'package:alzheimer_flutter/core/extensions/string_extension.dart';
import 'package:alzheimer_flutter/core/utils/methods.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import '../../../../core/preference/preference.dart';
import '../../../dto/mesurement/result.dart';
import '../dto/to_screening_final_result.dart';

class AZScreeningFinalResultController extends GetxController {
  ToScreeningFinalResult args = Get.arguments;
  final AZPreference pref = Get.find();

  late Result result;
  late String advice;
  late String suggestion;
  late String severity;
  late String remarks;
  late Color colorCode;
  late Color textColorCode;

  @override
  void onInit() {
    super.onInit();
    result = args.mesurementItem.measurement!.result!;
    advice = isLocaleBn() && result.bnAdvice!.isNotEmpty ? result.bnAdvice! : result.engAdvice!;
    suggestion = isLocaleBn() && result.suggestionBn!.isNotEmpty ? result.suggestionBn! : result.suggestion!;
    severity = result.severity!;
    colorCode = result.colorCode!.toColor();
    textColorCode = result.status == 'Normal' ? Colors.black : Colors.white;
    remarks = result.status == 'Normal' ? 'az_final_result_remark_no_substitute'.tr : 'az_final_result_remark_not_alone'.tr;
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
}
