
import 'package:get/get.dart';

import '../../../../core/utils/methods.dart';
import '../../../dto/question/questions_options.dart';
import '../../../dto/screening/input_content.dart';
import '../../../dto/screening/screening_page.dart';
import '../../../enum/Input_type_enum.dart';

import '../../../enum/option_enum.dart';


prepareScreeningPages(QuestionsAndOptions qando, {bool isEdit = false}) async {
  List<ScreeningPage> screeningPages = [];
  //s1
  screeningPages.add(ScreeningPage(
      page: 1,
      pageTitle: 'az_please_answer_the_following_questions'.tr,
      inputContents: [
        InputContent(
            type: InputTypeEnum.DATE,
            key: qando.azs1orientationaKey,
            label: 'az_which_date_is_today'.tr,
            hint: 'az_select_date'.tr,
            answer: isEdit ? qando.azs1orientationa!.first : null
        ),
        InputContent(
            type: InputTypeEnum.SELECT,
            key: qando.azs1orientationbKey,
            label: 'az_which_month_is_this'.tr,
            hint: 'az_select'.tr,
            options : OptionEnum.getLabelArrayByKey(qando.azs1orientationbKey),
            answer: isEdit ? OptionEnum.getMonthLabelByKeyAndNumber(qando.azs1orientationbKey, qando.azs1orientationb!.first) : null
        ),
        InputContent(
            type: InputTypeEnum.YEAR,
            key: qando.azs1orientationcKey,
            label: 'az_which_year_is_this'.tr,
            hint: 'az_select'.tr,
            options : List<String>.generate(401, (index) => trNmuber((index+1800).toString())),
            answer: isEdit ? trNmuber(qando.azs1orientationc!.first) : null
        ),
        InputContent(
            type: InputTypeEnum.SELECT,
            key: qando.azs1orientationdKey,
            label: 'az_which_day_is_it'.tr,
            hint: 'az_select'.tr,
            options : OptionEnum.getLabelArrayByKey(qando.azs1orientationdKey),
            answer: isEdit ? OptionEnum.getLabelByKeyAndText(qando.azs1orientationdKey, qando.azs1orientationd!.first) : null
        ),
        InputContent(
            type: InputTypeEnum.TRUE_FALSE,
            key: qando.azs1orientationeKey,
            label: 'az_which_district_do_you_live_in'.tr,
            hint: 'az_select'.tr,
            options : OptionEnum.getCorrectIncorrectArray(),
            answer: isEdit ? OptionEnum.getCorrectIncorrectLabelByBooleanText(qando.azs1orientatione!.first) : null
        ),
        InputContent(
            type: InputTypeEnum.TRUE_FALSE,
            key: qando.azs1orientationfKey,
            label: 'az_which_thana_upazila_do_you_live_in'.tr,
            hint: 'az_select'.tr,
            options : OptionEnum.getCorrectIncorrectArray(),
            answer: isEdit ? OptionEnum.getCorrectIncorrectLabelByBooleanText(qando.azs1orientationf!.first) : null
        )
      ]
  ));

  //s2 readonly
  screeningPages.add(ScreeningPage(
      page: 2,
      pageTitle: 'az_i_am_going_to_give_you_5_words_and_ask_later'.tr,
      inputContents: [
        InputContent(
          type: InputTypeEnum.MULTIPLE_SELECT,
          readOnly: true,
          key: qando.azs2attentionReadonlyKey,
          hint: 'az_select'.tr,
          options: OptionEnum.getLabelArrayByKey(qando.azs2attentionKey).sublist(0, 5),
        ),
      ]
  ));
  screeningPages.add(ScreeningPage(
      page: 2,
      pageTitle: 'az_please_tell_me_those_5_words'.tr,
      inputContents: [
        InputContent(
            type: InputTypeEnum.MULTIPLE_SELECT,
            key: qando.azs2attentionKey,
            hint: 'az_select'.tr,
            options: shuffle(OptionEnum.getLabelArrayByKey(qando.azs2attentionKey)),
            answer: isEdit ? OptionEnum.getLabelArrayByKeyAndNameArray(qando.azs2attentionKey, qando.azs2attention!) : null
        ),
      ]
  ));

  //s3 readonly
  screeningPages.add(ScreeningPage(
      page: 3,
      pageTitle: 'az_read_or_listen_the_list_of_digit_and_try_to_memorize'.tr,
      inputContents: [
        InputContent(
          type: InputTypeEnum.OTP,
          readOnly: true,
          key: qando.azs3questionKey,
          answer: bngToEng(qando.azs3question!.join(''))
        ),
      ]
  ));
  screeningPages.add(ScreeningPage(
      page: 3,
      pageTitle: 'az_repeat_list_of_digit'.tr,
      inputContents: [
        InputContent(
            type: InputTypeEnum.OTP,
            key: qando.azs3forwardKey,
            label: 'az_now_please_repeat_them_in_forward'.tr,
            answer: isEdit ? bngToEng(qando.azs3forward!.join('')): null
        ),
        InputContent(
            type: InputTypeEnum.OTP,
            key: qando.azs3backwardKey,
            label: 'az_and_now_in_backward_order'.tr,
            answer: isEdit ? bngToEng(qando.azs3backward!.join('')) : null
        ),
      ]
  ));

  //s4
  screeningPages.add(ScreeningPage(
      page: 4,
      pageTitle: 'az_do_you_remember_the_5_words_i_asked_you_to_remember'.tr,
      inputContents: [
        InputContent(
            type: InputTypeEnum.MULTIPLE_SELECT,
            key: qando.azs4attentionKey,
            options: shuffle(OptionEnum.getLabelArrayByKey(qando.azs4attentionKey)),
            answer: isEdit ? OptionEnum.getLabelArrayByKeyAndNameArray(qando.azs4attentionKey, qando.azs4attention!) : null
        ),
      ]
  ));
  //s5
  screeningPages.add(ScreeningPage(
      page: 5,
      pageTitle: 'az_please_answer_the_following_gk_questions'.tr,
      inputContents: [
        InputContent(
            type: InputTypeEnum.SELECT,
            key: qando.azs5memory1Key,
            label: 'az_tell_me_the_name_of_our_current_prime_minister'.tr,
            hint: 'az_select'.tr,
            options: OptionEnum.getLabelArrayByKey(qando.azs5memory1Key),
            answer: isEdit ? OptionEnum.getLabelByKeyAndName(qando.azs5memory1Key, qando.azs5memory1!.first) : null
        ),
        InputContent(
            type: InputTypeEnum.SELECT,
            key: qando.azs5memory2Key,
            label: 'az_tell_me_the_name_of_our_national_fish'.tr,
            hint: 'az_select'.tr,
            options: OptionEnum.getLabelArrayByKey(qando.azs5memory2Key),
            answer: isEdit ? OptionEnum.getLabelByKeyAndName(qando.azs5memory2Key, qando.azs5memory2!.first) : null
        ),
        InputContent(
            type: InputTypeEnum.SELECT,
            key: qando.azs5memory3Key,
            label: 'az_tell_me_the_year_of_liberation'.tr,
            hint: 'az_select'.tr,
            options: OptionEnum.getLabelArrayByKey(qando.azs5memory3Key),
            answer: isEdit ? OptionEnum.getLabelByKeyAndName(qando.azs5memory3Key, qando.azs5memory3!.first) : null
        ),
        InputContent(
            type: InputTypeEnum.SELECT,
            key: qando.azs5memory4Key,
            label: 'az_tell_me_the_capital_of_the_country'.tr,
            hint: 'az_select'.tr,
            options: OptionEnum.getLabelArrayByKey(qando.azs5memory4Key),
            answer: isEdit ? OptionEnum.getLabelByKeyAndName(qando.azs5memory4Key, qando.azs5memory4!.first) : null
        ),
        InputContent(
            type: InputTypeEnum.SELECT,
            key: qando.azs5memory5Key,
            label: 'az_tell_me_the_colors_of_the_national_flag'.tr,
            hint: 'az_select'.tr,
            options: OptionEnum.getLabelArrayByKey(qando.azs5memory5Key),
            answer: isEdit ? OptionEnum.getLabelByKeyAndName(qando.azs5memory5Key, qando.azs5memory5!.first) : null
        ),
      ]
  ));

  //s6
  screeningPages.add(ScreeningPage(
      page: 6,
      pageTitle: 'az_can_you_identify_the_notes_currencies'.tr,
      inputContents: [
        InputContent(
            type: InputTypeEnum.RADIO_WITH_FIGURE,
            key: qando.azs6memory1Key,
            hidden: qando.azs6memory1!.isEmpty,
            figure: qando.azs6memory1Label,
            options: OptionEnum.getLabelArrayByKey(qando.azs6memory1Key),
            answer: isEdit && qando.azs6memory1!.isNotEmpty ? OptionEnum.getLabelByKeyAndName(qando.azs6memory1Key, qando.azs6memory1!.first) : null
        ),
        InputContent(
            type: InputTypeEnum.RADIO_WITH_FIGURE,
            key: qando.azs6memory2Key,
            hidden: qando.azs6memory2!.isEmpty,
            figure: qando.azs6memory2Label,
            options: OptionEnum.getLabelArrayByKey(qando.azs6memory2Key),
            answer: isEdit && qando.azs6memory2!.isNotEmpty ? OptionEnum.getLabelByKeyAndName(qando.azs6memory2Key, qando.azs6memory2!.first) : null
        ),
        InputContent(
            type: InputTypeEnum.RADIO_WITH_FIGURE,
            key: qando.azs6memory3Key,
            hidden: qando.azs6memory3!.isEmpty,
            figure: qando.azs6memory3Label,
            options: OptionEnum.getLabelArrayByKey(qando.azs6memory3Key),
            answer: isEdit && qando.azs6memory3!.isNotEmpty ? OptionEnum.getLabelByKeyAndName(qando.azs6memory3Key, qando.azs6memory3!.first) : null
        ),
        InputContent(
            type: InputTypeEnum.RADIO_WITH_FIGURE,
            key: qando.azs6memory4Key,
            hidden: qando.azs6memory4!.isEmpty,
            figure: qando.azs6memory4Label,
            options: OptionEnum.getLabelArrayByKey(qando.azs6memory4Key),
            answer: isEdit && qando.azs6memory4!.isNotEmpty ? OptionEnum.getLabelByKeyAndName(qando.azs6memory4Key, qando.azs6memory4!.first) : null
        ),
        InputContent(
            type: InputTypeEnum.RADIO_WITH_FIGURE,
            key: qando.azs6memory5Key,
            hidden: qando.azs6memory5!.isEmpty,
            figure: qando.azs6memory5Label,
            options: OptionEnum.getLabelArrayByKey(qando.azs6memory5Key),
            answer: isEdit && qando.azs6memory5!.isNotEmpty ? OptionEnum.getLabelByKeyAndName(qando.azs6memory5Key, qando.azs6memory5!.first) : null
        ),
        InputContent(
            type: InputTypeEnum.RADIO_WITH_FIGURE,
            key: qando.azs6memory6Key,
            hidden: qando.azs6memory6!.isEmpty,
            figure: qando.azs6memory6Label,
            options: OptionEnum.getLabelArrayByKey(qando.azs6memory6Key),
            answer: isEdit && qando.azs6memory6!.isNotEmpty ? OptionEnum.getLabelByKeyAndName(qando.azs6memory6Key, qando.azs6memory6!.first) : null
        ),
        InputContent(
            type: InputTypeEnum.RADIO_WITH_FIGURE,
            key: qando.azs6memory7Key,
            hidden: qando.azs6memory7!.isEmpty,
            figure: qando.azs6memory7Label,
            options: OptionEnum.getLabelArrayByKey(qando.azs6memory7Key),
            answer: isEdit && qando.azs6memory7!.isNotEmpty ? OptionEnum.getLabelByKeyAndName(qando.azs6memory7Key, qando.azs6memory7!.first) : null
        ),
        InputContent(
            type: InputTypeEnum.RADIO_WITH_FIGURE,
            key: qando.azs6memory8Key,
            hidden: qando.azs6memory8!.isEmpty,
            figure: qando.azs6memory8Label,
            options: OptionEnum.getLabelArrayByKey(qando.azs6memory8Key),
            answer: isEdit && qando.azs6memory8!.isNotEmpty ? OptionEnum.getLabelByKeyAndName(qando.azs6memory8Key, qando.azs6memory8!.first) : null
        ),
        InputContent(
            type: InputTypeEnum.RADIO_WITH_FIGURE,
            key: qando.azs6memory9Key,
            hidden: qando.azs6memory9!.isEmpty,
            figure: qando.azs6memory9Label,
            options: OptionEnum.getLabelArrayByKey(qando.azs6memory9Key),
            answer: isEdit && qando.azs6memory9!.isNotEmpty ? OptionEnum.getLabelByKeyAndName(qando.azs6memory9Key, qando.azs6memory9!.first) : null
        ),
        InputContent(
            type: InputTypeEnum.RADIO_WITH_FIGURE,
            key: qando.azs6memory10Key,
            hidden: qando.azs6memory10!.isEmpty,
            figure: qando.azs6memory10Label,
            options: OptionEnum.getLabelArrayByKey(qando.azs6memory10Key),
            answer: isEdit && qando.azs6memory10!.isNotEmpty ? OptionEnum.getLabelByKeyAndName(qando.azs6memory10Key, qando.azs6memory10!.first) : null
        )
      ]
  ));

  //s7
  screeningPages.add(ScreeningPage(
      page: 7,
      pageTitle: 'az_please_answer_the_following_questions'.tr,
      inputContents: [
        InputContent(
            type: InputTypeEnum.SELECT,
            key: qando.azs7attention1Key,
            label: 'az_if_you_have_100_tk_and_you_gave_me_50_tk_from_that_100tk_how_much_money_do_you_have_now'.tr,
            hint: 'az_select'.tr,
            options: OptionEnum.getLabelArrayByKey(qando.azs7attention1Key),
            answer: isEdit ? OptionEnum.getLabelByKeyAndName(qando.azs7attention1Key, qando.azs7attention1!.first) : null
        ),
        InputContent(
            type: InputTypeEnum.SELECT,
            key: qando.azs7attention2Key,
            label: 'az_if_you_have_20_tk_and_i_give_you_5_tk_how_much_money_do_you_have_now'.tr,
            hint: 'az_select'.tr,
            options: OptionEnum.getLabelArrayByKey(qando.azs7attention2Key),
            answer: isEdit ? OptionEnum.getLabelByKeyAndName(qando.azs7attention2Key, qando.azs7attention2!.first) : null
        ),
        InputContent(
            type: InputTypeEnum.SELECT,
            key: qando.azs7attention3Key,
            label: 'az_if_you_have_500tk_and_you_give_me_200tk_how_much_money_do_you_have_left'.tr,
            hint: 'az_select'.tr,
            options: OptionEnum.getLabelArrayByKey(qando.azs7attention3Key),
            answer: isEdit ? OptionEnum.getLabelByKeyAndName(qando.azs7attention3Key, qando.azs7attention3!.first) : null
        ),
      ]
  ));

  //s8
  screeningPages.add(ScreeningPage(
      page: 8,
      pageTitle: 'az_name_the_following_body_parts'.tr,
      inputContents: [
        InputContent(
            type: InputTypeEnum.SELECT_WITH_FIGURE,
            key: qando.azs8naming1Key,
            hidden: qando.azs8naming1!.isEmpty,
            figure: qando.azs8naming1Label,
            hint: 'az_select_body_part'.tr,
            options: OptionEnum.getLabelArrayByNames(qando.azs8naming1),
            answer: isEdit && qando.azs8naming1!.isNotEmpty ? OptionEnum.getLabelByKeyAndName(qando.azs8naming1Key, qando.azs8naming1!.first) : null
        ),
        InputContent(
            type: InputTypeEnum.SELECT_WITH_FIGURE,
            key: qando.azs8naming2Key,
            hidden: qando.azs8naming2!.isEmpty,
            figure: qando.azs8naming2Label,
            hint: 'az_select_body_part'.tr,
            options: OptionEnum.getLabelArrayByNames(qando.azs8naming2),
            answer: isEdit && qando.azs8naming2!.isNotEmpty ? OptionEnum.getLabelByKeyAndName(qando.azs8naming2Key, qando.azs8naming2!.first) : null
        ),
        InputContent(
            type: InputTypeEnum.SELECT_WITH_FIGURE,
            key: qando.azs8naming3Key,
            hidden: qando.azs8naming3!.isEmpty,
            figure: qando.azs8naming3Label,
            hint: 'az_select_body_part'.tr,
            options: OptionEnum.getLabelArrayByNames(qando.azs8naming3),
            answer: isEdit && qando.azs8naming3!.isNotEmpty ? OptionEnum.getLabelByKeyAndName(qando.azs8naming3Key, qando.azs8naming3!.first) : null
        ),
        InputContent(
            type: InputTypeEnum.SELECT_WITH_FIGURE,
            key: qando.azs8naming4Key,
            hidden: qando.azs8naming4!.isEmpty,
            figure: qando.azs8naming4Label,
            hint: 'az_select_body_part'.tr,
            options: OptionEnum.getLabelArrayByNames(qando.azs8naming4),
            answer: isEdit && qando.azs8naming4!.isNotEmpty ? OptionEnum.getLabelByKeyAndName(qando.azs8naming4Key, qando.azs8naming4!.first) : null
        ),
        InputContent(
            type: InputTypeEnum.SELECT_WITH_FIGURE,
            key: qando.azs8naming5Key,
            hidden: qando.azs8naming5!.isEmpty,
            figure: qando.azs8naming5Label,
            hint: 'az_select_body_part'.tr,
            options: OptionEnum.getLabelArrayByNames(qando.azs8naming5),
            answer: isEdit && qando.azs8naming5!.isNotEmpty ? OptionEnum.getLabelByKeyAndName(qando.azs8naming5Key, qando.azs8naming5!.first) : null
        ),
        InputContent(
            type: InputTypeEnum.SELECT_WITH_FIGURE,
            key: qando.azs8naming6Key,
            hidden: qando.azs8naming6!.isEmpty,
            figure: qando.azs8naming6Label,
            hint: 'az_select_body_part'.tr,
            options: OptionEnum.getLabelArrayByNames(qando.azs8naming6),
            answer: isEdit && qando.azs8naming6!.isNotEmpty ? OptionEnum.getLabelByKeyAndName(qando.azs8naming6Key, qando.azs8naming6!.first) : null
        ),
        InputContent(
            type: InputTypeEnum.SELECT_WITH_FIGURE,
            key: qando.azs8naming7Key,
            hidden: qando.azs8naming7!.isEmpty,
            figure: qando.azs8naming7Label,
            hint: 'az_select_body_part'.tr,
            options: OptionEnum.getLabelArrayByNames(qando.azs8naming7),
            answer: isEdit && qando.azs8naming7!.isNotEmpty ? OptionEnum.getLabelByKeyAndName(qando.azs8naming7Key, qando.azs8naming7!.first) : null
        ),
        InputContent(
            type: InputTypeEnum.SELECT_WITH_FIGURE,
            key: qando.azs8naming8Key,
            hidden: qando.azs8naming8!.isEmpty,
            figure: qando.azs8naming8Label,
            hint: 'az_select_body_part'.tr,
            options: OptionEnum.getLabelArrayByNames(qando.azs8naming8),
            answer: isEdit && qando.azs8naming8!.isNotEmpty ? OptionEnum.getLabelByKeyAndName(qando.azs8naming8Key, qando.azs8naming8!.first) : null
        ),
        InputContent(
            type: InputTypeEnum.SELECT_WITH_FIGURE,
            key: qando.azs8naming9Key,
            hidden: qando.azs8naming9!.isEmpty,
            figure: qando.azs8naming9Label,
            hint: 'az_select_body_part'.tr,
            options: OptionEnum.getLabelArrayByNames(qando.azs8naming9),
            answer: isEdit && qando.azs8naming9!.isNotEmpty ? OptionEnum.getLabelByKeyAndName(qando.azs8naming9Key, qando.azs8naming9!.first) : null
        ),
        InputContent(
            type: InputTypeEnum.SELECT_WITH_FIGURE,
            key: qando.azs8naming10Key,
            hidden: qando.azs8naming10!.isEmpty,
            figure: qando.azs8naming10Label,
            hint: 'az_select_body_part'.tr,
            options: OptionEnum.getLabelArrayByNames(qando.azs8naming10),
            answer: isEdit && qando.azs8naming10!.isNotEmpty ? OptionEnum.getLabelByKeyAndName(qando.azs8naming10Key, qando.azs8naming10!.first) : null
        ),
        InputContent(
            type: InputTypeEnum.SELECT_WITH_FIGURE,
            key: qando.azs8naming11Key,
            hidden: qando.azs8naming11!.isEmpty,
            figure: qando.azs8naming11Label,
            hint: 'az_select_body_part'.tr,
            options: OptionEnum.getLabelArrayByNames(qando.azs8naming11),
            answer: isEdit && qando.azs8naming11!.isNotEmpty ? OptionEnum.getLabelByKeyAndName(qando.azs8naming11Key, qando.azs8naming11!.first) : null
        ),
        InputContent(
            type: InputTypeEnum.SELECT_WITH_FIGURE,
            key: qando.azs8naming12Key,
            hidden: qando.azs8naming12!.isEmpty,
            figure: qando.azs8naming12Label,
            hint: 'az_select_body_part'.tr,
            options: OptionEnum.getLabelArrayByNames(qando.azs8naming12),
            answer: isEdit && qando.azs8naming12!.isNotEmpty ? OptionEnum.getLabelByKeyAndName(qando.azs8naming12Key, qando.azs8naming12!.first) : null
        ),
      ]
  ));

  //s9
  screeningPages.add(ScreeningPage(
      page: 9,
      pageTitle: 'az_please_see_the_picture_and_tell_me_what_it_is'.tr,
      inputContents: [
        InputContent(
            type: InputTypeEnum.SELECT_WITH_FIGURE,
            key: qando.azs9naming1Key,
            hidden: qando.azs9naming1!.isEmpty,
            figure: qando.azs8naming1Label,
            hint: 'az_select'.tr,
            options: OptionEnum.getLabelArrayByNames(qando.azs9naming1),
            answer: isEdit && qando.azs9naming1!.isNotEmpty ? OptionEnum.getLabelByKeyAndName(qando.azs9naming1Key, qando.azs9naming1!.first) : null
        ),
        InputContent(
            type: InputTypeEnum.SELECT_WITH_FIGURE,
            key: qando.azs9naming2Key,
            hidden: qando.azs9naming2!.isEmpty,
            figure: qando.azs9naming2Label,
            hint: 'az_select'.tr,
            options: OptionEnum.getLabelArrayByNames(qando.azs9naming2),
            answer: isEdit && qando.azs9naming2!.isNotEmpty ? OptionEnum.getLabelByKeyAndName(qando.azs9naming2Key, qando.azs9naming2!.first) : null
        ),
        InputContent(
            type: InputTypeEnum.SELECT_WITH_FIGURE,
            key: qando.azs9naming3Key,
            hidden: qando.azs9naming3!.isEmpty,
            figure: qando.azs9naming3Label,
            hint: 'az_select'.tr,
            options: OptionEnum.getLabelArrayByNames(qando.azs9naming3),
            answer: isEdit && qando.azs9naming3!.isNotEmpty ? OptionEnum.getLabelByKeyAndName(qando.azs9naming3Key, qando.azs9naming3!.first) : null
        ),
        InputContent(
            type: InputTypeEnum.SELECT_WITH_FIGURE,
            key: qando.azs9naming4Key,
            hidden: qando.azs9naming4!.isEmpty,
            figure: qando.azs9naming4Label,
            hint: 'az_select'.tr,
            options: OptionEnum.getLabelArrayByNames(qando.azs9naming4),
            answer: isEdit && qando.azs9naming4!.isNotEmpty ? OptionEnum.getLabelByKeyAndName(qando.azs9naming4Key, qando.azs9naming4!.first) : null
        ),
        InputContent(
            type: InputTypeEnum.SELECT_WITH_FIGURE,
            key: qando.azs9naming5Key,
            hidden: qando.azs9naming5!.isEmpty,
            figure: qando.azs9naming5Label,
            hint: 'az_select'.tr,
            options: OptionEnum.getLabelArrayByNames(qando.azs9naming5),
            answer: isEdit && qando.azs9naming5!.isNotEmpty ? OptionEnum.getLabelByKeyAndName(qando.azs9naming5Key, qando.azs9naming5!.first) : null
        ),
        InputContent(
            type: InputTypeEnum.SELECT_WITH_FIGURE,
            key: qando.azs9naming6Key,
            hidden: qando.azs9naming6!.isEmpty,
            figure: qando.azs9naming6Label,
            hint: 'az_select'.tr,
            options: OptionEnum.getLabelArrayByNames(qando.azs9naming6),
            answer: isEdit && qando.azs9naming6!.isNotEmpty ? OptionEnum.getLabelByKeyAndName(qando.azs9naming6Key, qando.azs9naming6!.first) : null
        ),
        InputContent(
            type: InputTypeEnum.SELECT_WITH_FIGURE,
            key: qando.azs9naming7Key,
            hidden: qando.azs9naming7!.isEmpty,
            figure: qando.azs9naming7Label,
            hint: 'az_select'.tr,
            options: OptionEnum.getLabelArrayByNames(qando.azs9naming7),
            answer: isEdit && qando.azs9naming7!.isNotEmpty ? OptionEnum.getLabelByKeyAndName(qando.azs9naming7Key, qando.azs9naming7!.first) : null
        ),
        InputContent(
            type: InputTypeEnum.SELECT_WITH_FIGURE,
            key: qando.azs9naming8Key,
            hidden: qando.azs9naming8!.isEmpty,
            figure: qando.azs9naming8Label,
            hint: 'az_select'.tr,
            options: OptionEnum.getLabelArrayByNames(qando.azs9naming8),
            answer: isEdit && qando.azs9naming8!.isNotEmpty ? OptionEnum.getLabelByKeyAndName(qando.azs9naming8Key, qando.azs9naming8!.first) : null
        ),
        InputContent(
            type: InputTypeEnum.SELECT_WITH_FIGURE,
            key: qando.azs9naming9Key,
            hidden: qando.azs9naming9!.isEmpty,
            figure: qando.azs9naming9Label,
            hint: 'az_select'.tr,
            options: OptionEnum.getLabelArrayByNames(qando.azs9naming9),
            answer: isEdit && qando.azs9naming9!.isNotEmpty ? OptionEnum.getLabelByKeyAndName(qando.azs9naming9Key, qando.azs9naming9!.first) : null
        ),
        InputContent(
            type: InputTypeEnum.SELECT_WITH_FIGURE,
            key: qando.azs9naming10Key,
            hidden: qando.azs9naming10!.isEmpty,
            figure: qando.azs9naming10Label,
            hint: 'az_select'.tr,
            options: OptionEnum.getLabelArrayByNames(qando.azs9naming10),
            answer: isEdit && qando.azs9naming10!.isNotEmpty ? OptionEnum.getLabelByKeyAndName(qando.azs9naming10Key, qando.azs9naming10!.first) : null
        ),
        InputContent(
            type: InputTypeEnum.SELECT_WITH_FIGURE,
            key: qando.azs9naming11Key,
            hidden: qando.azs9naming11!.isEmpty,
            figure: qando.azs9naming11Label,
            hint: 'az_select'.tr,
            options: OptionEnum.getLabelArrayByNames(qando.azs9naming11),
            answer: isEdit && qando.azs9naming11!.isNotEmpty ? OptionEnum.getLabelByKeyAndName(qando.azs9naming11Key, qando.azs9naming11!.first) : null
        ),
        InputContent(
            type: InputTypeEnum.SELECT_WITH_FIGURE,
            key: qando.azs9naming12Key,
            hidden: qando.azs9naming12!.isEmpty,
            figure: qando.azs9naming12Label,
            hint: 'az_select'.tr,
            options: OptionEnum.getLabelArrayByNames(qando.azs9naming12),
            answer: isEdit && qando.azs9naming12!.isNotEmpty ? OptionEnum.getLabelByKeyAndName(qando.azs9naming12Key, qando.azs9naming12!.first) : null
        ),
        InputContent(
            type: InputTypeEnum.SELECT_WITH_FIGURE,
            key: qando.azs9naming13Key,
            hidden: qando.azs9naming13!.isEmpty,
            figure: qando.azs9naming13Label,
            hint: 'az_select'.tr,
            options: OptionEnum.getLabelArrayByNames(qando.azs9naming13),
            answer: isEdit && qando.azs9naming13!.isNotEmpty ? OptionEnum.getLabelByKeyAndName(qando.azs9naming13Key, qando.azs9naming13!.first) : null
        ),
        InputContent(
            type: InputTypeEnum.SELECT_WITH_FIGURE,
            key: qando.azs9naming14Key,
            hidden: qando.azs9naming14!.isEmpty,
            figure: qando.azs9naming14Label,
            hint: 'az_select'.tr,
            options: OptionEnum.getLabelArrayByNames(qando.azs9naming14),
            answer: isEdit && qando.azs9naming14!.isNotEmpty ? OptionEnum.getLabelByKeyAndName(qando.azs9naming14Key, qando.azs9naming14!.first) : null
        ),
        InputContent(
            type: InputTypeEnum.SELECT_WITH_FIGURE,
            key: qando.azs9naming15Key,
            hidden: qando.azs9naming15!.isEmpty,
            figure: qando.azs9naming15Label,
            hint: 'az_select'.tr,
            options: OptionEnum.getLabelArrayByNames(qando.azs9naming15),
            answer: isEdit && qando.azs9naming15!.isNotEmpty ? OptionEnum.getLabelByKeyAndName(qando.azs9naming15Key, qando.azs9naming15!.first) : null
        ),
        InputContent(
            type: InputTypeEnum.SELECT_WITH_FIGURE,
            key: qando.azs9naming16Key,
            hidden: qando.azs9naming16!.isEmpty,
            figure: qando.azs9naming16Label,
            hint: 'az_select'.tr,
            options: OptionEnum.getLabelArrayByNames(qando.azs9naming16),
            answer: isEdit && qando.azs9naming16!.isNotEmpty ? OptionEnum.getLabelByKeyAndName(qando.azs9naming16Key, qando.azs9naming16!.first) : null
        ),
        InputContent(
            type: InputTypeEnum.SELECT_WITH_FIGURE,
            key: qando.azs9naming17Key,
            hidden: qando.azs9naming17!.isEmpty,
            figure: qando.azs9naming17Label,
            hint: 'az_select'.tr,
            options: OptionEnum.getLabelArrayByNames(qando.azs9naming17),
            answer: isEdit && qando.azs9naming17!.isNotEmpty ? OptionEnum.getLabelByKeyAndName(qando.azs9naming17Key, qando.azs9naming17!.first) : null
        ),
        InputContent(
            type: InputTypeEnum.SELECT_WITH_FIGURE,
            key: qando.azs9naming18Key,
            hidden: qando.azs9naming18!.isEmpty,
            figure: qando.azs9naming18Label,
            hint: 'az_select'.tr,
            options: OptionEnum.getLabelArrayByNames(qando.azs9naming18),
            answer: isEdit && qando.azs9naming18!.isNotEmpty ? OptionEnum.getLabelByKeyAndName(qando.azs9naming18Key, qando.azs9naming18!.first) : null
        ),
        InputContent(
            type: InputTypeEnum.SELECT_WITH_FIGURE,
            key: qando.azs9naming19Key,
            hidden: qando.azs9naming19!.isEmpty,
            figure: qando.azs9naming19Label,
            hint: 'az_select'.tr,
            options: OptionEnum.getLabelArrayByNames(qando.azs9naming19),
            answer: isEdit && qando.azs9naming19!.isNotEmpty ? OptionEnum.getLabelByKeyAndName(qando.azs9naming19Key, qando.azs9naming19!.first) : null
        ),
        InputContent(
            type: InputTypeEnum.SELECT_WITH_FIGURE,
            key: qando.azs9naming20Key,
            hidden: qando.azs9naming20!.isEmpty,
            figure: qando.azs9naming20Label,
            hint: 'az_select'.tr,
            options: OptionEnum.getLabelArrayByNames(qando.azs9naming20),
            answer: isEdit && qando.azs9naming20!.isNotEmpty ? OptionEnum.getLabelByKeyAndName(qando.azs9naming20Key, qando.azs9naming20!.first) : null
        ),
        InputContent(
            type: InputTypeEnum.SELECT_WITH_FIGURE,
            key: qando.azs9naming21Key,
            hidden: qando.azs9naming21!.isEmpty,
            figure: qando.azs9naming21Label,
            hint: 'az_select'.tr,
            options: OptionEnum.getLabelArrayByNames(qando.azs9naming21),
            answer: isEdit && qando.azs9naming21!.isNotEmpty ? OptionEnum.getLabelByKeyAndName(qando.azs9naming21Key, qando.azs9naming21!.first) : null
        ),

      ]
  ));

  //s10
  screeningPages.add(ScreeningPage(
      page: 10,
      pageTitle: 'az_is_there_any_fruit'.tr,
      inputContents: [
        InputContent(
            type: InputTypeEnum.TRUE_FALSE_WITH_SELECT_IMAGE,
            key: qando.azs10language1aKey,
            options: OptionEnum.getYesNoArray(),
            answer: isEdit ? OptionEnum.getYesNoLabelByBooleanText((qando.azs10language1a!.first)) : null,
            child: InputContent(
              type: InputTypeEnum.RADIO_WITH_FIGURE,
              hidden: false,
              key: qando.azs10language1bKey,
              label: 'az_which_is_the_fruit'.tr,
              options: OptionEnum.getLabelArrayByKey(qando.azs10language1bKey),
              answer: isEdit && qando.azs10language1b!.isNotEmpty ? OptionEnum.getLabelByKeyAndName(qando.azs10language1bKey, qando.azs10language1b!.first) : null,
            )
        ),
      ]
  ));
  screeningPages.add(ScreeningPage(
      page: 10,
      pageTitle: 'az_is_there_any_kitchen_item'.tr,
      inputContents: [
        InputContent(
            type: InputTypeEnum.TRUE_FALSE_WITH_SELECT_IMAGE,
            key: qando.azs10language2aKey,
            options: OptionEnum.getYesNoArray(),
            answer: isEdit ? OptionEnum.getYesNoLabelByBooleanText((qando.azs10language2a!.first)) : null,
            child: InputContent(
              type: InputTypeEnum.RADIO_WITH_FIGURE,
              hidden: false,
              key: qando.azs10language2bKey,
              label: 'az_which_is_the_kitchen_item'.tr,
              options: OptionEnum.getLabelArrayByKey(qando.azs10language2bKey),
              answer: isEdit && qando.azs10language2b!.isNotEmpty ? OptionEnum.getLabelByKeyAndName(qando.azs10language2bKey, qando.azs10language2b?.first) : null,
            )
        ),
      ]
  ));
  screeningPages.add(ScreeningPage(
      page: 10,
      pageTitle: 'az_is_there_any_domestic_animal'.tr,
      inputContents: [
        InputContent(
            type: InputTypeEnum.TRUE_FALSE_WITH_SELECT_IMAGE,
            key: qando.azs10language3aKey,
            options: OptionEnum.getYesNoArray(),
            answer: isEdit ? OptionEnum.getYesNoLabelByBooleanText((qando.azs10language3a!.first)) : null,
            child: InputContent(
              type: InputTypeEnum.RADIO_WITH_FIGURE,
              hidden: false,
              key: qando.azs10language3bKey,
              label: 'az_which_is_the_domestic_animal'.tr,
              options: OptionEnum.getLabelArrayByKey(qando.azs10language3bKey),
              answer: isEdit && qando.azs10language3b!.isNotEmpty ? OptionEnum.getLabelByKeyAndName(qando.azs10language3bKey, qando.azs10language3b?.first) : null,
            )
        ),
      ]
  ));
  screeningPages.add(ScreeningPage(
      page: 10,
      pageTitle: 'az_is_there_any_vehicle'.tr,
      inputContents: [
        InputContent(
            type: InputTypeEnum.TRUE_FALSE_WITH_SELECT_IMAGE,
            key: qando.azs10language4aKey,
            options: OptionEnum.getYesNoArray(),
            answer: isEdit ? OptionEnum.getYesNoLabelByBooleanText((qando.azs10language4a!.first)) : null,
            child: InputContent(
              type: InputTypeEnum.RADIO_WITH_FIGURE,
              hidden: false,
              key: qando.azs10language4bKey,
              label: 'az_which_is_the_vehicle'.tr,
              options: OptionEnum.getLabelArrayByKey(qando.azs10language4bKey),
              answer: isEdit && qando.azs10language4b!.isNotEmpty ? OptionEnum.getLabelByKeyAndName(qando.azs10language4bKey, qando.azs10language4b?.first) : null,
            )
        ),
      ]
  ));

  //s11
  screeningPages.add(ScreeningPage(
      page: 11,
      pageTitle: 'az_please_repeat_this_words_after_me'.tr,
      inputContents: [
        InputContent(
            type: InputTypeEnum.TRUE_FALSE,
            key: qando.azs11language1Key,
            label: qando.azs11language1Label,
            labelAtCenter: true,
            options : OptionEnum.getCorrectIncorrectArray(),
            answer: isEdit && qando.azs11language1!.isNotEmpty ? OptionEnum.getCorrectIncorrectLabelByBooleanText(qando.azs11language1!.first) : null
        ),
        InputContent(
            type: InputTypeEnum.TRUE_FALSE,
            key: qando.azs11language2Key,
            label: qando.azs11language2Label,
            labelAtCenter: true,
            options : OptionEnum.getCorrectIncorrectArray(),
            answer: isEdit && qando.azs11language2!.isNotEmpty ? OptionEnum.getCorrectIncorrectLabelByBooleanText(qando.azs11language2!.first) : null
        ),
        InputContent(
            type: InputTypeEnum.TRUE_FALSE,
            key: qando.azs11language3Key,
            label: qando.azs11language3Label,
            labelAtCenter: true,
            options : OptionEnum.getCorrectIncorrectArray(),
            answer: isEdit && qando.azs11language3!.isNotEmpty ? OptionEnum.getCorrectIncorrectLabelByBooleanText(qando.azs11language3!.first) : null
        ),
        InputContent(
            type: InputTypeEnum.TRUE_FALSE,
            key: qando.azs11language4Key,
            label: qando.azs11language4Label,
            labelAtCenter: true,
            options : OptionEnum.getCorrectIncorrectArray(),
            answer: isEdit && qando.azs11language4!.isNotEmpty ? OptionEnum.getCorrectIncorrectLabelByBooleanText(qando.azs11language4!.first) : null
        ),
        InputContent(
            type: InputTypeEnum.TRUE_FALSE,
            key: qando.azs11language5Key,
            label: qando.azs11language5Label,
            labelAtCenter: true,
            options : OptionEnum.getCorrectIncorrectArray(),
            answer: isEdit && qando.azs11language5!.isNotEmpty ? OptionEnum.getCorrectIncorrectLabelByBooleanText(qando.azs11language5!.first) : null
        )
      ]
  ));

  //s12
  screeningPages.add(ScreeningPage(
      page: 12,
      pageTitle: 'az_please_repeat_the_sentence_after_me'.tr,
      inputContents: [
        InputContent(
            type: InputTypeEnum.TRUE_FALSE,
            hidden: qando.azs12language1 == null,
            key: qando.azs12language1Key,
            label: qando.azs12language1Label,
            options : OptionEnum.getCorrectIncorrectArray(),
            answer: isEdit && qando.azs12language1 != null && qando.azs12language1!.isNotEmpty ? OptionEnum.getCorrectIncorrectLabelByBooleanText(qando.azs12language1!.first) : null
        ),
        InputContent(
            type: InputTypeEnum.TRUE_FALSE,
            hidden: qando.azs12language2 == null,
            key: qando.azs12language2Key,
            label: qando.azs12language2Label,
            options : OptionEnum.getCorrectIncorrectArray(),
            answer: isEdit && qando.azs12language2 != null && qando.azs12language2!.isNotEmpty ? OptionEnum.getCorrectIncorrectLabelByBooleanText(qando.azs12language2!.first) : null
        ),
        InputContent(
            type: InputTypeEnum.TRUE_FALSE,
            hidden: qando.azs12language3 == null,
            key: qando.azs12language3Key,
            label: qando.azs12language3Label,
            options : OptionEnum.getCorrectIncorrectArray(),
            answer: isEdit && qando.azs12language3 != null && qando.azs12language3!.isNotEmpty ? OptionEnum.getCorrectIncorrectLabelByBooleanText(qando.azs12language3!.first) : null
        ),
        InputContent(
            type: InputTypeEnum.TRUE_FALSE,
            hidden: qando.azs12language4 == null,
            key: qando.azs12language4Key,
            label: qando.azs12language4Label,
            options : OptionEnum.getCorrectIncorrectArray(),
            answer: isEdit && qando.azs12language4 != null && qando.azs12language4!.isNotEmpty ? OptionEnum.getCorrectIncorrectLabelByBooleanText(qando.azs12language4!.first) : null
        ),
        InputContent(
            type: InputTypeEnum.TRUE_FALSE,
            hidden: qando.azs12language5 == null,
            key: qando.azs12language5Key,
            label: qando.azs12language5Label,
            options : OptionEnum.getCorrectIncorrectArray(),
            answer: isEdit && qando.azs12language5 != null && qando.azs12language5!.isNotEmpty ? OptionEnum.getCorrectIncorrectLabelByBooleanText(qando.azs12language5!.first) : null
        ),
        InputContent(
            type: InputTypeEnum.TRUE_FALSE,
            hidden: qando.azs12language6 == null,
            key: qando.azs12language6Key,
            label: qando.azs12language6Label,
            options : OptionEnum.getCorrectIncorrectArray(),
            answer: isEdit && qando.azs12language6 != null && qando.azs12language6!.isNotEmpty ? OptionEnum.getCorrectIncorrectLabelByBooleanText(qando.azs12language6!.first) : null
        ),
        InputContent(
            type: InputTypeEnum.TRUE_FALSE,
            hidden: qando.azs12language7 == null,
            key: qando.azs12language7Key,
            label: qando.azs12language7Label,
            options : OptionEnum.getCorrectIncorrectArray(),
            answer: isEdit && qando.azs12language7 != null && qando.azs12language7!.isNotEmpty ? OptionEnum.getCorrectIncorrectLabelByBooleanText(qando.azs12language7!.first) : null
        ),
        InputContent(
            type: InputTypeEnum.TRUE_FALSE,
            hidden: qando.azs12language8 == null,
            key: qando.azs12language8Key,
            label: qando.azs12language8Label,
            options : OptionEnum.getCorrectIncorrectArray(),
            answer: isEdit && qando.azs12language8 != null && qando.azs12language8!.isNotEmpty ? OptionEnum.getCorrectIncorrectLabelByBooleanText(qando.azs12language8!.first) : null
        ),
        InputContent(
            type: InputTypeEnum.TRUE_FALSE,
            hidden: qando.azs12language9 == null,
            key: qando.azs12language9Key,
            label: qando.azs12language9Label,
            options : OptionEnum.getCorrectIncorrectArray(),
            answer: isEdit && qando.azs12language9 != null && qando.azs12language9!.isNotEmpty ? OptionEnum.getCorrectIncorrectLabelByBooleanText(qando.azs12language9!.first) : null
        ),
        InputContent(
            type: InputTypeEnum.TRUE_FALSE,
            hidden: qando.azs12language10 == null,
            key: qando.azs12language10Key,
            label: qando.azs12language10Label,
            options : OptionEnum.getCorrectIncorrectArray(),
            answer: isEdit && qando.azs12language10 != null && qando.azs12language10!.isNotEmpty ? OptionEnum.getCorrectIncorrectLabelByBooleanText(qando.azs12language10!.first) : null
        ),
        InputContent(
            type: InputTypeEnum.TRUE_FALSE,
            hidden: qando.azs12language11 == null,
            key: qando.azs12language11Key,
            label: qando.azs12language11Label,
            options : OptionEnum.getCorrectIncorrectArray(),
            answer: isEdit && qando.azs12language11 != null && qando.azs12language11!.isNotEmpty ? OptionEnum.getCorrectIncorrectLabelByBooleanText(qando.azs12language11!.first) : null
        ),
      ]
  ));

  //s13
  String azs13fluencyaKey = '';
  String azs13fluencyaLabel = '';
  List<String?> azs13fluencya = [null];

  if(qando.azs13fluencya1!.isNotEmpty) {
    azs13fluencyaKey = qando.azs13fluencya1Key;
    azs13fluencyaLabel = qando.azs13fluencya1Label;
    azs13fluencya = qando.azs13fluencya1!;
  }
  else if(qando.azs13fluencya2!.isNotEmpty) {
    azs13fluencyaKey = qando.azs13fluencya2Key;
    azs13fluencyaLabel = qando.azs13fluencya2Label;
    azs13fluencya = qando.azs13fluencya2!;
  }
  else if(qando.azs13fluencya3!.isNotEmpty) {
    azs13fluencyaKey = qando.azs13fluencya3Key;
    azs13fluencyaLabel = qando.azs13fluencya3Label;
    azs13fluencya = qando.azs13fluencya3!;
  }
  else if(qando.azs13fluencya4!.isNotEmpty) {
    azs13fluencyaKey = qando.azs13fluencya4Key;
    azs13fluencyaLabel = qando.azs13fluencya4Label;
    azs13fluencya = qando.azs13fluencya4!;
  }
  else if(qando.azs13fluencya5!.isNotEmpty) {
    azs13fluencyaKey = qando.azs13fluencya5Key;
    azs13fluencyaLabel = qando.azs13fluencya5Label;
    azs13fluencya = qando.azs13fluencya5!;
  }
  else if(qando.azs13fluencya6!.isNotEmpty) {
    azs13fluencyaKey = qando.azs13fluencya6Key;
    azs13fluencyaLabel = qando.azs13fluencya6Label;
    azs13fluencya = qando.azs13fluencya6!;
  }
  else if(qando.azs13fluencya7!.isNotEmpty) {
    azs13fluencyaKey = qando.azs13fluencya7Key;
    azs13fluencyaLabel = qando.azs13fluencya7Label;
    azs13fluencya = qando.azs13fluencya7!;
  }
  else if(qando.azs13fluencya8!.isNotEmpty) {
    azs13fluencyaKey = qando.azs13fluencya8Key;
    azs13fluencyaLabel = qando.azs13fluencya8Label;
    azs13fluencya = qando.azs13fluencya8!;
  }
  else if(qando.azs13fluencya9!.isNotEmpty) {
    azs13fluencyaKey = qando.azs13fluencya9Key;
    azs13fluencyaLabel = qando.azs13fluencya9Label;
    azs13fluencya = qando.azs13fluencya9!;
  }
  else if(qando.azs13fluencya10!.isNotEmpty) {
    azs13fluencyaKey = qando.azs13fluencya10Key;
    azs13fluencyaLabel = qando.azs13fluencya10Label;
    azs13fluencya = qando.azs13fluencya10!;
  }
  else if(qando.azs13fluencya11!.isNotEmpty) {
    azs13fluencyaKey = qando.azs13fluencya11Key;
    azs13fluencyaLabel = qando.azs13fluencya11Label;
    azs13fluencya = qando.azs13fluencya11!;
  }
  else if(qando.azs13fluencya12!.isNotEmpty) {
    azs13fluencyaKey = qando.azs13fluencya12Key;
    azs13fluencyaLabel = qando.azs13fluencya12Label;
    azs13fluencya = qando.azs13fluencya12!;
  }
  else if(qando.azs13fluencya13!.isNotEmpty) {
    azs13fluencyaKey = qando.azs13fluencya13Key;
    azs13fluencyaLabel = qando.azs13fluencya13Label;
    azs13fluencya = qando.azs13fluencya13!;
  }
  else if(qando.azs13fluencya14!.isNotEmpty) {
    azs13fluencyaKey = qando.azs13fluencya14Key;
    azs13fluencyaLabel = qando.azs13fluencya14Label;
    azs13fluencya = qando.azs13fluencya14!;
  } else {

  }


  screeningPages.add(ScreeningPage(
      page: 13,
      pageTitle: 'az_are_you_educated'.tr,
      inputContents: [
        InputContent(
            type: InputTypeEnum.TRUE_FALSE_WITH_SELECT_FOR_TRUE_AND_SELECT_FOR_FALSE,
            key: qando.azs13fluencyKey,
            options: OptionEnum.getYesNoArray(),
            answer: isEdit && azs13fluencyaKey.isNotEmpty ? OptionEnum.getLabelYes() : isEdit && azs13fluencyaKey.isEmpty ? OptionEnum.getLabelNo() : null,
            child: InputContent(
                type: InputTypeEnum.RADIO_WITH_FIGURE,
                hidden: isEdit && azs13fluencyaKey.isNotEmpty ? false : true,
                key: azs13fluencyaKey,
                label: 'az_would_like_you_to_generate_as_much_as_word_you_can_beginning_with_a_letter'.tr,
                figure:  azs13fluencyaLabel,
                options: OptionEnum.getLabelArrayByKey(qando.azs13fluencyKey),
                answer: isEdit && azs13fluencyaKey.isNotEmpty ? OptionEnum.getLabelByName(azs13fluencya.first) : null
            ),
            secondChild: InputContent(
                type: InputTypeEnum.RADIO_WITH_FIGURE,
                hidden: isEdit && azs13fluencyaKey.isEmpty ? false : true,
                key: qando.azs13fluencybKey,
                label: 'az_can_you_name_as_many_fruits_and_vegetables_as_possible'.tr,
                options: OptionEnum.getLabelArrayByKey(qando.azs13fluencyKey),
                answer: isEdit && azs13fluencyaKey.isEmpty ? OptionEnum.getLabelByName(qando.azs13fluencyb!.first) : null
            )
        )
      ]
  ));


  //s14
  screeningPages.add(ScreeningPage(
      page: 14,
      pageTitle: 'az_can_you_name_as_many_animals_as_possible'.tr,
      inputContents: [
        InputContent(
          type: InputTypeEnum.SINGLE_SELECT,
          key: qando.azs14fluencyKey,
          hint: 'az_select'.tr,
          options: OptionEnum.getLabelArrayByKey(qando.azs14fluencyKey),
          answer: isEdit ? OptionEnum.getLabelByKeyAndName(qando.azs14fluencyKey, qando.azs14fluency!.first) : null
        ),
      ]
  ));

  //s15
  screeningPages.add(ScreeningPage(
      page: 15,
      pageTitle: 'az_please_copy_this_diagram'.tr,
      inputContents: [
        InputContent(
          type: InputTypeEnum.TRUE_FALSE_WITH_FIGURE_TAKE_PICTURE,
          key: qando.azs15visuospatial1Key,
          figure: qando.azs15visuospatial1Label,
          hint: 'az_select'.tr,
          options: OptionEnum.getCorrectIncorrectArray(),
          answer: isEdit ? OptionEnum.getCorrectIncorrectLabelByBooleanText(qando.azs15visuospatial1!.first) : null
        ),
      ]
  ));
  screeningPages.add(ScreeningPage(
      page: 15,
      pageTitle: 'az_please_copy_this_diagram'.tr,
      inputContents: [
        InputContent(
          type: InputTypeEnum.TRUE_FALSE_WITH_FIGURE_TAKE_PICTURE,
          key: qando.azs15visuospatial2Key,
          figure: qando.azs15visuospatial2Label,
          hint: 'az_select'.tr,
          options: OptionEnum.getCorrectIncorrectArray(),
          answer: isEdit ? OptionEnum.getCorrectIncorrectLabelByBooleanText(qando.azs15visuospatial2!.first) : null
        ),
      ]
  ));

  //s16
  List<InputContent> inputContents = [
    InputContent(
      type: InputTypeEnum.SELECT_WITH_FIGURE,
      key: qando.azs16visuospatial1Key,
      figure: qando.azs16visuospatial1Label,
      hint: 'az_select'.tr,
      options: OptionEnum.getLabelArrayByNames(qando.azs16visuospatial1),
      answer: isEdit && qando.azs16visuospatial1!.isNotEmpty ? OptionEnum.getLabelByKeyAndName(qando.azs16visuospatial1Key, qando.azs16visuospatial1!.first) : null
    ),
    InputContent(
      type: InputTypeEnum.SELECT_WITH_FIGURE,
      key: qando.azs16visuospatial2Key,
      figure: qando.azs16visuospatial2Label,
      hint: 'az_select'.tr,
      options: OptionEnum.getLabelArrayByNames(qando.azs16visuospatial2),
      answer: isEdit && qando.azs16visuospatial2!.isNotEmpty ? OptionEnum.getLabelByKeyAndName(qando.azs16visuospatial2Key, qando.azs16visuospatial2!.first) : null
    ),
    InputContent(
      type: InputTypeEnum.SELECT_WITH_FIGURE,
      key: qando.azs16visuospatial3Key,
      figure: qando.azs16visuospatial3Label,
      hint: 'az_select'.tr,
      options: OptionEnum.getLabelArrayByNames(qando.azs16visuospatial3),
      answer: isEdit && qando.azs16visuospatial3!.isNotEmpty ? OptionEnum.getLabelByKeyAndName(qando.azs16visuospatial3Key, qando.azs16visuospatial3!.first) : null
    ),
    InputContent(
      type: InputTypeEnum.SELECT_WITH_FIGURE,
      key: qando.azs16visuospatial4Key,
      figure: qando.azs16visuospatial4Label,
      hint: 'az_select'.tr,
      options: OptionEnum.getLabelArrayByNames(qando.azs16visuospatial4),
      answer: isEdit && qando.azs16visuospatial4!.isNotEmpty ? OptionEnum.getLabelByKeyAndName(qando.azs16visuospatial4Key, qando.azs16visuospatial4!.first) : null
    ),
    InputContent(
      type: InputTypeEnum.SELECT_WITH_FIGURE,
      key: qando.azs16visuospatial5Key,
      figure: qando.azs16visuospatial5Label,
      hint: 'az_select'.tr,
      options: OptionEnum.getLabelArrayByNames(qando.azs16visuospatial5),
      answer: isEdit && qando.azs16visuospatial5!.isNotEmpty ? OptionEnum.getLabelByKeyAndName(qando.azs16visuospatial5Key, qando.azs16visuospatial5!.first) : null
    ),
    InputContent(
      type: InputTypeEnum.SELECT_WITH_FIGURE,
      key: qando.azs16visuospatial6Key,
      figure: qando.azs16visuospatial6Label,
      hint: 'az_select'.tr,
      options: OptionEnum.getLabelArrayByNames(qando.azs16visuospatial6),
      answer: isEdit && qando.azs16visuospatial6!.isNotEmpty ? OptionEnum.getLabelByKeyAndName(qando.azs16visuospatial6Key, qando.azs16visuospatial6!.first) : null
    ),
    InputContent(
      type: InputTypeEnum.SELECT_WITH_FIGURE,
      key: qando.azs16visuospatial7Key,
      figure: qando.azs16visuospatial7Label,
      hint: 'az_select'.tr,
      options: OptionEnum.getLabelArrayByNames(qando.azs16visuospatial7),
      answer: isEdit && qando.azs16visuospatial7!.isNotEmpty ? OptionEnum.getLabelByKeyAndName(qando.azs16visuospatial7Key, qando.azs16visuospatial7!.first) : null
    ),
    InputContent(
      type: InputTypeEnum.SELECT_WITH_FIGURE,
      key: qando.azs16visuospatial8Key,
      figure: qando.azs16visuospatial8Label,
      hint: 'az_select'.tr,
      options: OptionEnum.getLabelArrayByNames(qando.azs16visuospatial8),
      answer: isEdit && qando.azs16visuospatial8!.isNotEmpty ? OptionEnum.getLabelByKeyAndName(qando.azs16visuospatial8Key, qando.azs16visuospatial8!.first) : null
    ),
    InputContent(
      type: InputTypeEnum.SELECT_WITH_FIGURE,
      key: qando.azs16visuospatial9Key,
      figure: qando.azs16visuospatial9Label,
      hint: 'az_select'.tr,
      options: OptionEnum.getLabelArrayByNames(qando.azs16visuospatial9),
      answer: isEdit && qando.azs16visuospatial9!.isNotEmpty ? OptionEnum.getLabelByKeyAndName(qando.azs16visuospatial9Key, qando.azs16visuospatial9!.first) : null
    ),
    InputContent(
      type: InputTypeEnum.SELECT_WITH_FIGURE,
      key: qando.azs16visuospatial10Key,
      figure: qando.azs16visuospatial10Label,
      hint: 'az_select'.tr,
      options: OptionEnum.getLabelArrayByNames(qando.azs16visuospatial10),
      answer: isEdit && qando.azs16visuospatial10!.isNotEmpty ? OptionEnum.getLabelByKeyAndName(qando.azs16visuospatial10Key, qando.azs16visuospatial10!.first) : null
    ),
    InputContent(
      type: InputTypeEnum.SELECT_WITH_FIGURE,
      key: qando.azs16visuospatial11Key,
      figure: qando.azs16visuospatial11Label,
      hint: 'az_select'.tr,
      options: OptionEnum.getLabelArrayByNames(qando.azs16visuospatial11),
      answer: isEdit && qando.azs16visuospatial11!.isNotEmpty ? OptionEnum.getLabelByKeyAndName(qando.azs16visuospatial11Key, qando.azs16visuospatial11!.first) : null
    ),
  ];
  screeningPages.add(ScreeningPage(
      page: 16,
      pageTitle: 'az_count_the_dots_without_pointing_to_them'.tr,
      inputContents: [inputContents.where((e) => e.options!.isNotEmpty).first],
  ));
  screeningPages.add(ScreeningPage(
      page: 16,
      pageTitle: 'az_count_the_dots_without_pointing_to_them'.tr,
      inputContents: [inputContents.where((e) => e.options!.isNotEmpty).last]
  ));

  return screeningPages;
}