import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';
import 'package:responsive_grid/responsive_grid.dart';
import '../../../../core/widgets/body.dart';
import '../../../widgets/common_widget.dart';
import '../../../enum/data_state.dart';
import '../../../dto/screening/screening_page.dart';
import '../../../widgets/question_container.dart';
import '../controllers/add_screening_controller.dart';

class AZAddScreeningView extends GetView<AZAddScreeningController> {
  static const PATH = '/add-screening';
  @override
  Widget build(BuildContext context) {
    return Obx(()=> WillPopScope(
      onWillPop: () async {
        controller.pressBack();
        return false;
        //return true;
      },
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          fontFamily: 'roboto',
        ),
        home: Scaffold(
            backgroundColor: const Color(0xFFf5eaf0),
            appBar: AppBar(
              title: Text('az_screening'.tr),
              centerTitle: true,
              backgroundColor: Colors.pink,
              leading: BackButton(onPressed:()=> controller.pressBack()),
            ),
            body: buildBody(context),
            bottomNavigationBar:
            controller.isFirstPage ?
            Padding(
              padding: const EdgeInsets.only(left: 10, right: 10),
              child: Row(children: [
                FlexibleButton(text: 'Next', flex: 1, onPressed: controller.nextPage)
              ]),
            ) :
            controller.isLastPage ?
            Padding(
              padding: const EdgeInsets.only(left: 10, right: 10),
              child: Row(children: [
                FlexibleButton(text: 'Previous', flex: 1, onPressed: controller.previousPage),
                FlexibleButton(text: 'Submit', flex: 1, onPressed: (){
                  controller.submit();
                })
              ]),
            ) :
            Padding(
              padding: const EdgeInsets.only(left: 10, right: 10),
              child: Row(children: [
                FlexibleButton(text: 'Previous', flex: 1, onPressed: controller.previousPage),
                FlexibleButton(text: 'Next', flex: 1, onPressed: controller.nextPage),
              ]),
            ),
        ),
      ),
    ));
  }

  buildBody(BuildContext context) {
    switch(controller.dataState.value) {
      case DataState.LOADING: return LoadingBody();
      case DataState.ERROR: return ReloadBody(()=> controller.getQuestionsAndOptions());
      case DataState.EMPTY: return EmptyBody();
      case DataState.LOADED: return builBodyContainer(context);
      default: return ReloadBody(()=> controller.getQuestionsAndOptions());
    }
  }

  builBodyContainer(BuildContext context) {
    return Stack(
      children: [
        FormBuilder(
            key: controller.form,
            onChanged: () {
              controller.form.currentState!.save();
              try{
                //debugPrint(controller.form.currentState!.value.toString());
              } catch(e){}
            },
            child:  PageView.builder(
                physics: const NeverScrollableScrollPhysics(),
                controller: controller.mPageController,
                itemCount: controller.screeningPages.length,
                onPageChanged: controller.onPageChanged,
                itemBuilder: (context, index) {
                  return FutureBuilder<ScreeningPage> (
                      future: controller.getScreeningPages(index),
                      builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                        if(snapshot.hasData){
                          ScreeningPage screeningPage = snapshot.data;
                          return SingleChildScrollView (
                            child: Column(
                              children: [
                                ProfileHeader(context: context, image: controller.pref.profilePicture, name: controller.pref.fullName??'User', mobile: controller.pref.phone??'Phone'),
                                ResponsiveGridRow(
                                    children: [
                                      PageImage(image: 'assets/images/alzheimer/screening_${controller.screeningPages[controller.selectedPageIndex.value].page??1}.png', height: Get.context!.isLandscape ? 50: 100),
                                    ]
                                ),
                                ResponsiveGridList(
                                    rowMainAxisAlignment: MainAxisAlignment.center,
                                    shrinkWrap: true,
                                    desiredItemWidth: 50,
                                    minSpacing: 5,
                                    children: controller.screeningPagesIndicatorList.map((i) {
                                      Color background = Colors.pink;
                                      Color textColor = Colors.white;
                                      if(controller.screeningPages[controller.selectedPageIndex.value].page == i){
                                        background = Colors.white;
                                        textColor = Colors.black;
                                      }
                                      return IndicatorContainer(text: i.toString(), background: background, textColor: textColor);
                                    }).toList()
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 2, right: 2),
                                    child: Card(
                                    child: Container(
                                      color: Colors.white,
                                      padding: const EdgeInsets.all(10),
                                      child: buildEachQuestionContainer(screeningPage, context, controller.form),
                                    )
                                  )
                                )

                              ],
                            )
                          );
                        } else if (snapshot.hasError) {
                          return Center(child: Text(snapshot.error.toString()));
                        } else {
                          return const Center(child: CircularProgressIndicator());
                        }
                      }
                  );
                }
            )
        ),
      ],
    );
  }
}
