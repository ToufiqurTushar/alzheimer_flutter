import 'package:alzheimer_flutter/app/dto/screening/screening_page.dart';
import 'package:alzheimer_flutter/core/utils/dialog_util.dart';
import 'package:alzheimer_flutter/core/utils/snackbar_util.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:logger/logger.dart';
import '../../../../core/preference/preference.dart';
import '../views/screening_pages.dart';
import '../../../dto/answer/answers.dart';
import '../../../dto/answer/answers_request.dart';
import '../../../dto/mesurement/measurement_response.dart';
import '../../../dto/question/questions_options_response.dart';
import '../../../routes/app_pages.dart';
import '../../../widgets/common_widget.dart';
import '../../../enum/data_state.dart';
import '../../../repositories/alzheimer_repository.dart';
import '../../screening_final_result/dto/to_screening_final_result.dart';


class AZAddScreeningController extends GetxController {
  final AlzheimerRepository alzheimerRepository = Get.find();
  final AZPreference pref = Get.find();

  //live data
  final dataState = DataState.LOADING.obs;
  final selectedPageIndex = 0.obs;
  List<int> screeningPagesIndicatorList = [1,2,3,4].obs;

  //variable
  final form = GlobalKey<FormBuilderState>();
  final mPageController = PageController();
  List<ScreeningPage> screeningPages = [];


  //get value as variable
  bool get isLastPage => selectedPageIndex.value == screeningPages.length - 1;
  bool get isFirstPage => selectedPageIndex.value == 0;

  //override functions
  @override
  void onInit() {
    super.onInit();
    getQuestionsAndOptions();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  //server calling to get questions and options
  getQuestionsAndOptions() async {
    try{
      dataState(DataState.LOADING);
      QuestionsOptionsResponse? responseDto = await alzheimerRepository.getQuestionsAndOptions();
      if(responseDto != null) {
        screeningPages = await prepareScreeningPages(responseDto.questionsAndOptions!);
        updateScreeningPagesIndicatorList();

        dataState(DataState.LOADED);
      } else {
        dataState(DataState.ERROR);
      }
    } catch(e){
      dataState(DataState.ERROR);
    }
  }


  updateScreeningPagesIndicatorList() {
    int currentPage = screeningPages[selectedPageIndex.value].page??1;
    int lastPage = screeningPages[screeningPages.length-1].page??1;
    if(currentPage == lastPage && screeningPages.length >= 4) {
      screeningPagesIndicatorList = [currentPage-3, currentPage-2, currentPage-1, currentPage].obs;
    } else if(currentPage < 4) {
      screeningPagesIndicatorList = List<int>.generate(4, (index) => index+1).obs;
    } else if(currentPage>=0 && screeningPages.length >= 4) {
      screeningPagesIndicatorList = [currentPage-2, currentPage-1, currentPage, currentPage+1].obs;
    } else  {
      screeningPagesIndicatorList = List<int>.generate(screeningPages.length, (index) => index+1).obs;
    }
  }

  //get every screen content used in snapshot for future builder of screening page
  Future<ScreeningPage> getScreeningPages(int index) async {
    return screeningPages[index];
  }

  onPageChanged(index) {
    selectedPageIndex.value = index;
    updateScreeningPagesIndicatorList();
  }

  nextPage() {
    SystemChannels.textInput.invokeMethod('TextInput.hide');
    if (form.currentState?.saveAndValidate() ?? false) {
      mPageController.nextPage(duration: 300.milliseconds, curve: Curves.ease);
      updateScreeningPagesIndicatorList();
    } else { //not validated
      //mPageController.nextPage(duration: 300.milliseconds, curve: Curves.ease);
      SnackbarUtil.showErrorToast(message: 'az_please_fillup_required_field'.tr);
    }

  }

  previousPage() {
    SystemChannels.textInput.invokeMethod('TextInput.hide');
    if (form.currentState?.saveAndValidate() ?? false) {
      mPageController.previousPage(duration: 300.milliseconds, curve: Curves.ease);
      updateScreeningPagesIndicatorList();
    } else { //not validated
      //mPageController.previousPage(duration: 300.milliseconds, curve: Curves.ease);
      SnackbarUtil.showErrorToast(message: 'az_please_fillup_required_field_for_previous'.tr);
    }
  }

  navigateToPage(int index) {
    mPageController.animateToPage(index, duration: 100.milliseconds, curve: Curves.ease);
    updateScreeningPagesIndicatorList();
  }


  submit() async {
    if (form.currentState?.saveAndValidate() ?? false) {
      final map = form.currentState!.value;
      AnswersRequest answersRequest = AnswersRequest();
      answersRequest.answers = Answers.fromJson(map);
      answersRequest.measuredAt = DateTime.now().millisecondsSinceEpoch;
      answersRequest.userId = pref.userId;
      DialogUtil.showOverlayLoading();
      MeasurementResponse? responseDto = await alzheimerRepository.postAnswers(answersRequest);
      DialogUtil.closeOverlayLoading();
      if(responseDto !=null && responseDto.id != null) {
        Logger().e(responseDto.toJson());
        Get.back();
        Get.toNamed(Routes.SCREENING_FINAL_RESULT, arguments: ToScreeningFinalResult(mesurementItem: responseDto));
        SnackbarUtil.showSuccess(message: 'az_submitted_successfully'.tr);
      } else {
        //SnackbarUtil.showErrorToast(message: 'az_something_wrong'.tr);
        SnackbarUtil.showErrorToast(message: responseDto.toString());
      }
    } else {
      SnackbarUtil.showErrorToast(message: 'az_please_fillup_required_field'.tr);
    }
  }


  //app close warning
  pressBack() {
    showYesNoPopup(
        Get.context!,
        subTitle: 'az_do_you_want_to_go_back'.tr,
        onPressedYes: (){
          Navigator.pop(Get.context!);
          Get.back();
        },
        onPressedNo: (){
          Navigator.pop(Get.context!);
        }
    );
  }

}
