import 'package:alzheimer_flutter/core/providers/api_provider.dart';
import 'package:get/get.dart';

import '../../../../core/preference/preference.dart';
import '../../../repositories/alzheimer_repository.dart';
import '../controllers/add_screening_controller.dart';

class AZAddScreeningBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<AZAddScreeningController>(
      () => AZAddScreeningController(),
    );
    Get.lazyPut<AlzheimerRepository>(
       () => AlzheimerRepository(apiProvider: Get.put(ApiProvider())),
    );
    Get.lazyPut<AZPreference>(
       () => AZPreference(), fenix: true
    );
  }
}
