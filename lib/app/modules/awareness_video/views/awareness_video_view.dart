import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/awareness_video_controller.dart';

class AZAwarenessVideoView extends GetView<AZAwarenessVideoController> {
  static const PATH = '/awareness-video';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Awareness Video'),
        centerTitle: true,
      ),
      body: const Center(
        child: Text(
          'AZAwarenessVideoView is working',
          style: TextStyle(fontSize: 20),
        ),
      ),
    );
  }
}
