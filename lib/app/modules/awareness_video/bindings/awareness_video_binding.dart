import 'package:get/get.dart';

import '../../../../core/preference/preference.dart';
import '../controllers/awareness_video_controller.dart';

class AZAwarenessVideoBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<AZAwarenessVideoController>(
      () => AZAwarenessVideoController(),
    );
    Get.lazyPut<AZPreference>(
       () => AZPreference(), fenix: true
    );
  }
}
