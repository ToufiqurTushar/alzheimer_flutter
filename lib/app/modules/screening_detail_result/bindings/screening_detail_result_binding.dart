import 'package:get/get.dart';

import '../../../../core/providers/api_provider.dart';
import '../../../repositories/alzheimer_repository.dart';
import '../controllers/screening_detail_result_controller.dart';

class AZScreeningDetailResultBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<AZScreeningDetailResultController>(
      () => AZScreeningDetailResultController(),
    );
    Get.lazyPut<AlzheimerRepository>(
      () => AlzheimerRepository(apiProvider: Get.put(ApiProvider())),
    );
  }
}
