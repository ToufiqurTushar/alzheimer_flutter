import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';
import '../../../../core/widgets/body.dart';
import '../../../widgets/common_widget.dart';
import '../../../enum/data_state.dart';
import '../controllers/screening_detail_result_controller.dart';

class AZScreeningDetailResultView extends GetView<AZScreeningDetailResultController> {
  static const PATH = '/screening-detail-result';
  @override
  Widget build(BuildContext context) {
    return Obx(()=>MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        fontFamily: 'roboto',
      ),
      home: Scaffold(
          backgroundColor: const Color(0xFFf5eaf0),
          appBar: AppBar(
            title: Text('az_detail_result'.tr),
            centerTitle: true,
            backgroundColor: Colors.pink,
            leading: BackButton(onPressed:()=> Get.back()),
          ),
          bottomNavigationBar: BottomAppBar(
            child: SimpleButton('END', onPressed: (){
              Get.back();
            }),
          ),
          body: buildBody(context)
        ),
    ));
  }

  buildBody(BuildContext context) {
    switch(controller.dataState.value) {
      case DataState.LOADING: return LoadingBody();
      case DataState.ERROR: return ReloadBody(()=> controller.getQuestionsOptionsAndAnswers());
      case DataState.EMPTY: return EmptyBody();
      case DataState.LOADED: return builBodyContainer(context);
      default: return ReloadBody(()=> controller.getQuestionsOptionsAndAnswers());
    }
  }

  builBodyContainer(BuildContext context) {
    //return Text('1');
    return DefaultTabController(
      initialIndex: 0,
      length: controller.tabBarTabs.length,
      child: Column(
        children: [
          ProfileHeader(context: context, image: controller.pref.profilePicture, name: controller.pref.fullName??'User', mobile: controller.pref.phone??'Phone'),
          Padding(
            padding: const EdgeInsets.only(left: 5, right: 5),
            child: Card(
              child: TabBar(
                isScrollable: true,
                unselectedLabelColor: Colors.black,
                labelColor: Colors.pink,
                indicatorWeight: 4,
                tabs: controller.tabBarTabs,
              ),
            ),
          ),
          FormBuilder(
            key:  controller.form,
            child: Expanded(
              child: Padding(
                padding: const EdgeInsets.only(left: 5, right: 5),
                child: TabBarView(
                    children: controller.tabBarViewChildren
                ),
              ),
            ),
          )
        ]
      ),
    );
  }
}
