import '../../../dto/mesurement/measurement_response.dart';

class ToScreeningDetailsResult {
  ToScreeningDetailsResult({
      required this.mesurementItem
  });

  MeasurementResponse mesurementItem;

}