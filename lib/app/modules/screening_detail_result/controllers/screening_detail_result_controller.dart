import 'dart:convert';

import 'package:alzheimer_flutter/app/dto/answer/answers.dart';
import 'package:alzheimer_flutter/app/dto/mesurement/measurement_response.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';
import 'package:logger/logger.dart';
import '../../../../core/preference/preference.dart';
import '../../add_screening/views/screening_pages.dart';
import '../../../dto/answer/answers_request.dart';
import '../../../dto/question/questions_options.dart';
import '../../../repositories/alzheimer_repository.dart';
import '../../../enum/data_state.dart';
import '../../../dto/screening/screening_page.dart';
import '../../../widgets/question_container.dart';
import '../dto/to_screening_details_result.dart';


class AZScreeningDetailResultController extends GetxController {
  ToScreeningDetailsResult args = Get.arguments;
  final AZPreference pref = Get.find();
  final AlzheimerRepository alzheimerRepository = Get.find();

  //live data
  final dataState = DataState.LOADING.obs;

  //variable
  final form = GlobalKey<FormBuilderState>();
  List<Widget> tabBarTabs = [];
  List<Widget> tabBarViewChildren = [];
  List<ScreeningPage> screeningPages = [];
  late MeasurementResponse measurement;

  @override
  void onInit() {
    super.onInit();
    measurement = args.mesurementItem;
    getQuestionsOptionsAndAnswers();
  }

  @override
  void onReady() {
    super.onReady();

  }

  @override
  void onClose() {}

  getQuestionsOptionsAndAnswers() async {
    try{
      dataState(DataState.LOADING);
      QuestionsAndOptions qando = measurement.details!;
      Logger().e(jsonEncode(qando.azs12language1));
      Logger().e(measurement.details!.toJson());

      screeningPages = await prepareScreeningPages(qando, isEdit: true);
      await buildTabBarAndView();

      dataState(DataState.LOADED);
    } catch(e){
      dataState(DataState.ERROR);
    }
  }

  getQuestionsOptionsAndAnswersFromJsonAsset() async {
    try{
      dataState(DataState.LOADING);
      MeasurementResponse? responseDto = await alzheimerRepository.getQuestionsOptionsAndAnswersFromJsonAsset();
      if(responseDto != null) {
        QuestionsAndOptions qando = QuestionsAndOptions.fromJson(responseDto.details!.toJson());
        screeningPages = await prepareScreeningPages(qando, isEdit: true);
        await buildTabBarAndView();

        dataState(DataState.LOADED);
      } else {
        dataState(DataState.ERROR);
      }
    } catch(e){
      dataState(DataState.ERROR);
    }
  }


  buildTabBarAndView() async{
    tabBarTabs = [
        const Tab(text: 'Orientation'),
        const Tab(text: 'Attention'),
        const Tab(text: 'Naming'),
        const Tab(text: 'Memory'),
        const Tab(text: 'Language'),
        const Tab(text: 'Fluency'),
        const Tab(text: 'Visuospatial'),
    ];
    tabBarViewChildren = [
      //Orientation
      SingleChildScrollView(
          child: IgnorePointer(
            ignoring: true,
            child: Padding(
              padding: const EdgeInsets.only(left: 8, right: 8),
              child: Column(children:[
                buildEachQuestionContainer(screeningPages[0], Get.context, form)
              ]),
            ),
          )
      ),
      //Attention
      SingleChildScrollView(
          child:IgnorePointer(
            ignoring: true,
            child: Padding(
              padding: const EdgeInsets.only(left: 8, right: 8),
              child: Column(children:[
                buildEachQuestionContainer(screeningPages[1], Get.context, form),//
                buildEachQuestionContainer(screeningPages[2], Get.context, form),
                buildEachQuestionContainer(screeningPages[3], Get.context, form),//
                buildEachQuestionContainer(screeningPages[4], Get.context, form),
                buildEachQuestionContainer(screeningPages[8], Get.context, form),
              ]),
            ),
          )
      ),
      //Naming
      SingleChildScrollView(
          child:IgnorePointer(
            ignoring: true,
            child: Padding(
              padding: const EdgeInsets.only(left: 8, right: 8),
              child: Column(children:[
                buildEachQuestionContainer(screeningPages[9], Get.context, form),
                buildEachQuestionContainer(screeningPages[10], Get.context, form)
              ]),
            ),
          )
      ),
      //Memory
      SingleChildScrollView(
          child:IgnorePointer(
            ignoring: true,
            child: Padding(
              padding: const EdgeInsets.only(left: 8, right: 8),
              child: Column(children:[
                buildEachQuestionContainer(screeningPages[5], Get.context, form),
                buildEachQuestionContainer(screeningPages[6], Get.context, form),
                buildEachQuestionContainer(screeningPages[7], Get.context, form),
              ]),
            ),
          )
      ),
      //Language
      SingleChildScrollView(
          child:IgnorePointer(
            ignoring: true,
            child: Padding(
              padding: const EdgeInsets.only(left: 8, right: 8),
              child: Column(children:[
                buildEachQuestionContainer(screeningPages[11], Get.context, form),
                buildEachQuestionContainer(screeningPages[12], Get.context, form),
                buildEachQuestionContainer(screeningPages[13], Get.context, form),
                buildEachQuestionContainer(screeningPages[14], Get.context, form),
                buildEachQuestionContainer(screeningPages[15], Get.context, form),
                buildEachQuestionContainer(screeningPages[16], Get.context, form),
              ]),
            ),
          )
      ),
      //Fluency
      SingleChildScrollView(
          child:IgnorePointer(
            ignoring: true,
            child: Padding(
              padding: const EdgeInsets.only(left: 8, right: 8),
              child: Column(children:[
                buildEachQuestionContainer(screeningPages[17], Get.context, form),
                buildEachQuestionContainer(screeningPages[18], Get.context, form)
              ]),
            ),
          )
      ),
      //Visuospatial
      SingleChildScrollView(
          child:IgnorePointer(
            ignoring: true,
            child: Padding(
              padding: const EdgeInsets.only(left: 8, right: 8),
              child: Column(children:[
                buildEachQuestionContainer(screeningPages[19], Get.context, form),
                buildEachQuestionContainer(screeningPages[20], Get.context, form),
                buildEachQuestionContainer(screeningPages[21], Get.context, form),
                buildEachQuestionContainer(screeningPages[22], Get.context, form),
              ]),
            ),
          )
      ),
    ];
  }
}
