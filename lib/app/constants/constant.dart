class Constant {
  static const String user = 'user';
}

class DateConstant {
  static const String ymdhmsz = 'yyyy-MM-dd HH:mm:ss.z';//
  static const String ymdhms = 'yyyy-MM-dd HH:mm:ss';//
  static const String ymd = 'yyyy-MM-dd';//
  static const String dmy = 'dd-MM-yyyy';//
  static const String hms = 'HH:mm:ss';//
  static const String hma = 'hh:mm a';//
  static const String dM = 'dd MMM';//
  static const String dMy = 'dd MMM yyyy';//
  static const String dMyhma = 'dd MMM yyyy hh:mm a';//
  static const String hh = 'HH';//
}
class OptionConstant {
  static const String YES = 'Yes';//
  static const String YES_BN = 'হ্যাঁ';//
  static const String NO = 'No';//
  static const String NO_BN = 'না';//
  static const String CORRECT = 'Correct';//
  static const String CORRECT_BN = 'সঠিক';//
  static const String INCORRECT = 'Incorrect';//
  static const String INCORRECT_BN = 'ভুল';//
  static const String ONE = '1';//
  static const String ONE_BN = '১';//
  static const String ZERO = '0';//
  static const String DONT_KNOW = "I don't know";//
  static const String DONT_KNOW_BN = 'জানা নেই';//
}
