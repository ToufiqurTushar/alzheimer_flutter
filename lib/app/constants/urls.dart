import 'package:get_storage/get_storage.dart';

import '../../core/preference/preference.dart';

class Urls {
  static const baseUrl = 'https://core-dev.cmedhealth.com/';
  static const getQuestionsOptionsUrl = 'api/v1/alzheimer/question';
  static const postAnswersUrl = 'api/v1/alzheimer/answer';
  static getBaseUrl() {
    var getBaseUrl = GetStorage().read(PrefKey.baseUrl);
    if(getBaseUrl != null) {
      return getBaseUrl;
    }
    return baseUrl;
  }
  static getAlzheimerMeasurementUrl({required int userId}) {
    return  'api/v1/alzheimer/user/$userId';
  }

}
