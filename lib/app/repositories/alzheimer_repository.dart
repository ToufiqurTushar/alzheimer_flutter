import 'dart:async';
import 'package:alzheimer_flutter/app/dto/answer/answers_request.dart';
import 'package:logger/logger.dart';
import '../../core/providers/api_provider.dart';
import '../constants/urls.dart';
import '../dto/mesurement/measurement_response.dart';
import '../dto/question/questions_options_response.dart';

class AlzheimerRepository{
  final ApiProvider apiProvider;
  AlzheimerRepository({required this.apiProvider});
  ///////////////////////////////////////////

  Future<QuestionsOptionsResponse?> getQuestionsAndOptions() async {
    try {
      //questions_and_options
      final response = await apiProvider.GET(Urls.getQuestionsOptionsUrl, requireBearerToken: true);
      if (response.statusCode == 200) {
        return QuestionsOptionsResponse.fromJson(response.body);
      }
      // final response = await apiProvider.getJsonFromAsset(fileName: 'alzheimer/questions_response');
      // if (response != null) {
      //   return QuestionsOptionsResponse.fromJson(response);
      // }
      return null;
    } catch (err) {
      rethrow;
    }
  }

  Future<MeasurementResponse?> getQuestionsOptionsAndAnswersFromJsonAsset() async {
    try {
      //questions_and_options
      final response = await apiProvider.getJsonFromAsset(fileName: 'alzheimer/measurement_response');
      if (response != null) {
        return MeasurementResponse.fromJson(response);
      }
      // final response = await apiProvider.GET(Urls.getAlzheimerMeasurementUrl(userId: 1), requireBearerToken: true);
      // if (response.statusCode == 200) {
      //   return MeasurementResponse.fromJson(response.body);
      // }
      return null;
    } catch (err) {
      rethrow;
    }
  }

  Future<MeasurementResponse?> postAnswers(AnswersRequest answersRequest) async {
    try {
      Logger().i(answersRequest.toJson());
      final response = await apiProvider.POST(Urls.postAnswersUrl, answersRequest.toJson(), requireBearerToken: true);
      if (response.statusCode == 200) {
        return MeasurementResponse.fromJson(response.body);
      }
      return null;
    } catch (err) {
      rethrow;
    }
  }

  Future<List<MeasurementResponse>?> getAllAlzheimerMesurement({int? userId}) async {
    try {
      final response = await apiProvider.GET(Urls.getAlzheimerMeasurementUrl(userId: userId??0), requireBearerToken: true);
      if (response.statusCode == 200) {
        return MeasurementResponse.fromJsonList(response.body);
      }
      return null;
    } catch (err) {
      rethrow;
    }
  }
}
