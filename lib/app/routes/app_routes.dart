part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  Routes._();
  static const ALZHEIMER_HOME = _Paths.ALZHEIMER_HOME;
  static const ADD_SCREENING = _Paths.ADD_SCREENING;
  static const SCREENING_LIST = _Paths.SCREENING_LIST;
  static const SCREENING_LIST_ITEM = _Paths.SCREENING_LIST_ITEM;
  static const SCREENING_DETAIL_RESULT = _Paths.SCREENING_DETAIL_RESULT;
  static const SCREENING_FINAL_RESULT = _Paths.SCREENING_FINAL_RESULT;
  static const REFER = _Paths.REFER;
  static const AWARENESS_VIDEO = _Paths.AWARENESS_VIDEO;
}

abstract class _Paths {
  _Paths._();
  static const ALZHEIMER_HOME = '/alzheimer-home';
  static const ADD_SCREENING = '/add-screening';
  static const SCREENING_LIST = '/screening-list';
  static const SCREENING_LIST_ITEM = '/screening-list-item';
  static const SCREENING_DETAIL_RESULT = '/screening-detail-result';
  static const SCREENING_FINAL_RESULT = '/screening-final-result';
  static const REFER = '/refer';
  static const AWARENESS_VIDEO = '/awareness-video';
}
