import 'package:get/get.dart';

import '../modules/add_screening/bindings/add_screening_binding.dart';
import '../modules/add_screening/views/add_screening_view.dart';
import '../modules/home/bindings/home_binding.dart';
import '../modules/home/views/home_view.dart';
import '../modules/awareness_video/bindings/awareness_video_binding.dart';
import '../modules/awareness_video/views/awareness_video_view.dart';
import '../modules/refer/bindings/refer_binding.dart';
import '../modules/refer/views/refer_view.dart';
import '../modules/screening_detail_result/bindings/screening_detail_result_binding.dart';
import '../modules/screening_detail_result/views/screening_detail_result_view.dart';
import '../modules/screening_final_result/bindings/screening_final_result_binding.dart';
import '../modules/screening_final_result/views/screening_final_result_view.dart';
import '../modules/screening_list/bindings/screening_list_binding.dart';
import '../modules/screening_list/views/screening_list_view.dart';
import '../modules/screening_list_item/bindings/screening_list_item_binding.dart';
import '../modules/screening_list_item/views/screening_list_item_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.ALZHEIMER_HOME;

  static final routes = [
    GetPage(
      name: _Paths.ALZHEIMER_HOME,
      page: () => AZHomeView(),
      binding: AZHomeBinding(),
    ),
    GetPage(
      name: _Paths.ADD_SCREENING,
      page: () => AZAddScreeningView(),
      binding: AZAddScreeningBinding(),
    ),
    GetPage(
      name: _Paths.SCREENING_LIST,
      page: () => AZScreeningListView(),
      binding: AZScreeningListBinding(),
    ),
    GetPage(
      name: _Paths.SCREENING_LIST_ITEM,
      page: () => AZScreeningListItemView(),
      binding: AZScreeningListItemBinding(),
    ),
    GetPage(
      name: _Paths.SCREENING_DETAIL_RESULT,
      page: () => AZScreeningDetailResultView(),
      binding: AZScreeningDetailResultBinding(),
    ),
    GetPage(
      name: _Paths.SCREENING_FINAL_RESULT,
      page: () => AZScreeningFinalResultView(),
      binding: AZScreeningFinalResultBinding(),
    ),
    GetPage(
      name: _Paths.REFER,
      page: () => AZReferView(),
      binding: AZReferBinding(),
    ),
    GetPage(
      name: _Paths.AWARENESS_VIDEO,
      page: () => AZAwarenessVideoView(),
      binding: AZAwarenessVideoBinding(),
    ),

  ];
}
