import 'package:alzheimer_flutter/core/utils/methods.dart';
import 'package:intl/intl.dart';

import '../constants/constant.dart';

enum OptionEnum {
  //AZ_S1_ORIENTATION_B
  AZ_S1_ORIENTATION_B_OPT_1('AZ_S1_ORIENTATION_B', 'January', 'জানুয়ারী'),
  AZ_S1_ORIENTATION_B_OPT_2('AZ_S1_ORIENTATION_B', 'February', 'ফেব্রুয়ারী'),
  AZ_S1_ORIENTATION_B_OPT_3('AZ_S1_ORIENTATION_B', 'March', 'মার্চ'),
  AZ_S1_ORIENTATION_B_OPT_4('AZ_S1_ORIENTATION_B', 'April', 'এপ্রিল'),
  AZ_S1_ORIENTATION_B_OPT_5('AZ_S1_ORIENTATION_B', 'May', 'মে'),
  AZ_S1_ORIENTATION_B_OPT_6('AZ_S1_ORIENTATION_B', 'June', 'জুন'),
  AZ_S1_ORIENTATION_B_OPT_7('AZ_S1_ORIENTATION_B', 'July', 'জুলাই'),
  AZ_S1_ORIENTATION_B_OPT_8('AZ_S1_ORIENTATION_B', 'August', 'আগস্ট'),
  AZ_S1_ORIENTATION_B_OPT_9('AZ_S1_ORIENTATION_B', 'September', 'সেপ্টেম্বর'),
  AZ_S1_ORIENTATION_B_OPT_10('AZ_S1_ORIENTATION_B', 'October', 'অক্টোবর'),
  AZ_S1_ORIENTATION_B_OPT_11('AZ_S1_ORIENTATION_B', 'November', 'নভেম্বর'),
  AZ_S1_ORIENTATION_B_OPT_12('AZ_S1_ORIENTATION_B', 'December', 'ডিসেম্বর'),
  AZ_S1_ORIENTATION_B_OPT_13('AZ_S1_ORIENTATION_B', OptionConstant.DONT_KNOW, OptionConstant.DONT_KNOW_BN),

  //AZ_S1_ORIENTATION_D
  AZ_S1_ORIENTATION_D_OPT_1('AZ_S1_ORIENTATION_D', 'Saturday', 'শনিবার'),
  AZ_S1_ORIENTATION_D_OPT_2('AZ_S1_ORIENTATION_D', 'Sunday', 'রবিবার'),
  AZ_S1_ORIENTATION_D_OPT_3('AZ_S1_ORIENTATION_D', 'Monday', 'সোমবার'),
  AZ_S1_ORIENTATION_D_OPT_4('AZ_S1_ORIENTATION_D', 'Tuesday', 'মঙ্গলবার'),
  AZ_S1_ORIENTATION_D_OPT_5('AZ_S1_ORIENTATION_D', 'Wednesday', 'বুধবার'),
  AZ_S1_ORIENTATION_D_OPT_6('AZ_S1_ORIENTATION_D', 'Thursday', 'বৃহস্পতিবার'),
  AZ_S1_ORIENTATION_D_OPT_7('AZ_S1_ORIENTATION_D', 'Friday', 'শুক্রবার'),
  AZ_S1_ORIENTATION_D_OPT_8('AZ_S1_ORIENTATION_D', OptionConstant.DONT_KNOW, OptionConstant.DONT_KNOW_BN),

  //AZ_S2_ATTENTION
  AZ_S2_ATTENTION_OPT_1('AZ_S2_ATTENTION', 'Rose', 'গোলাপ'),
  AZ_S2_ATTENTION_OPT_2('AZ_S2_ATTENTION', 'Lemon', 'লেবু'),
  AZ_S2_ATTENTION_OPT_3('AZ_S2_ATTENTION', 'Ball', 'বল'),
  AZ_S2_ATTENTION_OPT_4('AZ_S2_ATTENTION', 'Mosque', 'মসজিদ'),
  AZ_S2_ATTENTION_OPT_5('AZ_S2_ATTENTION', 'Red', 'লাল'),
  AZ_S2_ATTENTION_OPT_6('AZ_S2_ATTENTION', 'Green', 'সবুজ'),
  AZ_S2_ATTENTION_OPT_7('AZ_S2_ATTENTION', 'Sky', 'আকাশ'),
  AZ_S2_ATTENTION_OPT_8('AZ_S2_ATTENTION', 'Cat', 'বিড়াল'),
  AZ_S2_ATTENTION_OPT_9('AZ_S2_ATTENTION', 'Clock', 'ঘড়ি'),

  //AZ_S4_ATTENTION
  AZ_S4_ATTENTION_OPT_1('AZ_S4_ATTENTION', 'Rose', 'গোলাপ'),
  AZ_S4_ATTENTION_OPT_2('AZ_S4_ATTENTION', 'Lemon', 'লেবু'),
  AZ_S4_ATTENTION_OPT_3('AZ_S4_ATTENTION', 'Ball', 'বল'),
  AZ_S4_ATTENTION_OPT_4('AZ_S4_ATTENTION', 'Mosque', 'মসজিদ'),
  AZ_S4_ATTENTION_OPT_5('AZ_S4_ATTENTION', 'Red', 'লাল'),
  AZ_S4_ATTENTION_OPT_6('AZ_S4_ATTENTION', 'Green', 'সবুজ'),
  AZ_S4_ATTENTION_OPT_7('AZ_S4_ATTENTION', 'Sky', 'আকাশ'),
  AZ_S4_ATTENTION_OPT_8('AZ_S4_ATTENTION', 'Cat', 'বিড়াল'),
  AZ_S4_ATTENTION_OPT_9('AZ_S4_ATTENTION', 'Clock', 'ঘড়ি'),

  //AZ_S5_MEMORY_1
  AZ_S5_MEMORY_1_OPT_1('AZ_S5_MEMORY_1', 'Sheikh Hasina', 'শেখ হাসিনা'),
  AZ_S5_MEMORY_1_OPT_2('AZ_S5_MEMORY_1', 'Khaleda Zia', 'খালেদা জিয়া'),
  AZ_S5_MEMORY_1_OPT_3('AZ_S5_MEMORY_1', 'Fakhruddin Ahmed', 'ফখরুদ্দিন আহমেদ'),
  AZ_S5_MEMORY_1_OPT_4('AZ_S5_MEMORY_1', 'Tajuddin Ahmed', 'তাজউদ্দীন আহমেদ'),
  AZ_S5_MEMORY_1_OPT_5('AZ_S5_MEMORY_1', OptionConstant.DONT_KNOW, OptionConstant.DONT_KNOW_BN),

  //AZ_S5_MEMORY_2
  AZ_S5_MEMORY_2_OPT_1('AZ_S5_MEMORY_2', 'Rui', 'রুই'),
  AZ_S5_MEMORY_2_OPT_2('AZ_S5_MEMORY_2', 'Ilish', 'ইলিশ'),
  AZ_S5_MEMORY_2_OPT_3('AZ_S5_MEMORY_2', 'Boaal', 'বোয়াল'),
  AZ_S5_MEMORY_2_OPT_4('AZ_S5_MEMORY_2', 'Magur', 'মাগুর'),
  AZ_S5_MEMORY_2_OPT_5('AZ_S5_MEMORY_2', 'Koi', 'কই'),
  AZ_S5_MEMORY_2_OPT_6('AZ_S5_MEMORY_2', OptionConstant.DONT_KNOW, OptionConstant.DONT_KNOW_BN),

  //AZ_S5_MEMORY_3
  AZ_S5_MEMORY_3_OPT_1('AZ_S5_MEMORY_3', '1947', '১৯৪৭'),
  AZ_S5_MEMORY_3_OPT_2('AZ_S5_MEMORY_3', '1952', '১৯৫২'),
  AZ_S5_MEMORY_3_OPT_3('AZ_S5_MEMORY_3', '1969', '১৯৬৯'),
  AZ_S5_MEMORY_3_OPT_4('AZ_S5_MEMORY_3', '1970', '১৯৭০'),
  AZ_S5_MEMORY_3_OPT_5('AZ_S5_MEMORY_3', '1971', '১৯৭১'),
  AZ_S5_MEMORY_3_OPT_6('AZ_S5_MEMORY_3', '1975', '১৯৭৫'),
  AZ_S5_MEMORY_3_OPT_7('AZ_S5_MEMORY_3', OptionConstant.DONT_KNOW, OptionConstant.DONT_KNOW_BN),

  //AZ_S5_MEMORY_4
  AZ_S5_MEMORY_4_OPT_1('AZ_S5_MEMORY_4', 'Dhaka', 'ঢাকা'),
  AZ_S5_MEMORY_4_OPT_2('AZ_S5_MEMORY_4', 'Chattogram', 'চট্টগ্রাম'),
  AZ_S5_MEMORY_4_OPT_3('AZ_S5_MEMORY_4', 'Rajshahi', 'রাজশাহী'),
  AZ_S5_MEMORY_4_OPT_4('AZ_S5_MEMORY_4', 'Barisal', 'বরিশাল'),
  AZ_S5_MEMORY_4_OPT_5('AZ_S5_MEMORY_4', 'Khulna', 'খুলনা'),
  AZ_S5_MEMORY_4_OPT_6('AZ_S5_MEMORY_4', OptionConstant.DONT_KNOW, OptionConstant.DONT_KNOW_BN),

  //AZ_S5_MEMORY_4
  AZ_S5_MEMORY_5_OPT_1('AZ_S5_MEMORY_5', 'Red', 'লাল'),
  AZ_S5_MEMORY_5_OPT_2('AZ_S5_MEMORY_5', 'Green', 'সবুজ'),
  AZ_S5_MEMORY_5_OPT_3('AZ_S5_MEMORY_5', 'Red & Green', 'লাল ও সবুজ'),
  AZ_S5_MEMORY_5_OPT_4('AZ_S5_MEMORY_5', 'Blue', 'নীল'),
  AZ_S5_MEMORY_5_OPT_5('AZ_S5_MEMORY_5', 'Red & Blue', 'লাল নীল'),
  AZ_S5_MEMORY_5_OPT_6('AZ_S5_MEMORY_5', OptionConstant.DONT_KNOW, OptionConstant.DONT_KNOW_BN),

  //AZ_S6_MEMORY_1
  AZ_S6_MEMORY_1_OPT_1('AZ_S6_MEMORY_1', '500', '৫০০'),
  AZ_S6_MEMORY_1_OPT_2('AZ_S6_MEMORY_1', '50', '৫০'),
  AZ_S6_MEMORY_1_OPT_3('AZ_S6_MEMORY_1', '1000', '১০০০'),
  AZ_S6_MEMORY_1_OPT_4('AZ_S6_MEMORY_1', OptionConstant.DONT_KNOW, OptionConstant.DONT_KNOW_BN),

  //AZ_S6_MEMORY_2
  AZ_S6_MEMORY_2_OPT_1('AZ_S6_MEMORY_2', '100', '১০০'),
  AZ_S6_MEMORY_2_OPT_2('AZ_S6_MEMORY_2', '500', '৫০০'),
  AZ_S6_MEMORY_2_OPT_3('AZ_S6_MEMORY_2', '200', '২০০'),
  AZ_S6_MEMORY_2_OPT_4('AZ_S6_MEMORY_2', OptionConstant.DONT_KNOW, OptionConstant.DONT_KNOW_BN),

  //AZ_S6_MEMORY_3
  AZ_S6_MEMORY_3_OPT_1('AZ_S6_MEMORY_3', '1000', '১০০০'),
  AZ_S6_MEMORY_3_OPT_2('AZ_S6_MEMORY_3', '500', '৫০০'),
  AZ_S6_MEMORY_3_OPT_3('AZ_S6_MEMORY_3', '200', '২০০'),
  AZ_S6_MEMORY_3_OPT_4('AZ_S6_MEMORY_3', OptionConstant.DONT_KNOW, OptionConstant.DONT_KNOW_BN),

  //AZ_S6_MEMORY_4
  AZ_S6_MEMORY_4_OPT_1('AZ_S6_MEMORY_4', '1000', '১০০০'),
  AZ_S6_MEMORY_4_OPT_2('AZ_S6_MEMORY_4', '200', '২০০'),
  AZ_S6_MEMORY_4_OPT_3('AZ_S6_MEMORY_4', '100', '১০০'),
  AZ_S6_MEMORY_4_OPT_4('AZ_S6_MEMORY_4', OptionConstant.DONT_KNOW, OptionConstant.DONT_KNOW_BN),

  //AZ_S6_MEMORY_5
  AZ_S6_MEMORY_5_OPT_1('AZ_S6_MEMORY_5', '10', '১০'),
  AZ_S6_MEMORY_5_OPT_2('AZ_S6_MEMORY_5', '100', '১০০'),
  AZ_S6_MEMORY_5_OPT_3('AZ_S6_MEMORY_5', '50', '৫০'),
  AZ_S6_MEMORY_5_OPT_4('AZ_S6_MEMORY_5', OptionConstant.DONT_KNOW, OptionConstant.DONT_KNOW_BN),

  //AZ_S6_MEMORY_6
  AZ_S6_MEMORY_6_OPT_1('AZ_S6_MEMORY_6', '20', '২০'),
  AZ_S6_MEMORY_6_OPT_2('AZ_S6_MEMORY_6', '10', '১০'),
  AZ_S6_MEMORY_6_OPT_3('AZ_S6_MEMORY_6', '5', '৫'),
  AZ_S6_MEMORY_6_OPT_4('AZ_S6_MEMORY_6', OptionConstant.DONT_KNOW, OptionConstant.DONT_KNOW_BN),

  //AZ_S6_MEMORY_7
  AZ_S6_MEMORY_7_OPT_1('AZ_S6_MEMORY_7', '10', '১০'),
  AZ_S6_MEMORY_7_OPT_2('AZ_S6_MEMORY_7', OptionConstant.ONE, OptionConstant.ONE_BN),
  AZ_S6_MEMORY_7_OPT_3('AZ_S6_MEMORY_7', '2', '২'),
  AZ_S6_MEMORY_7_OPT_4('AZ_S6_MEMORY_7', OptionConstant.DONT_KNOW, OptionConstant.DONT_KNOW_BN),

  //AZ_S6_MEMORY_8
  AZ_S6_MEMORY_8_OPT_1('AZ_S6_MEMORY_8', OptionConstant.ONE, OptionConstant.ONE_BN),
  AZ_S6_MEMORY_8_OPT_2('AZ_S6_MEMORY_8', '2', '২'),
  AZ_S6_MEMORY_8_OPT_3('AZ_S6_MEMORY_8', '5', '৫'),
  AZ_S6_MEMORY_8_OPT_4('AZ_S6_MEMORY_8', OptionConstant.DONT_KNOW, OptionConstant.DONT_KNOW_BN),

  //AZ_S6_MEMORY_9
  AZ_S6_MEMORY_9_OPT_1('AZ_S6_MEMORY_9', '5', '৫'),
  AZ_S6_MEMORY_9_OPT_2('AZ_S6_MEMORY_9', OptionConstant.ONE, OptionConstant.ONE_BN),
  AZ_S6_MEMORY_9_OPT_3('AZ_S6_MEMORY_9', '2', '২'),
  AZ_S6_MEMORY_9_OPT_4('AZ_S6_MEMORY_9', OptionConstant.DONT_KNOW, OptionConstant.DONT_KNOW_BN),

  //AZ_S6_MEMORY_10
  AZ_S6_MEMORY_10_OPT_1('AZ_S6_MEMORY_10', '2', '২'),
  AZ_S6_MEMORY_10_OPT_2('AZ_S6_MEMORY_10', OptionConstant.ONE, OptionConstant.ONE_BN),
  AZ_S6_MEMORY_10_OPT_3('AZ_S6_MEMORY_10', '5', '৫'),
  AZ_S6_MEMORY_10_OPT_4('AZ_S6_MEMORY_10', OptionConstant.DONT_KNOW, OptionConstant.DONT_KNOW_BN),

  //AZ_S7_ATTENTION_1
  AZ_S7_ATTENTION_1_OPT_1('AZ_S7_ATTENTION_1', '1000 Taka', '১০০০ টাকা'),
  AZ_S7_ATTENTION_1_OPT_2('AZ_S7_ATTENTION_1', '25 Taka', '২৫ টাকা'),
  AZ_S7_ATTENTION_1_OPT_3('AZ_S7_ATTENTION_1', '50 Taka', '৫০ টাকা'),
  AZ_S7_ATTENTION_1_OPT_4('AZ_S7_ATTENTION_1', '100 Taka', '১০০ টাকা'),
  AZ_S7_ATTENTION_1_OPT_5('AZ_S7_ATTENTION_1', '500 Taka', '৫০০ টাকা'),
  AZ_S7_ATTENTION_1_OPT_6('AZ_S7_ATTENTION_1', OptionConstant.DONT_KNOW, OptionConstant.DONT_KNOW_BN),

  //AZ_S7_ATTENTION_2
  AZ_S7_ATTENTION_2_OPT_1('AZ_S7_ATTENTION_2', '100 Taka', '১০০ টাকা'),
  AZ_S7_ATTENTION_2_OPT_2('AZ_S7_ATTENTION_2', '25 Taka', '২৫ টাকা'),
  AZ_S7_ATTENTION_2_OPT_3('AZ_S7_ATTENTION_2', '50 Taka', '৫০ টাকা'),
  AZ_S7_ATTENTION_2_OPT_4('AZ_S7_ATTENTION_2', '30 Taka', '৩০ টাকা'),
  AZ_S7_ATTENTION_2_OPT_5('AZ_S7_ATTENTION_2', '20 Taka', '২০ টাকা'),
  AZ_S7_ATTENTION_2_OPT_6('AZ_S7_ATTENTION_2', OptionConstant.DONT_KNOW, OptionConstant.DONT_KNOW_BN),

  //AZ_S7_ATTENTION_3
  AZ_S7_ATTENTION_3_OPT_1('AZ_S7_ATTENTION_3', '100 Taka', '১০০ টাকা'),
  AZ_S7_ATTENTION_3_OPT_2('AZ_S7_ATTENTION_3', '250 Taka', '২৫০ টাকা'),
  AZ_S7_ATTENTION_3_OPT_3('AZ_S7_ATTENTION_3', '300 Taka', '৩০০ টাকা'),
  AZ_S7_ATTENTION_3_OPT_4('AZ_S7_ATTENTION_3', '400 Taka', '৪০০ টাকা'),
  AZ_S7_ATTENTION_3_OPT_5('AZ_S7_ATTENTION_3', '150 Taka', '১৫০ টাকা'),
  AZ_S7_ATTENTION_3_OPT_6('AZ_S7_ATTENTION_3', OptionConstant.DONT_KNOW, OptionConstant.DONT_KNOW_BN),

  //AZ_S8_NAMING_1
  AZ_S8_NAMING_1_OPT_1('AZ_S8_NAMING_1', 'Teeth', 'দাঁত'),
  AZ_S8_NAMING_1_OPT_2('AZ_S8_NAMING_1', 'Forehead', 'কপাল'),
  AZ_S8_NAMING_1_OPT_3('AZ_S8_NAMING_1', 'Leg/Knee', 'পা/হাঁটু'),
  AZ_S8_NAMING_1_OPT_4('AZ_S8_NAMING_1', 'Hair', 'চুল'),
  AZ_S8_NAMING_1_OPT_5('AZ_S8_NAMING_1', 'Finger', 'আঙুল'),
  AZ_S8_NAMING_1_OPT_6('AZ_S8_NAMING_1', OptionConstant.DONT_KNOW, OptionConstant.DONT_KNOW_BN),

  //AZ_S8_NAMING_2
  AZ_S8_NAMING_2_OPT_1('AZ_S8_NAMING_2', 'Teeth', 'দাঁত'),
  AZ_S8_NAMING_2_OPT_2('AZ_S8_NAMING_2', 'Forehead', 'কপাল'),
  AZ_S8_NAMING_2_OPT_3('AZ_S8_NAMING_2', 'Leg/Knee', 'পা/হাঁটু'),
  AZ_S8_NAMING_2_OPT_4('AZ_S8_NAMING_2', 'Hair', 'চুল'),
  AZ_S8_NAMING_2_OPT_5('AZ_S8_NAMING_2', 'Eye', 'চোখ'),
  AZ_S8_NAMING_2_OPT_6('AZ_S8_NAMING_2', OptionConstant.DONT_KNOW, OptionConstant.DONT_KNOW_BN),

  //AZ_S8_NAMING_3
  AZ_S8_NAMING_3_OPT_1('AZ_S8_NAMING_3', 'Teeth', 'দাঁত'),
  AZ_S8_NAMING_3_OPT_2('AZ_S8_NAMING_3', 'Ear', 'কান'),
  AZ_S8_NAMING_3_OPT_3('AZ_S8_NAMING_3', 'Leg/Knee', 'পা/হাঁটু'),
  AZ_S8_NAMING_3_OPT_4('AZ_S8_NAMING_3', 'Hair', 'চুল'),
  AZ_S8_NAMING_3_OPT_5('AZ_S8_NAMING_3', 'Finger', 'আঙুল'),
  AZ_S8_NAMING_3_OPT_6('AZ_S8_NAMING_3', OptionConstant.DONT_KNOW, OptionConstant.DONT_KNOW_BN),

  //AZ_S8_NAMING_4
  AZ_S8_NAMING_4_OPT_1('AZ_S8_NAMING_4', 'Teeth', 'দাঁত'),
  AZ_S8_NAMING_4_OPT_2('AZ_S8_NAMING_4', 'Forehead', 'কপাল'),
  AZ_S8_NAMING_4_OPT_3('AZ_S8_NAMING_4', 'Eye', 'চোখ'),
  AZ_S8_NAMING_4_OPT_4('AZ_S8_NAMING_4', 'Nose', 'নাক'),
  AZ_S8_NAMING_4_OPT_5('AZ_S8_NAMING_4', 'Leg/Knee', 'পা/হাঁটু'),
  AZ_S8_NAMING_4_OPT_6('AZ_S8_NAMING_4', OptionConstant.DONT_KNOW, OptionConstant.DONT_KNOW_BN),

  //AZ_S8_NAMING_5
  AZ_S8_NAMING_5_OPT_1('AZ_S8_NAMING_5', 'Teeth', 'দাঁত'),
  AZ_S8_NAMING_5_OPT_2('AZ_S8_NAMING_5', 'Arm/Elbow', 'বাহু/কনুই'),
  AZ_S8_NAMING_5_OPT_3('AZ_S8_NAMING_5', 'Leg/Knee', 'পা/হাঁটু'),
  AZ_S8_NAMING_5_OPT_4('AZ_S8_NAMING_5', 'Nose', 'নাক'),
  AZ_S8_NAMING_5_OPT_5('AZ_S8_NAMING_5', 'Eye', 'চোখ'),
  AZ_S8_NAMING_5_OPT_6('AZ_S8_NAMING_5', OptionConstant.DONT_KNOW, OptionConstant.DONT_KNOW_BN),

  //AZ_S8_NAMING_6
  AZ_S8_NAMING_6_OPT_1('AZ_S8_NAMING_6', 'Teeth', 'দাঁত'),
  AZ_S8_NAMING_6_OPT_2('AZ_S8_NAMING_6', 'Arm/Elbow', 'বাহু/কনুই'),
  AZ_S8_NAMING_6_OPT_3('AZ_S8_NAMING_6', 'Leg/Knee', 'পা/হাঁটু'),
  AZ_S8_NAMING_6_OPT_4('AZ_S8_NAMING_6', 'Nose', 'নাক'),
  AZ_S8_NAMING_6_OPT_5('AZ_S8_NAMING_6', 'Eye', 'চোখ'),
  AZ_S8_NAMING_6_OPT_6('AZ_S8_NAMING_6', OptionConstant.DONT_KNOW, OptionConstant.DONT_KNOW_BN),

  //AZ_S8_NAMING_7
  AZ_S8_NAMING_7_OPT_1('AZ_S8_NAMING_7', 'Teeth', 'দাঁত'),
  AZ_S8_NAMING_7_OPT_2('AZ_S8_NAMING_7', 'Arm/Elbow', 'বাহু/কনুই'),
  AZ_S8_NAMING_7_OPT_3('AZ_S8_NAMING_7', 'Leg/Knee', 'পা/হাঁটু'),
  AZ_S8_NAMING_7_OPT_4('AZ_S8_NAMING_7', 'Forehead', 'কপাল'),
  AZ_S8_NAMING_7_OPT_5('AZ_S8_NAMING_7', 'Eye', 'চোখ'),
  AZ_S8_NAMING_7_OPT_6('AZ_S8_NAMING_7', OptionConstant.DONT_KNOW, OptionConstant.DONT_KNOW_BN),

  //AZ_S8_NAMING_8
  AZ_S8_NAMING_8_OPT_1('AZ_S8_NAMING_8', 'Teeth', 'দাঁত'),
  AZ_S8_NAMING_8_OPT_2('AZ_S8_NAMING_8', 'Arm/Elbow', 'বাহু/কনুই'),
  AZ_S8_NAMING_8_OPT_3('AZ_S8_NAMING_8', 'Leg/Knee', 'পা/হাঁটু'),
  AZ_S8_NAMING_8_OPT_4('AZ_S8_NAMING_8', 'Forehead', 'কপাল'),
  AZ_S8_NAMING_8_OPT_5('AZ_S8_NAMING_8', 'Neck', 'ঘাড়'),
  AZ_S8_NAMING_8_OPT_6('AZ_S8_NAMING_8', OptionConstant.DONT_KNOW, OptionConstant.DONT_KNOW_BN),

  //AZ_S8_NAMING_9
  AZ_S8_NAMING_9_OPT_1('AZ_S8_NAMING_9', 'Teeth', 'দাঁত'),
  AZ_S8_NAMING_9_OPT_2('AZ_S8_NAMING_9', 'Hand', 'হাত'),
  AZ_S8_NAMING_9_OPT_3('AZ_S8_NAMING_9', 'Leg/Knee', 'পা/হাঁটু'),
  AZ_S8_NAMING_9_OPT_4('AZ_S8_NAMING_9', 'Forehead', 'কপাল'),
  AZ_S8_NAMING_9_OPT_5('AZ_S8_NAMING_9', 'Neck', 'ঘাড়'),
  AZ_S8_NAMING_9_OPT_6('AZ_S8_NAMING_9', OptionConstant.DONT_KNOW, OptionConstant.DONT_KNOW_BN),

  //AZ_S8_NAMING_10
  AZ_S8_NAMING_10_OPT_1('AZ_S8_NAMING_10', 'Teeth', 'দাঁত'),
  AZ_S8_NAMING_10_OPT_2('AZ_S8_NAMING_10', 'Hand', 'হাত'),
  AZ_S8_NAMING_10_OPT_3('AZ_S8_NAMING_10', 'Leg/Knee', 'পা/হাঁটু'),
  AZ_S8_NAMING_10_OPT_4('AZ_S8_NAMING_10', 'Feet', 'পায়ের পাতা'),
  AZ_S8_NAMING_10_OPT_5('AZ_S8_NAMING_10', 'Neck', 'ঘাড়'),
  AZ_S8_NAMING_10_OPT_6('AZ_S8_NAMING_10', OptionConstant.DONT_KNOW, OptionConstant.DONT_KNOW_BN),

  //AZ_S8_NAMING_11
  AZ_S8_NAMING_11_OPT_1('AZ_S8_NAMING_11', 'Hair', 'চুল'),
  AZ_S8_NAMING_11_OPT_2('AZ_S8_NAMING_11', 'Hand', 'হাত'),
  AZ_S8_NAMING_11_OPT_3('AZ_S8_NAMING_11', 'Leg/Knee', 'পা/হাঁটু'),
  AZ_S8_NAMING_11_OPT_4('AZ_S8_NAMING_11', 'Feet', 'পায়ের পাতা'),
  AZ_S8_NAMING_11_OPT_5('AZ_S8_NAMING_11', 'Finger', 'আঙুল'),
  AZ_S8_NAMING_11_OPT_6('AZ_S8_NAMING_11', OptionConstant.DONT_KNOW, OptionConstant.DONT_KNOW_BN),

  //AZ_S8_NAMING_12
  AZ_S8_NAMING_12_OPT_1('AZ_S8_NAMING_12', 'Hair', 'চুল'),
  AZ_S8_NAMING_12_OPT_2('AZ_S8_NAMING_12', 'Hand', 'হাত'),
  AZ_S8_NAMING_12_OPT_3('AZ_S8_NAMING_12', 'Leg/Knee', 'পা/হাঁটু'),
  AZ_S8_NAMING_12_OPT_4('AZ_S8_NAMING_12', 'Feet', 'পায়ের পাতা'),
  AZ_S8_NAMING_12_OPT_5('AZ_S8_NAMING_12', 'Finger', 'আঙুল'),
  AZ_S8_NAMING_12_OPT_6('AZ_S8_NAMING_12', OptionConstant.DONT_KNOW, OptionConstant.DONT_KNOW_BN),

  //AZ_S9_NAMING_1
  AZ_S9_NAMING_1_OPT_1('AZ_S9_NAMING_1', 'Hen', 'মুরগি'),
  AZ_S9_NAMING_1_OPT_2('AZ_S9_NAMING_1', 'Train', 'ট্রেন'),
  AZ_S9_NAMING_1_OPT_3('AZ_S9_NAMING_1', 'Teeth', 'দাঁত'),
  AZ_S9_NAMING_1_OPT_4('AZ_S9_NAMING_1', 'Plate', 'প্লেট'),
  AZ_S9_NAMING_1_OPT_5('AZ_S9_NAMING_1', 'Watch', 'ঘড়ি'),
  AZ_S9_NAMING_1_OPT_6('AZ_S9_NAMING_1', 'Mango', 'আম'),
  AZ_S9_NAMING_1_OPT_7('AZ_S9_NAMING_1', OptionConstant.DONT_KNOW, OptionConstant.DONT_KNOW_BN),


  //AZ_S9_NAMING_2
  AZ_S9_NAMING_2_OPT_1('AZ_S9_NAMING_2', 'Goat', 'ছাগল'),
  AZ_S9_NAMING_2_OPT_2('AZ_S9_NAMING_2', 'Boat', 'নৌকা'),
  AZ_S9_NAMING_2_OPT_3('AZ_S9_NAMING_2', 'Boti', 'বটি'),
  AZ_S9_NAMING_2_OPT_4('AZ_S9_NAMING_2', 'Book', 'বই'),
  AZ_S9_NAMING_2_OPT_5('AZ_S9_NAMING_2', 'Hen', 'মুরগি'),
  AZ_S9_NAMING_2_OPT_6('AZ_S9_NAMING_2', 'Dog', 'কুকুর'),
  AZ_S9_NAMING_2_OPT_7('AZ_S9_NAMING_2', OptionConstant.DONT_KNOW, OptionConstant.DONT_KNOW_BN),

  //AZ_S9_NAMING_3
  AZ_S9_NAMING_3_OPT_1('AZ_S9_NAMING_3', 'Book', 'বই'),
  AZ_S9_NAMING_3_OPT_2('AZ_S9_NAMING_3', 'Rickshaw', 'রিকশা'),
  AZ_S9_NAMING_3_OPT_3('AZ_S9_NAMING_3', 'Train', 'ট্রেন'),
  AZ_S9_NAMING_3_OPT_4('AZ_S9_NAMING_3', 'Plate', 'প্লেট'),
  AZ_S9_NAMING_3_OPT_5('AZ_S9_NAMING_3', 'Mango', 'আম'),
  AZ_S9_NAMING_3_OPT_6('AZ_S9_NAMING_3', 'Boti', 'বটি'),
  AZ_S9_NAMING_3_OPT_7('AZ_S9_NAMING_3', OptionConstant.DONT_KNOW, OptionConstant.DONT_KNOW_BN),

  //AZ_S9_NAMING_4
  AZ_S9_NAMING_4_OPT_1('AZ_S9_NAMING_4', 'Train', 'ট্রেন'),
  AZ_S9_NAMING_4_OPT_2('AZ_S9_NAMING_4', 'Duck', 'হাঁস'),
  AZ_S9_NAMING_4_OPT_3('AZ_S9_NAMING_4', 'Mango', 'আম'),
  AZ_S9_NAMING_4_OPT_4('AZ_S9_NAMING_4', 'Boti', 'বটি'),
  AZ_S9_NAMING_4_OPT_5('AZ_S9_NAMING_4', 'Rickshaw', 'রিকশা'),
  AZ_S9_NAMING_4_OPT_6('AZ_S9_NAMING_4', 'Cow', 'গাভী'),
  AZ_S9_NAMING_4_OPT_7('AZ_S9_NAMING_4', OptionConstant.DONT_KNOW, OptionConstant.DONT_KNOW_BN),

  //AZ_S9_NAMING_5
  AZ_S9_NAMING_5_OPT_1('AZ_S9_NAMING_5', 'Boat', 'নৌকা'),
  AZ_S9_NAMING_5_OPT_2('AZ_S9_NAMING_5', 'Patil', 'পাতিল'),
  AZ_S9_NAMING_5_OPT_3('AZ_S9_NAMING_5', 'Bus', 'বাস'),
  AZ_S9_NAMING_5_OPT_4('AZ_S9_NAMING_5', 'Mango', 'আম'),
  AZ_S9_NAMING_5_OPT_5('AZ_S9_NAMING_5', 'Train', 'ট্রেন'),
  AZ_S9_NAMING_5_OPT_6('AZ_S9_NAMING_5', 'Boti', 'বটি'),
  AZ_S9_NAMING_5_OPT_7('AZ_S9_NAMING_5', OptionConstant.DONT_KNOW, OptionConstant.DONT_KNOW_BN),

  //AZ_S9_NAMING_6
  AZ_S9_NAMING_6_OPT_1('AZ_S9_NAMING_6', 'Cow', 'গাভী'),
  AZ_S9_NAMING_6_OPT_2('AZ_S9_NAMING_6', 'Patil', 'পাতিল'),
  AZ_S9_NAMING_6_OPT_3('AZ_S9_NAMING_6', 'Goat', 'ছাগল'),
  AZ_S9_NAMING_6_OPT_4('AZ_S9_NAMING_6', 'Duck', 'হাঁস'),
  AZ_S9_NAMING_6_OPT_5('AZ_S9_NAMING_6', 'Book', 'বই'),
  AZ_S9_NAMING_6_OPT_6('AZ_S9_NAMING_6', 'Hen', 'মুরগি'),
  AZ_S9_NAMING_6_OPT_7('AZ_S9_NAMING_6', OptionConstant.DONT_KNOW, OptionConstant.DONT_KNOW_BN),

  //AZ_S9_NAMING_7
  AZ_S9_NAMING_7_OPT_1('AZ_S9_NAMING_7', 'Goat', 'ছাগল'),
  AZ_S9_NAMING_7_OPT_2('AZ_S9_NAMING_7', 'Boat', 'নৌকা'),
  AZ_S9_NAMING_7_OPT_3('AZ_S9_NAMING_7', 'Dog', 'কুকুর'),
  AZ_S9_NAMING_7_OPT_4('AZ_S9_NAMING_7', 'Pen', 'কলম'),
  AZ_S9_NAMING_7_OPT_5('AZ_S9_NAMING_7', 'Watch', 'ঘড়ি'),
  AZ_S9_NAMING_7_OPT_6('AZ_S9_NAMING_7', 'Guava', 'পেয়ারা'),
  AZ_S9_NAMING_7_OPT_7('AZ_S9_NAMING_7', OptionConstant.DONT_KNOW, OptionConstant.DONT_KNOW_BN),

  //AZ_S9_NAMING_8
  AZ_S9_NAMING_8_OPT_1('AZ_S9_NAMING_8', 'Cow', 'গাভী'),
  AZ_S9_NAMING_8_OPT_2('AZ_S9_NAMING_8', 'Train', 'ট্রেন'),
  AZ_S9_NAMING_8_OPT_3('AZ_S9_NAMING_8', 'Duck', 'হাঁস'),
  AZ_S9_NAMING_8_OPT_4('AZ_S9_NAMING_8', 'Patil', 'পাতিল'),
  AZ_S9_NAMING_8_OPT_5('AZ_S9_NAMING_8', 'Goat', 'ছাগল'),
  AZ_S9_NAMING_8_OPT_6('AZ_S9_NAMING_8', 'Rickshaw', 'রিকশা'),
  AZ_S9_NAMING_8_OPT_7('AZ_S9_NAMING_8', OptionConstant.DONT_KNOW, OptionConstant.DONT_KNOW_BN),

  //AZ_S9_NAMING_9
  AZ_S9_NAMING_9_OPT_1('AZ_S9_NAMING_9', 'Plate', 'প্লেট'),
  AZ_S9_NAMING_9_OPT_2('AZ_S9_NAMING_9', 'Boti', 'বটি'),
  AZ_S9_NAMING_9_OPT_3('AZ_S9_NAMING_9', 'Mango', 'আম'),
  AZ_S9_NAMING_9_OPT_4('AZ_S9_NAMING_9', 'Bus', 'বাস'),
  AZ_S9_NAMING_9_OPT_5('AZ_S9_NAMING_9', 'Guava', 'পেয়ারা'),
  AZ_S9_NAMING_9_OPT_6('AZ_S9_NAMING_9', 'Goat', 'ছাগল'),
  AZ_S9_NAMING_9_OPT_7('AZ_S9_NAMING_9', OptionConstant.DONT_KNOW, OptionConstant.DONT_KNOW_BN),

  //AZ_S9_NAMING_10
  AZ_S9_NAMING_10_OPT_1('AZ_S9_NAMING_10', 'Cow', 'গাভী'),
  AZ_S9_NAMING_10_OPT_2('AZ_S9_NAMING_10', 'Watch', 'ঘড়ি'),
  AZ_S9_NAMING_10_OPT_3('AZ_S9_NAMING_10', 'Plate', 'প্লেট'),
  AZ_S9_NAMING_10_OPT_4('AZ_S9_NAMING_10', 'Guava', 'পেয়ারা'),
  AZ_S9_NAMING_10_OPT_5('AZ_S9_NAMING_10', 'Train', 'ট্রেন'),
  AZ_S9_NAMING_10_OPT_6('AZ_S9_NAMING_10', 'Mango', 'আম'),
  AZ_S9_NAMING_10_OPT_7('AZ_S9_NAMING_10', OptionConstant.DONT_KNOW, OptionConstant.DONT_KNOW_BN),

  //AZ_S9_NAMING_11
  AZ_S9_NAMING_11_OPT_1('AZ_S9_NAMING_11', 'Boti', 'বটি'),
  AZ_S9_NAMING_11_OPT_2('AZ_S9_NAMING_11', 'Duck', 'হাঁস'),
  AZ_S9_NAMING_11_OPT_3('AZ_S9_NAMING_11', 'Train', 'ট্রেন'),
  AZ_S9_NAMING_11_OPT_4('AZ_S9_NAMING_11', 'Jackfruit', 'কাঁঠাল'),
  AZ_S9_NAMING_11_OPT_5('AZ_S9_NAMING_11', 'Pen', 'কলম'),
  AZ_S9_NAMING_11_OPT_6('AZ_S9_NAMING_11', 'Hen', 'মুরগি'),
  AZ_S9_NAMING_11_OPT_7('AZ_S9_NAMING_11', OptionConstant.DONT_KNOW, OptionConstant.DONT_KNOW_BN),

  //AZ_S9_NAMING_12
  AZ_S9_NAMING_12_OPT_1('AZ_S9_NAMING_12', 'Mango', 'আম'),
  AZ_S9_NAMING_12_OPT_2('AZ_S9_NAMING_12', 'Jackfruit', 'কাঁঠাল'),
  AZ_S9_NAMING_12_OPT_3('AZ_S9_NAMING_12', 'Book', 'বই'),
  AZ_S9_NAMING_12_OPT_4('AZ_S9_NAMING_12', 'Guava', 'পেয়ারা'),
  AZ_S9_NAMING_12_OPT_5('AZ_S9_NAMING_12', 'Cow', 'গাভী'),
  AZ_S9_NAMING_12_OPT_6('AZ_S9_NAMING_12', 'Boti', 'বটি'),
  AZ_S9_NAMING_12_OPT_7('AZ_S9_NAMING_12', OptionConstant.DONT_KNOW, OptionConstant.DONT_KNOW_BN),

  //AZ_S9_NAMING_13
  AZ_S9_NAMING_13_OPT_1('AZ_S9_NAMING_13', 'Rickshaw', 'রিকশা'),
  AZ_S9_NAMING_13_OPT_2('AZ_S9_NAMING_13', 'Patil', 'পাতিল'),
  AZ_S9_NAMING_13_OPT_3('AZ_S9_NAMING_13', 'Pen', 'কলম'),
  AZ_S9_NAMING_13_OPT_4('AZ_S9_NAMING_13', 'Goat', 'ছাগল'),
  AZ_S9_NAMING_13_OPT_5('AZ_S9_NAMING_13', 'Jackfruit', 'কাঁঠাল'),
  AZ_S9_NAMING_13_OPT_6('AZ_S9_NAMING_13', 'Jug', 'জগ'),
  AZ_S9_NAMING_13_OPT_7('AZ_S9_NAMING_13', OptionConstant.DONT_KNOW, OptionConstant.DONT_KNOW_BN),

  //AZ_S9_NAMING_14
  AZ_S9_NAMING_14_OPT_1('AZ_S9_NAMING_14', 'Watch', 'ঘড়ি'),
  AZ_S9_NAMING_14_OPT_2('AZ_S9_NAMING_14', 'Rickshaw', 'রিকশা'),
  AZ_S9_NAMING_14_OPT_3('AZ_S9_NAMING_14', 'Duck', 'হাঁস'),
  AZ_S9_NAMING_14_OPT_4('AZ_S9_NAMING_14', 'Dog', 'কুকুর'),
  AZ_S9_NAMING_14_OPT_5('AZ_S9_NAMING_14', 'Bus', 'বাস'),
  AZ_S9_NAMING_14_OPT_6('AZ_S9_NAMING_14', 'Mango', 'আম'),
  AZ_S9_NAMING_14_OPT_7('AZ_S9_NAMING_14', OptionConstant.DONT_KNOW, OptionConstant.DONT_KNOW_BN),

  //AZ_S9_NAMING_15
  AZ_S9_NAMING_15_OPT_1('AZ_S9_NAMING_15', 'Boat', 'নৌকা'),
  AZ_S9_NAMING_15_OPT_2('AZ_S9_NAMING_15', 'Dog', 'কুকুর'),
  AZ_S9_NAMING_15_OPT_3('AZ_S9_NAMING_15', 'Book', 'বই'),
  AZ_S9_NAMING_15_OPT_4('AZ_S9_NAMING_15', 'Patil', 'পাতিল'),
  AZ_S9_NAMING_15_OPT_5('AZ_S9_NAMING_15', 'Boti', 'বটি'),
  AZ_S9_NAMING_15_OPT_6('AZ_S9_NAMING_15', 'Train', 'ট্রেন'),
  AZ_S9_NAMING_15_OPT_7('AZ_S9_NAMING_15', OptionConstant.DONT_KNOW, OptionConstant.DONT_KNOW_BN),

  //AZ_S9_NAMING_16
  AZ_S9_NAMING_16_OPT_1('AZ_S9_NAMING_16', 'Jackfruit', 'কাঁঠাল'),
  AZ_S9_NAMING_16_OPT_2('AZ_S9_NAMING_16', 'Jug', 'জগ'),
  AZ_S9_NAMING_16_OPT_3('AZ_S9_NAMING_16', 'Banana', 'কলা'),
  AZ_S9_NAMING_16_OPT_4('AZ_S9_NAMING_16', 'Pen', 'কলম'),
  AZ_S9_NAMING_16_OPT_5('AZ_S9_NAMING_16', 'Book', 'বই'),
  AZ_S9_NAMING_16_OPT_6('AZ_S9_NAMING_16', 'Dog', 'কুকুর'),
  AZ_S9_NAMING_16_OPT_7('AZ_S9_NAMING_16', OptionConstant.DONT_KNOW, OptionConstant.DONT_KNOW_BN),

  //AZ_S9_NAMING_17
  AZ_S9_NAMING_17_OPT_1('AZ_S9_NAMING_17', 'Book', 'বই'),
  AZ_S9_NAMING_17_OPT_2('AZ_S9_NAMING_17', 'Plate', 'প্লেট'),
  AZ_S9_NAMING_17_OPT_3('AZ_S9_NAMING_17', 'Rickshaw', 'রিকশা'),
  AZ_S9_NAMING_17_OPT_4('AZ_S9_NAMING_17', 'Cow', 'গাভী'),
  AZ_S9_NAMING_17_OPT_5('AZ_S9_NAMING_17', 'Boat', 'নৌকা'),
  AZ_S9_NAMING_17_OPT_6('AZ_S9_NAMING_17', 'Goat', 'নৌকা'),
  AZ_S9_NAMING_17_OPT_7('AZ_S9_NAMING_17', OptionConstant.DONT_KNOW, OptionConstant.DONT_KNOW_BN),

  //AZ_S9_NAMING_18
  AZ_S9_NAMING_18_OPT_1('AZ_S9_NAMING_18', 'Goat', 'ছাগল'),
  AZ_S9_NAMING_18_OPT_2('AZ_S9_NAMING_18', 'Rickshaw', 'রিকশা'),
  AZ_S9_NAMING_18_OPT_3('AZ_S9_NAMING_18', 'Hen', 'মুরগি'),
  AZ_S9_NAMING_18_OPT_4('AZ_S9_NAMING_18', 'Pen', 'কলম'),
  AZ_S9_NAMING_18_OPT_5('AZ_S9_NAMING_18', 'Boti', 'বটি'),
  AZ_S9_NAMING_18_OPT_6('AZ_S9_NAMING_18', 'Book', 'বই'),
  AZ_S9_NAMING_18_OPT_7('AZ_S9_NAMING_18', OptionConstant.DONT_KNOW, OptionConstant.DONT_KNOW_BN),

  //AZ_S9_NAMING_19
  AZ_S9_NAMING_19_OPT_1('AZ_S9_NAMING_19', 'Pen', 'কলম'),
  AZ_S9_NAMING_19_OPT_2('AZ_S9_NAMING_19', 'Cow', 'গাভী'),
  AZ_S9_NAMING_19_OPT_3('AZ_S9_NAMING_19', 'Bus', 'বাস'),
  AZ_S9_NAMING_19_OPT_4('AZ_S9_NAMING_19', 'Train', 'ট্রেন'),
  AZ_S9_NAMING_19_OPT_5('AZ_S9_NAMING_19', 'Jackfruit', 'কাঁঠাল'),
  AZ_S9_NAMING_19_OPT_6('AZ_S9_NAMING_19', 'Patil', 'পাতিল'),
  AZ_S9_NAMING_19_OPT_7('AZ_S9_NAMING_19', OptionConstant.DONT_KNOW, OptionConstant.DONT_KNOW_BN),

  //AZ_S9_NAMING_20
  AZ_S9_NAMING_20_OPT_1('AZ_S9_NAMING_20', 'Patil', 'পাতিল'),
  AZ_S9_NAMING_20_OPT_2('AZ_S9_NAMING_20', 'Duck', 'হাঁস'),
  AZ_S9_NAMING_20_OPT_3('AZ_S9_NAMING_20', 'Dog', 'কুকুর'),
  AZ_S9_NAMING_20_OPT_4('AZ_S9_NAMING_20', 'Hen', 'মুরগি'),
  AZ_S9_NAMING_20_OPT_5('AZ_S9_NAMING_20', 'Train', 'ট্রেন'),
  AZ_S9_NAMING_20_OPT_6('AZ_S9_NAMING_20', 'Watch', 'ঘড়ি'),
  AZ_S9_NAMING_20_OPT_7('AZ_S9_NAMING_20', OptionConstant.DONT_KNOW, OptionConstant.DONT_KNOW_BN),

  //AZ_S9_NAMING_21
  AZ_S9_NAMING_21_OPT_1('AZ_S9_NAMING_21', 'Banana', 'কলা'),
  AZ_S9_NAMING_21_OPT_2('AZ_S9_NAMING_21', 'Hen', 'মুরগি'),
  AZ_S9_NAMING_21_OPT_3('AZ_S9_NAMING_21', 'Bus', 'বাস'),
  AZ_S9_NAMING_21_OPT_4('AZ_S9_NAMING_21', 'Dog', 'কুকুর'),
  AZ_S9_NAMING_21_OPT_5('AZ_S9_NAMING_21', 'Jug', 'জগ'),
  AZ_S9_NAMING_21_OPT_6('AZ_S9_NAMING_21', 'Book', 'বই'),
  AZ_S9_NAMING_21_OPT_7('AZ_S9_NAMING_21', OptionConstant.DONT_KNOW, OptionConstant.DONT_KNOW_BN),

  //AZ_S10_LANGUAGE_1_B
  AZ_S10_LANGUAGE_1_B_OPT_1('AZ_S10_LANGUAGE_1_B', 'Book', 'বই'),
  AZ_S10_LANGUAGE_1_B_OPT_2('AZ_S10_LANGUAGE_1_B', 'Hand', 'হাত'),
  AZ_S10_LANGUAGE_1_B_OPT_3('AZ_S10_LANGUAGE_1_B', 'Guava', 'পেয়ারা'),
  AZ_S10_LANGUAGE_1_B_OPT_4('AZ_S10_LANGUAGE_1_B', 'Plate', 'প্লেট'),
  AZ_S10_LANGUAGE_1_B_OPT_5('AZ_S10_LANGUAGE_1_B', 'Rickshaw', 'রিকশা'),
  AZ_S10_LANGUAGE_1_B_OPT_6('AZ_S10_LANGUAGE_1_B', 'Dog', 'কুকুর'),
  AZ_S10_LANGUAGE_1_B_OPT_7('AZ_S10_LANGUAGE_1_B', OptionConstant.DONT_KNOW, OptionConstant.DONT_KNOW_BN),

  //AZ_S10_LANGUAGE_2_B
  AZ_S10_LANGUAGE_2_B_OPT_1('AZ_S10_LANGUAGE_2_B', 'Book', 'বই'),
  AZ_S10_LANGUAGE_2_B_OPT_2('AZ_S10_LANGUAGE_2_B', 'Hand', 'হাত'),
  AZ_S10_LANGUAGE_2_B_OPT_3('AZ_S10_LANGUAGE_2_B', 'Guava', 'পেয়ারা'),
  AZ_S10_LANGUAGE_2_B_OPT_4('AZ_S10_LANGUAGE_2_B', 'Plate', 'প্লেট'),
  AZ_S10_LANGUAGE_2_B_OPT_5('AZ_S10_LANGUAGE_2_B', 'Rickshaw', 'রিকশা'),
  AZ_S10_LANGUAGE_2_B_OPT_6('AZ_S10_LANGUAGE_2_B', 'Dog', 'কুকুর'),
  AZ_S10_LANGUAGE_2_B_OPT_7('AZ_S10_LANGUAGE_2_B', OptionConstant.DONT_KNOW, OptionConstant.DONT_KNOW_BN),

  //AZ_S10_LANGUAGE_3_B
  AZ_S10_LANGUAGE_3_B_OPT_1('AZ_S10_LANGUAGE_3_B', 'Book', 'বই'),
  AZ_S10_LANGUAGE_3_B_OPT_2('AZ_S10_LANGUAGE_3_B', 'Hand', 'হাত'),
  AZ_S10_LANGUAGE_3_B_OPT_3('AZ_S10_LANGUAGE_3_B', 'Guava', 'পেয়ারা'),
  AZ_S10_LANGUAGE_3_B_OPT_4('AZ_S10_LANGUAGE_3_B', 'Plate', 'প্লেট'),
  AZ_S10_LANGUAGE_3_B_OPT_5('AZ_S10_LANGUAGE_3_B', 'Rickshaw', 'রিকশা'),
  AZ_S10_LANGUAGE_3_B_OPT_6('AZ_S10_LANGUAGE_3_B', 'Cow', 'গরু'),
  AZ_S10_LANGUAGE_3_B_OPT_7('AZ_S10_LANGUAGE_3_B', OptionConstant.DONT_KNOW, OptionConstant.DONT_KNOW_BN),

  //AZ_S10_LANGUAGE_4_B
  AZ_S10_LANGUAGE_4_B_OPT_1('AZ_S10_LANGUAGE_4_B', 'Book', 'বই'),
  AZ_S10_LANGUAGE_4_B_OPT_2('AZ_S10_LANGUAGE_4_B', 'Hand', 'হাত'),
  AZ_S10_LANGUAGE_4_B_OPT_3('AZ_S10_LANGUAGE_4_B', 'Guava', 'পেয়ারা'),
  AZ_S10_LANGUAGE_4_B_OPT_4('AZ_S10_LANGUAGE_4_B', 'Plate', 'প্লেট'),
  AZ_S10_LANGUAGE_4_B_OPT_5('AZ_S10_LANGUAGE_4_B', 'Rickshaw', 'রিকশা'),
  AZ_S10_LANGUAGE_4_B_OPT_6('AZ_S10_LANGUAGE_4_B', 'Dog', 'কুকুর'),
  AZ_S10_LANGUAGE_4_B_OPT_7('AZ_S10_LANGUAGE_4_B', OptionConstant.DONT_KNOW, OptionConstant.DONT_KNOW_BN),

  //AZ_S13_FLUENCY
  AZ_S13_FLUENCY_OPT_1('AZ_S13_FLUENCY', '0-1', '০-১'),
  AZ_S13_FLUENCY_OPT_2('AZ_S13_FLUENCY', '2-3', '২-৩'),
  AZ_S13_FLUENCY_OPT_3('AZ_S13_FLUENCY', '4-5', '৪-৫'),
  AZ_S13_FLUENCY_OPT_4('AZ_S13_FLUENCY', '6-7', '৬-৭'),
  AZ_S13_FLUENCY_OPT_5('AZ_S13_FLUENCY', '8-10', '৮-১০'),
  AZ_S13_FLUENCY_OPT_6('AZ_S13_FLUENCY', '11-13', '১১-১৩'),
  AZ_S13_FLUENCY_OPT_7('AZ_S13_FLUENCY', '14-17', '১৪-১৭'),
  AZ_S13_FLUENCY_OPT_8('AZ_S13_FLUENCY', '18 or more than 18', '১৮ বা ১৮ এরও বেশি'),

  //AZ_S14_FLUENCY
  AZ_S14_FLUENCY_OPT_1('AZ_S14_FLUENCY', '0-4', '০-৪'),
  AZ_S14_FLUENCY_OPT_2('AZ_S14_FLUENCY', '5-6', '৫-৬'),
  AZ_S14_FLUENCY_OPT_3('AZ_S14_FLUENCY', '7-8', '৭-৮'),
  AZ_S14_FLUENCY_OPT_4('AZ_S14_FLUENCY', '9-10', '৯-১০'),
  AZ_S14_FLUENCY_OPT_5('AZ_S14_FLUENCY', '11-13', '১১-১৩'),
  AZ_S14_FLUENCY_OPT_6('AZ_S14_FLUENCY', '14-16', '১৪-১৬'),
  AZ_S14_FLUENCY_OPT_7('AZ_S14_FLUENCY', '17-21', '১৭-২১'),
  AZ_S14_FLUENCY_OPT_8('AZ_S14_FLUENCY', '22 or more than 22', '২২ বা ২২ এরও বেশি'),

  //AZ_S16_VISUOSPATIAL_1
  AZ_S16_VISUOSPATIAL_1_OPT_1('AZ_S16_VISUOSPATIAL_1', '5', '৫'),
  AZ_S16_VISUOSPATIAL_1_OPT_2('AZ_S16_VISUOSPATIAL_1', '7', '৭'),
  AZ_S16_VISUOSPATIAL_1_OPT_3('AZ_S16_VISUOSPATIAL_1', '9', '৯'),

  //AZ_S16_VISUOSPATIAL_2
  AZ_S16_VISUOSPATIAL_2_OPT_1('AZ_S16_VISUOSPATIAL_2', '7', '৭'),
  AZ_S16_VISUOSPATIAL_2_OPT_2('AZ_S16_VISUOSPATIAL_2', '6', '৬'),
  AZ_S16_VISUOSPATIAL_2_OPT_3('AZ_S16_VISUOSPATIAL_2', '9', '৯'),

  //AZ_S16_VISUOSPATIAL_3
  AZ_S16_VISUOSPATIAL_3_OPT_1('AZ_S16_VISUOSPATIAL_3', '7', '৭'),
  AZ_S16_VISUOSPATIAL_3_OPT_2('AZ_S16_VISUOSPATIAL_3', '6', '৬'),
  AZ_S16_VISUOSPATIAL_3_OPT_3('AZ_S16_VISUOSPATIAL_3', '9', '৯'),

  //AZ_S16_VISUOSPATIAL_4
  AZ_S16_VISUOSPATIAL_4_OPT_1('AZ_S16_VISUOSPATIAL_4', '8', '৮'),
  AZ_S16_VISUOSPATIAL_4_OPT_2('AZ_S16_VISUOSPATIAL_4', '9', '৯'),
  AZ_S16_VISUOSPATIAL_4_OPT_3('AZ_S16_VISUOSPATIAL_4', '10', '১০'),

  //AZ_S16_VISUOSPATIAL_5
  AZ_S16_VISUOSPATIAL_5_OPT_1('AZ_S16_VISUOSPATIAL_5', '8', '৮'),
  AZ_S16_VISUOSPATIAL_5_OPT_2('AZ_S16_VISUOSPATIAL_5', '9', '৯'),
  AZ_S16_VISUOSPATIAL_5_OPT_3('AZ_S16_VISUOSPATIAL_5', '12', '১২'),

  //AZ_S16_VISUOSPATIAL_6
  AZ_S16_VISUOSPATIAL_6_OPT_1('AZ_S16_VISUOSPATIAL_6', '10', '১০'),
  AZ_S16_VISUOSPATIAL_6_OPT_2('AZ_S16_VISUOSPATIAL_6', '11', '১১'),
  AZ_S16_VISUOSPATIAL_6_OPT_3('AZ_S16_VISUOSPATIAL_6', '15', '১৫'),

  //AZ_S16_VISUOSPATIAL_7
  AZ_S16_VISUOSPATIAL_7_OPT_1('AZ_S16_VISUOSPATIAL_7', '10', '১০'),
  AZ_S16_VISUOSPATIAL_7_OPT_2('AZ_S16_VISUOSPATIAL_7', '11', '১১'),
  AZ_S16_VISUOSPATIAL_7_OPT_3('AZ_S16_VISUOSPATIAL_7', '14', '১৪'),

  //AZ_S16_VISUOSPATIAL_8
  AZ_S16_VISUOSPATIAL_8_OPT_1('AZ_S16_VISUOSPATIAL_8', '11', '১১'),
  AZ_S16_VISUOSPATIAL_8_OPT_2('AZ_S16_VISUOSPATIAL_8', '12', '১২'),
  AZ_S16_VISUOSPATIAL_8_OPT_3('AZ_S16_VISUOSPATIAL_8', '13', '১৩'),

  //AZ_S16_VISUOSPATIAL_9
  AZ_S16_VISUOSPATIAL_9_OPT_1('AZ_S16_VISUOSPATIAL_9', '12', '১২'),
  AZ_S16_VISUOSPATIAL_9_OPT_2('AZ_S16_VISUOSPATIAL_9', '13', '১৩'),
  AZ_S16_VISUOSPATIAL_9_OPT_3('AZ_S16_VISUOSPATIAL_9', '14', '১৪'),

  //AZ_S16_VISUOSPATIAL_10
  AZ_S16_VISUOSPATIAL_10_OPT_1('AZ_S16_VISUOSPATIAL_10', '12', '১২'),
  AZ_S16_VISUOSPATIAL_10_OPT_2('AZ_S16_VISUOSPATIAL_10', '14', '১৪'),
  AZ_S16_VISUOSPATIAL_10_OPT_3('AZ_S16_VISUOSPATIAL_10', '15', '১৫'),
  
  //AZ_S16_VISUOSPATIAL_11
  AZ_S16_VISUOSPATIAL_11_OPT_1('AZ_S16_VISUOSPATIAL_11', '12', '১২'),
  AZ_S16_VISUOSPATIAL_11_OPT_2('AZ_S16_VISUOSPATIAL_11', '14', '১৪'),
  AZ_S16_VISUOSPATIAL_11_OPT_3('AZ_S16_VISUOSPATIAL_11', '15', '১৫');


  final String key;
  final String label;
  final String labelBn;
  const OptionEnum(this.key, this.label, this.labelBn);

  static String getLabelByName(String? name) {
    for (OptionEnum e in OptionEnum.values) {
      if (e.name == name) {
        if(isLocaleBn()) {
          return e.labelBn;
        } else {
          return e.label;
        }
      }
    }
    return 'N/A';
  }

  static List<String> getLabelArrayByNames(List<String>? names) {
    if(names == null) return [];
    List<String> optionList = [];
    for (String element in names) {
      for (OptionEnum e in OptionEnum.values) {
        if (e.name == element) {
          if(isLocaleBn()) {
            optionList.add(e.labelBn);
          } else {
            optionList.add(e.label);
          }
        }
      }
    }
    return optionList;
  }

  static List<String> getLabelArrayByKey(String? key) {
    if(key == null) return [];
    List<String> optionList = [];
    for (OptionEnum e in OptionEnum.values) {
      if (e.key == key) {
        if(isLocaleBn()) {
          optionList.add(e.labelBn);
        } else {
          optionList.add(e.label);
        }
      }
    }
    return optionList;
  }

  static List<String> getCorrectIncorrectArray() {
    List<String> optionList = [];
    if(isLocaleBn()) {
      optionList = [OptionConstant.CORRECT_BN, OptionConstant.INCORRECT_BN];
    } else {
      optionList = [OptionConstant.CORRECT, OptionConstant.INCORRECT];
    }
    return optionList;
  }

  static List<String> getYesNoArray() {
    List<String> optionList = [];
    if(isLocaleBn()) {
      optionList = [OptionConstant.YES_BN, OptionConstant.NO_BN];
    } else {
      optionList = [OptionConstant.YES, OptionConstant.NO];
    }
    return optionList;
  }


  static String getLabelYes() {
    if(isLocaleBn()) {
      return OptionConstant.YES_BN;
    } else {
      return OptionConstant.YES;
    }
  }
  static String getLabelNo() {
    if(isLocaleBn()) {
      return OptionConstant.NO_BN;
    } else {
      return OptionConstant.NO;
    }
  }


  static String getEnumNameBykeyAndLabel(String key, String label) {
    for (OptionEnum e in OptionEnum.values) {
      if (e.key == key) {
        if(e.label == label) {
          return e.name;
        } else if(e.labelBn == label) {
          return e.name;
        }
      }
    }
    return "";
  }

  static String? getMonthLabelByKeyAndNumber(String key, String number) {
    for (OptionEnum e in OptionEnum.values) {
      if (e.key == key) {
        if(e.name.split("_").last == number) {
          if(isLocaleBn()) {
            return e.labelBn;
          } else {
            return e.label;
          }

        }
      }
    }
    return null;
  }


  static String getMonthNumberByKeyAndLabel(String key, String label) {
    int i = 0;
    for (OptionEnum e in OptionEnum.values) {
      if (e.key == key) {
        i++;
        if(isLocaleBn() && e.labelBn == label) {
            return i.toString();
        } else if(e.label == label) {
            return i.toString();
        }
      }
    }
    return '';
  }

  static String getLabelEnByKeyAndLabel(String key, String label) {
    for (OptionEnum e in OptionEnum.values) {
      if (e.key == key) {
        if (e.labelBn == label || e.label == label) {
          return e.label;
        }
      }
    }
    return '';
  }

  static String getNameByKeyAndLabel(String key, String? label) {
    if(label == null) return '';
    for (OptionEnum e in OptionEnum.values) {
      if (e.key == key) {
        if (e.labelBn == label || e.label == label) {
          return e.name;
        }
      }
    }
    return '';
  }

  static String? getLabelByKeyAndText(String key, String text) {
    for (OptionEnum e in OptionEnum.values) {
      if (e.key == key) {
        if(e.label == text) {
          if(isLocaleBn()) {
            return e.labelBn;
          } else {
            return e.label;
          }
        }
      }
    }
    return null;
  }

  static String? getLabelByKeyAndName(String key, String? name) {
    if(name == null) {
      return null;
    }
    for (OptionEnum e in OptionEnum.values) {
      if (e.key == key) {
        if(e.name == name) {
          if(isLocaleBn()) {
            return e.labelBn;
          } else {
            return e.label;
          }
        }
      }
    }
    return null;
  }


  static List<String>? getLabelArrayByKeyAndNameArray(String key, List<String> nameArray) {
    List<String> labelArray = [];
    for (OptionEnum e in OptionEnum.values) {
      if (e.key == key) {
        if(nameArray.contains(e.name)) {
          if(isLocaleBn()) {
            labelArray.add(e.labelBn);
          } else {
            labelArray.add(e.label);
          }
        }
      }
    }
    return labelArray;
  }

  static String getDateTextByLabel(dynamic label) {
    if(label == OptionConstant.DONT_KNOW_BN || label == OptionConstant.DONT_KNOW) {
      return OptionConstant.DONT_KNOW;
    }
    if(label.runtimeType == String) {
      DateTime dateTime = DateFormat(DateConstant.dmy).parse(label);

      return DateFormat(DateConstant.dmy).format(dateTime).toString();
    } else {
      return DateFormat(DateConstant.dmy).format(label).toString();
    }
  }

  static List<String> getNameArrayByKeyAndLabels(String key, dynamic labels) {
    List<String> labelArray = getLabelArrayByLabels(labels);
    List<String> nameArray = [];
    for (OptionEnum e in OptionEnum.values) {
      if (e.key == key) {
        if(labelArray.contains(e.label) || labelArray.contains(e.labelBn)) {
          nameArray.add(e.name);
        }
      }
    }
    return nameArray;
  }

  static List<String> getLabelArrayByLabels(dynamic labels) {
    List<String> labelArray = [];
    if(labels.runtimeType == String) {
      var splitArray = labels.split(',');
      for(String s in splitArray) {
        labelArray.add(s.trim().replaceAll('"', '').replaceAll('[', '').replaceAll(']', ''));
      }
    } else {
      labelArray = labels;
    }

    return labelArray;
  }



  static bool getBooleanByLabel(String? label) {
    return (label == OptionConstant.YES_BN || label == OptionConstant.CORRECT_BN || label == OptionConstant.YES || label == OptionConstant.CORRECT || label == OptionConstant.ONE);
  }

  static String getBooleanTextByLabel(String? label) {
    if(label == null) {
      return '';
    }
    return getBooleanByLabel(label) ? OptionConstant.ONE : OptionConstant.ZERO;
  }

  static String? getYesNoLabelByBooleanText(String? text) {
    if(text == null) return null;
    if(isLocaleBn()) {
      if(text == OptionConstant.ONE) {
        return OptionConstant.YES_BN;
      }
      else {
        return OptionConstant.NO_BN;
      }
    } else {
      if(text == OptionConstant.ONE) {
        return OptionConstant.YES;
      }
      else {
        return OptionConstant.NO;
      }
    }
  }

  static String? getCorrectIncorrectLabelByBooleanText(String? text) {
    if(text == null) return null;
    if(isLocaleBn()) {
      if(text == OptionConstant.ONE) {
        return OptionConstant.CORRECT_BN;
      }
      else {
        return OptionConstant.INCORRECT_BN;
      }
    } else {
      if(text == OptionConstant.ONE) {
        return OptionConstant.CORRECT;
      }
      else {
        return OptionConstant.INCORRECT;
      }
    }
  }

  static String getImageUrlFromLabel(String label) {
    String text = '';
    for (OptionEnum e in OptionEnum.values) {
      if(e.label == label) {
        text = e.label;
        break;
      }
      if(e.labelBn == label) {
        text = e.label;
        break;
      }
    }
    if(text == OptionConstant.DONT_KNOW)  return '';
    return getImageUrlFromText(text);
  }

  static String getDontKnowByLocale() {
    if(isLocaleBn()) {
      return OptionConstant.DONT_KNOW_BN;
    }
    return OptionConstant.DONT_KNOW;
  }

  static String getImageUrlFromText(String figure) {
    figure = figure.replaceAll('/','_').replaceAll(' ','_');
    figure = 'assets/images/alzheimer/${figure.toLowerCase()}.png';
    return figure;
  }
}