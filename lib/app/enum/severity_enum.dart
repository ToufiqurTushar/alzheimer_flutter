import '../../core/utils/methods.dart';

enum SeverityEnum {
  HIGH_RISK('High Risk', 'উচ্চ ঝুঁকি'),
  LOW_RISK('Low Risk', 'কম ঝুঁকি'),
  MODERATE_RISK('Moderate Risk', 'মাঝারি ঝুঁকি'),
  NORMAL('Normal', 'স্বাভাবিক');

  final String label;
  final String labelBn;
  const SeverityEnum(this.label, this.labelBn);

  static String getLocaleLabelByLabel(String? label) {
    for (SeverityEnum e in SeverityEnum.values) {
      if (e.label == label) {
        if(isLocaleBn()) {
          return e.labelBn;
        } else {
          return e.label;
        }
      }
    }
    return 'N/A';
  }

}