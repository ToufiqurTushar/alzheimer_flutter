import 'package:alzheimer_flutter/app/constants/constant.dart';
import 'package:alzheimer_flutter/app/widgets/photo_zoom_overlay.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_image_picker/form_builder_image_picker.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:pinput/pinput.dart';

import '../../core/dto/input_data.dart';
import '../../core/utils/methods.dart';
import '../enum/option_enum.dart';
import 'common_widget.dart';

Widget InputLabel(InputData input) {
  return Column(
    crossAxisAlignment: input.labelAtCenter ? CrossAxisAlignment.center : CrossAxisAlignment.start,
    children: [
      RichText(
        text: TextSpan(
          text: input.label!,
          style:  TextStyle(fontWeight: FontWeight.bold, fontSize: input.labelAtCenter ? 20 : 17, color: input.labelAtCenter ? Colors.purple : Colors.black),
          children: <TextSpan>[
            if(input.isRequired) const TextSpan(text: '', style: TextStyle(color: Colors.red),),
          ],
        ),
      ),
      if(input.figure != null) Center(child: SimpleButton(input.figure!, padding: const EdgeInsets.all(20), style: const TextStyle(color: Colors.pink, fontSize: 42))),
    ],
  ) ;
}

InputDate(InputData input, form) {
  // if(input.value != null && !input.value.contains("-")) {//only date
  //   input.value = null;
  // }
  return ListBody(
      children: [
        if(input.label != null) InputLabel(input),
        FormBuilderField<String>(
            name: input.key,
            initialValue: input.value,
            builder: (FormFieldState field) {
              return Container(
                margin: const EdgeInsets.only(top: 5, bottom: 15),
                padding: const EdgeInsets.all(8.0),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  color: Colors.white,
                  border: Border.all(
                      color: Colors.red
                  ),
                ),
                child: InkWell(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(field.value != null ?  trNmuber(field.value) : input.hint, style: const TextStyle(fontSize: 16),),
                            const Icon(Icons.calendar_today, color: Colors.grey,)
                          ],
                        ),
                        if(field.errorText != null) Text(field.errorText!, style: const TextStyle(color: Colors.red, fontSize: 12),),
                      ],
                    ),
                  ),
                  onTap: () {
                    showDialog(
                      context: Get.context!,
                      builder: (BuildContext context) {
                        return AlertDialog(
                          content: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              ListTile(
                                title: Text(OptionEnum.getDontKnowByLocale()),
                                onTap: () {
                                  form.currentState!.fields[input.key]?.didChange(OptionEnum.getDontKnowByLocale());
                                  Get.back();
                                },
                              ),
                              ListTile(
                                title: Text(input.hint),
                                onTap: () async {
                                  Get.back();
                                  DateTime? picked = await showDatePicker(
                                    context: Get.context!,
                                    locale: Get.locale,
                                    initialDate: DateTime.now(),
                                    firstDate: DateTime(1850),
                                    lastDate: DateTime(DateTime.now().year+50),
                                    // builder: (BuildContext context, Widget? child) {
                                    //   return Theme(
                                    //     data: ThemeData.light(),
                                    //     child: child!,
                                    //   );
                                    // }
                                  );
                                  if (picked != null) {
                                    form.currentState!.fields[input.key]?.didChange(
                                        DateFormat(DateConstant.dmy).format(picked).toString()
                                    );
                                  }
                                },
                              )
                            ],
                          ),
                        );
                      }
                    );
                  },
                ),
                // child: FormBuilderDateTimePicker(
                //   initialValue: input.value != null ? DateFormat(DateConstant.dmy).parse(input.value) : null,
                //   inputType: InputType.date,
                //   name: input.key,
                //   //locale: const Locale('bn', 'BD'),
                //   firstDate: DateTime(1850),
                //   lastDate: DateTime(DateTime.now().year+50),
                //   format: DateFormat(DateConstant.dmy),
                //   decoration: InputDecoration(
                //     border: InputBorder.none,
                //     hintText: /*input.value != null ? input.value.trNumber : */input.hint,
                //     suffixIcon: Icon(Icons.calendar_today),
                //   ),
                //   valueTransformer: (val)=> DateFormat(DateConstant.dmy).format(val!).toString(),
                //   autovalidateMode: AutovalidateMode.onUserInteraction,
                //   validator: (input.isRequired) ? FormBuilderValidators.compose([
                //     FormBuilderValidators.required(errorText: 'az_required'.tr),
                //   ]) : null
                // ),
              );
            },
            autovalidateMode: AutovalidateMode.onUserInteraction,
            validator: (input.isRequired) ? FormBuilderValidators.compose([
              FormBuilderValidators.required(errorText: 'az_required'.tr),
            ]) : null
        )
      ]
  );
}

InputYear(InputData input, form) {
  String selectedYearText = input.value ?? input.hint;

  return ListBody(
      children: [
        if(input.label != null) InputLabel(input),
        FormBuilderField<String>(
            name: input.key,
            initialValue: input.value,
            builder: (FormFieldState field) {
              return ListBody(
                children: [
                  InkWell(
                    child: Container(
                      margin: const EdgeInsets.only(top: 5, bottom: 15),
                      padding: const EdgeInsets.fromLTRB(8, 20, 0, 20),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5),
                        color: Colors.white,
                        border: Border.all(
                            color: Colors.red
                        ),
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(trNmuber(field.value??selectedYearText), style: TextStyle(color: selectedYearText == input.hint ? Colors.black54 : Colors.black, fontSize: 16),),
                          if(field.errorText != null && input.options!.isNotEmpty) Text(field.errorText!, style: const TextStyle(color: Colors.red, fontSize: 12),),
                        ],
                      ),
                    ),
                    onTap: () async {
                      showDialog(
                        context: Get.context!,
                        builder: (BuildContext context) {
                          return AlertDialog(
                            content: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                ListTile(
                                  title: Text(OptionEnum.getDontKnowByLocale()),
                                  onTap: () {
                                    form.currentState!.fields[input.key]?.didChange(OptionEnum.getDontKnowByLocale());
                                    Get.back();
                                  },
                                ),
                                ListTile(
                                  title: Text(input.hint),
                                  onTap: () async {
                                    Get.back();
                                    showDialog(
                                      context: Get.context!,
                                      builder: (BuildContext context) {
                                        var firstYear = 1800;
                                        var lastYear = DateTime.now().year + 100;
                                        var selectedYear = field.value != null ? int.tryParse(field.value)??DateTime.now().year: DateTime.now().year;
                                        var offset = ((selectedYear-firstYear)/3)*100-100;
                                        final ScrollController scrollController = ScrollController();
                                        WidgetsBinding.instance.addPostFrameCallback((_) {
                                          scrollController.animateTo(
                                            offset,
                                            duration: const Duration(microseconds: 100),
                                            curve: Curves.easeInOut,
                                          );
                                        });
                                        return AlertDialog(
                                          title: Text(input.hint),
                                          content: Container(
                                            width: Get.size.width-20,
                                            height: Get.size.height-300,
                                            child: GridView.builder(
                                              controller: scrollController,
                                              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                                                  crossAxisCount: 3,
                                                  childAspectRatio: 3 / 4
                                              ),
                                              itemBuilder: (context, index) {
                                                var year = (index+firstYear).toString();
                                                return InkWell(
                                                  child: Container(
                                                    decoration: BoxDecoration(
                                                      borderRadius: BorderRadius.circular(4),
                                                      color: year == field.value ? Colors.pink : Colors.white,
                                                    ),
                                                    height: 200, // set the height of each item
                                                    child: Center(
                                                      child: Text(trNmuber(year), style: TextStyle(color: year == field.value ? Colors.white : year == DateTime.now().year.toString() ? Colors.pink : Colors.black, fontSize: 16, fontWeight: FontWeight.bold),),
                                                    ),
                                                  ),
                                                  onTap: (){
                                                    selectedYearText = year;
                                                    form.currentState!.fields[input.key]?.didChange(selectedYearText);
                                                    Navigator.pop(context);
                                                  },
                                                );
                                              },
                                              itemCount: lastYear-firstYear,
                                            ),
                                            // child: GridView.count(
                                            //   controller: scrollController,
                                            //   crossAxisCount: 3,
                                            //   childAspectRatio: 3/4,
                                            // /*  crossAxisSpacing: 10.0,
                                            //   mainAxisSpacing: 10.0,*/
                                            //   shrinkWrap: true,
                                            //   children: List.generate(lastYear-firstYear, (index) {
                                            //     var year = (index+firstYear).toString();
                                            //     return InkWell(
                                            //       child: Center(
                                            //         child: Container(
                                            //             decoration: BoxDecoration(
                                            //               borderRadius: BorderRadius.circular(4),
                                            //               color: year == field.value ? Colors.pink : Colors.white,
                                            //             ),
                                            //             child: Padding(
                                            //               padding: const EdgeInsets.all(0.0),
                                            //               child: Text(trNmuber(year), style: TextStyle(color: year == field.value ? Colors.white : Colors.black, fontSize: 16, fontWeight: FontWeight.bold),),
                                            //             )
                                            //         ),
                                            //       ),
                                            //       onTap: (){
                                            //         selectedYearText = year;
                                            //         form.currentState!.fields[input.key]?.didChange(selectedYearText);
                                            //         Navigator.pop(context);
                                            //       },
                                            //     );
                                            //   }),
                                            // ),

                                            // child: YearPicker(
                                            //   firstDate: DateTime(1800, 1),
                                            //   lastDate: DateTime(DateTime.now().year + 100, 1),
                                            //   initialDate: DateTime.now(),
                                            //   selectedDate: selectedDateTime ?? DateTime.now(),
                                            //   onChanged: (DateTime dateTime) {
                                            //     selectedDateTime = dateTime;
                                            //     selectedDateTimeText = selectedDateTime!.year.toString();
                                            //     form.currentState!.fields[input.key]?.didChange(selectedDateTimeText);
                                            //     Navigator.pop(context);
                                            //   },
                                            // ),
                                          ),
                                        );
                                      },
                                    );
                                  },
                                )
                              ],
                            ),
                          );
                        }
                      );
                    },
                  ),
                ],
              );
            },
            autovalidateMode: AutovalidateMode.onUserInteraction,
            validator: (input.isRequired) ? FormBuilderValidators.compose([
              FormBuilderValidators.required(errorText: 'az_required'.tr),
            ]) : null
        )
      ]
  );
}

InputSelect(InputData input) {
  return ListBody(
      children: [
        if(input.label != null) InputLabel(input),
        Container(
          margin: const EdgeInsets.only(top: 5, bottom: 15),
          padding: const EdgeInsets.fromLTRB(8, 8, 12, 8),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5),
            color: Colors.white,
            border: Border.all(
                color: Colors.red
            ),
          ),
          child: FormBuilderDropdown<String>(
              initialValue: input.value,
              name: input.key,
              decoration: InputDecoration(
                hintText:  input.hint,
                border: InputBorder.none,
              ),
              items: input.options!.map((eachItem) => DropdownMenuItem<String>(
                alignment: AlignmentDirectional.centerStart,
                value: eachItem,
                child: Text(eachItem),
              )).toList(),
              onChanged: (val) {},
              valueTransformer: (val) => val?.toString(),
              autovalidateMode: AutovalidateMode.onUserInteraction,
              validator: (input.isRequired) ? FormBuilderValidators.compose([
                FormBuilderValidators.required(errorText: 'az_required'.tr),
              ]) : null
          ),
        ),
      ]
  );
}

InputMultipleSelect(InputData input) {
  List<FormBuilderChipOption<String>> optionWidgets = [];
  for (var value in input.options!) {
    input.isReadOnly ?
    optionWidgets.add(
        FormBuilderChipOption(
          value: value.toString(),
          child: Text(value),
        )
    ) :
    optionWidgets.add(
        FormBuilderChipOption(
          value: value.toString(),
          avatar: const CircleAvatar(backgroundColor: Colors.white,child: Icon(Icons.check, color:  Colors.green),),
        )
    );
  }
  return ListBody(
      children: [
        if(input.label != null) InputLabel(input),
        Container(
          margin: const EdgeInsets.only(top: 5, bottom: 15),
          padding: const EdgeInsets.fromLTRB(8, 8, 12, 8),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5),
            color: Colors.white,
            border: Border.all(
                color: Colors.red
            ),
          ),
          child: FormBuilderFilterChip<String>(
              initialValue: input.value,
              name: input.key,
              decoration: InputDecoration(
                hintText:  input.hint,
                border: InputBorder.none,
              ),
              showCheckmark: false,
              avatarBorder: const CircleBorder( side: BorderSide(
                  color: Colors.blue
              )),
              labelStyle: const TextStyle(fontSize: 19, color: Colors.black),
              shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(5))),
              selectedColor: Colors.pink,
              alignment: WrapAlignment.spaceBetween,
              crossAxisAlignment: WrapCrossAlignment.center,
              options: optionWidgets,
              onChanged: (val) {},
              valueTransformer: (val) => val?.toString(),
              autovalidateMode: AutovalidateMode.onUserInteraction,
              validator: (input.isRequired) ? FormBuilderValidators.compose([
                FormBuilderValidators.required(errorText: 'az_required'.tr),
              ]) : null
          ),
        ),
      ]
  );
}

InputSingleSelect(InputData input) {
  List<FormBuilderChipOption<String>> optionWidgets = [];
  for (var value in input.options!) {
    optionWidgets.add(
        FormBuilderChipOption(
          value: value.toString(),
          avatar: const CircleAvatar(backgroundColor: Colors.white,child: Icon(Icons.adjust_rounded, color:  Colors.green),),
        )
    );
  }
  return ListBody(
      children: [
        if(input.label != null) InputLabel(input),
        Container(
          margin: const EdgeInsets.only(top: 5, bottom: 15),
          padding: const EdgeInsets.fromLTRB(8, 8, 12, 8),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5),
            color: Colors.white,
            border: Border.all(
                color: Colors.red
            ),
          ),
          child: FormBuilderChoiceChip<String>(
              initialValue: input.value,
              name: input.key,
              decoration: InputDecoration(
                hintText:  input.hint,
                border: InputBorder.none,
              ),
              avatarBorder: const CircleBorder( side: BorderSide(
                  color: Colors.blue
              )),
              labelStyle: const TextStyle(fontSize: 19, color: Colors.black),
              shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(5))),
              selectedColor: Colors.pink,
              alignment: WrapAlignment.spaceBetween,
              crossAxisAlignment: WrapCrossAlignment.center,
              options: optionWidgets,
              onChanged: (val) {},
              valueTransformer: (val) => val?.toString(),
              autovalidateMode: AutovalidateMode.onUserInteraction,
              validator: (input.isRequired) ? FormBuilderValidators.compose([
                FormBuilderValidators.required(errorText: 'az_required'.tr),
              ]) : null
          ),
        ),
      ]
  );
}

InputSelectSearch(InputData input, form) {
  return ListBody(
      children: [
        if(input.label != null) InputLabel(input),
        FormBuilderField<String>(
            name: input.key,
            initialValue: input.value,
            builder: (FormFieldState field) {
              return ListBody(
                children: [
                  Container(
                    margin: const EdgeInsets.only(top: 5, bottom: 15),
                    padding: const EdgeInsets.fromLTRB(8, 8, 0, 8),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      color: Colors.white,
                      border: Border.all(
                          color: Colors.red
                      ),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        DropdownSearch<dynamic>(
                          dropdownDecoratorProps: DropDownDecoratorProps(
                            dropdownSearchDecoration: InputDecoration(hintText: input.hint, border: InputBorder.none),
                          ),
                          popupProps: (input.type == InputData.selectSearch) ? const PopupProps.dialog(showSearchBox: true) : const PopupProps.menu(),
                          items: input.options!,
                          itemAsString: (dynamic item) => item.toString(),
                          selectedItem: field.value,
                          dropdownBuilder: (BuildContext context, dynamic item) => field.value == null ? Text(input.hint, style: const TextStyle(color: Colors.black54, fontSize: 16)) : Text(item.toString(),style: const TextStyle(fontSize: 16)) ,
                          onChanged: (dynamic data) =>  form.currentState!.fields[input.key]?.didChange(data.toString()),//_onSelectedReligion(value),
                        ),
                        if(field.errorText != null && input.options!.isNotEmpty) Text(field.errorText!, style: const TextStyle(color: Colors.red, fontSize: 12),),
                      ],
                    ),
                  ),
                ],
              );
            },
            autovalidateMode: AutovalidateMode.onUserInteraction,
            validator: (input.isRequired) ? FormBuilderValidators.compose([
              FormBuilderValidators.required(errorText: 'az_required'.tr),
            ]) : null
        )
      ]
  );
}

InputSelectImage(InputData input, {bool parentCondition = true}) {
  return ListBody(
      children: [
        if(input.label != null && parentCondition) InputLabel(input),
        Container(
            margin: const EdgeInsets.only(top: 5, bottom: 15),
            padding: const EdgeInsets.fromLTRB(8, 8, 12, 8),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5),
              color: Colors.white,
              border: Border.all(
                  color: Colors.red
              ),
            ),
            child: FormBuilderRadioGroup<String>(
              enabled: parentCondition,
              initialValue: input.value,
              wrapAlignment: WrapAlignment.spaceBetween,
              decoration: InputDecoration(
                hintText:  input.hint,
                border: InputBorder.none,
              ),
              name: input.key,
              options: input.options!.map((eachItem) => FormBuilderFieldOption(
                value: eachItem.toString(),
                child: Column(children: [
                  Card(
                    //color: Colors.pink[800],
                    child: Container(
                        width: Get.width/5,
                        height: Get.width/6,
                        child: OptionEnum.getImageUrlFromLabel(eachItem).isNotEmpty ?
                        InkWell(
                          child: Image.asset(OptionEnum.getImageUrlFromLabel(eachItem),),
                          onTap:()=> Navigator.of(Get.overlayContext!).push(PhotoZoomOverlay(assetImageUrl: OptionEnum.getImageUrlFromLabel(eachItem))),
                        ) :
                        Center(child: Text(eachItem),)
                    ),
                  )
                ]),
              )).toList(growable: false),
              valueTransformer: (val) => val?.toString(),
              onChanged: (val) {},
              controlAffinity: ControlAffinity.trailing,
              validator: parentCondition == true ? FormBuilderValidators.compose([FormBuilderValidators.required(errorText: 'az_select'.tr)]) : FormBuilderValidators.compose([]),
            )
        ),
      ]
  );
}


InputTrueFalse(InputData input) {
  return ListBody(
      children: [
        if(input.label != null) InputLabel(input),
        Container(
          margin: const EdgeInsets.only(top: 5, bottom: 15),
          padding: const EdgeInsets.fromLTRB(8, 8, 12, 8),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5),
            color: Colors.white,
            border: Border.all(
                color: Colors.red
            ),
          ),
          child: FormBuilderChoiceChip<String>(
              initialValue: input.value,
              decoration: InputDecoration(
                hintText:  input.hint,
                border: InputBorder.none,
              ),
              alignment: WrapAlignment.spaceEvenly,
              name: input.key,
              selectedColor: Colors.pinkAccent,
              selectedShadowColor:Colors.green,
              shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(5))),
              options: [
                FormBuilderChipOption(
                  value: input.options![0],
                  avatar: const CircleAvatar(backgroundColor: Colors.white,child: Icon(Icons.check, color: Colors.green,),),
                  child: Text(input.options![0], style: const TextStyle(fontSize: 18),),
                ),
                FormBuilderChipOption(
                  value: input.options![1],
                  avatar: const CircleAvatar(backgroundColor: Colors.white,child: Icon(Icons.close, color: Colors.red,),),
                  child: Text(input.options![1], style: const TextStyle(fontSize: 18),),
                ),
              ],

              autovalidateMode: AutovalidateMode.onUserInteraction,
              validator: (input.isRequired) ? FormBuilderValidators.compose([
                FormBuilderValidators.required(errorText: 'az_select'.tr),
              ]) : null
          ),
        ),
      ]
  );
}

InputTrueFalseWithFigure(InputData input) {
  return ListBody(
      children: [
        Card(
          //color: Colors.pink[800],
          child: Container(
              child: Image.asset(OptionEnum.getImageUrlFromText(input.figure!),)
          ),
        ),
        InputTrueFalse(input),
      ]
  );

}

InputTrueFalseWithSelectImage(InputData input, InputData childInput, form) {
  return ListBody(
      children: [
        if(input.label != null) InputLabel(input),
        FormBuilderField<String>(
            name: '${input.key}_parent',
            initialValue: input.value,
            builder: (FormFieldState field) {
              return ListBody(
                children: [
                  Container(
                    margin: const EdgeInsets.only(top: 5, bottom: 15),
                    padding: const EdgeInsets.fromLTRB(8, 8, 8, 8),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      color: Colors.white,
                      border: Border.all(
                          color: Colors.red
                      ),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        FormBuilderChoiceChip<String>(
                            initialValue: field.value,
                            decoration: InputDecoration(
                              hintText:  input.hint,
                              border: InputBorder.none,
                            ),
                            alignment: WrapAlignment.spaceEvenly,
                            name: input.key,
                            selectedColor: Colors.pinkAccent,
                            selectedShadowColor:Colors.green,
                            shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(5))),
                            options: [
                              FormBuilderChipOption(
                                value: input.options![0],
                                avatar: const CircleAvatar(backgroundColor: Colors.white,child: Icon(Icons.check, color: Colors.green,),),
                                child: Text(input.options![0], style: const TextStyle(fontSize: 18),),
                              ),
                              FormBuilderChipOption(
                                value: input.options![1],
                                avatar: const CircleAvatar(backgroundColor: Colors.white,child: Icon(Icons.close, color: Colors.red,),),
                                child: Text(input.options![1], style: const TextStyle(fontSize: 18),),
                              ),
                            ],
                            onChanged: (val){
                              form.currentState.patchValue({
                                '${input.key}_parent': val,
                                childInput.key: null,
                              });
                            },
                            autovalidateMode: AutovalidateMode.onUserInteraction,
                            validator: (input.isRequired) ? FormBuilderValidators.compose([
                              FormBuilderValidators.required(errorText: 'az_select'.tr),
                            ]) : null
                        ),
                        if(OptionEnum.getBooleanByLabel(field.value) || childInput.isVisible)
                        InputSelectImage(childInput, parentCondition: OptionEnum.getBooleanByLabel(field.value)),
                      ],
                    ),
                  ),
                ],
              );
            }
        )
      ]
  );
}

InputTrueFalseWithSelectForTrueAndSelectForFalse(InputData input, InputData childInput, InputData secondChildInput, form) {
  return ListBody(
      children: [
        if(input.label != null) InputLabel(input),
        FormBuilderField<String>(
            name: '${input.key}_parent',
            initialValue: input.value,
            builder: (FormFieldState field) {
              return ListBody(
                children: [
                  Container(
                    margin: const EdgeInsets.only(top: 5, bottom: 15),
                    padding: const EdgeInsets.fromLTRB(8, 8, 8, 8),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      color: Colors.white,
                      border: Border.all(
                          color: Colors.red
                      ),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        FormBuilderChoiceChip<String>(
                            initialValue: field.value,
                            decoration: InputDecoration(
                              hintText:  input.hint,
                              border: InputBorder.none,
                            ),
                            alignment: WrapAlignment.spaceEvenly,
                            name: input.key,
                            selectedColor: Colors.pinkAccent,
                            selectedShadowColor:Colors.green,
                            shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(5))),
                            options: [
                              FormBuilderChipOption(
                                value: input.options![0],
                                avatar: const CircleAvatar(backgroundColor: Colors.white,child: Icon(Icons.check, color: Colors.green,),),
                                child: Text(input.options![0], style: const TextStyle(fontSize: 18),),
                              ),
                              FormBuilderChipOption(
                                value: input.options![1],
                                avatar: const CircleAvatar(backgroundColor: Colors.white,child: Icon(Icons.close, color: Colors.red,),),
                                child: Text(input.options![1], style: const TextStyle(fontSize: 18),),
                              ),
                            ],
                            onChanged: (val){
                              form.currentState.patchValue({
                                '${input.key}_parent': val,
                                childInput.key: null,
                                secondChildInput.key: null,
                              });
                            },
                            autovalidateMode: AutovalidateMode.onUserInteraction,
                            validator: (input.isRequired) ? FormBuilderValidators.compose([
                              FormBuilderValidators.required(errorText: 'az_select'.tr),
                            ]) : null
                        ),
                        if(field.value != null && (OptionEnum.getBooleanByLabel(field.value) || childInput.isVisible))
                          InputSingleSelect(childInput),

                        if(field.value != null && (!OptionEnum.getBooleanByLabel(field.value) || secondChildInput.isVisible))
                          InputSingleSelect(secondChildInput)
                      ],
                    ),
                  ),
                ],
              );
            }
        )
      ]
  );
}


InputOtp(InputData input, context, form) {
  final pinController = TextEditingController();
  return ListBody(
      children: [
        if(input.label != null) InputLabel(input),
        FormBuilderField<String>(
            name: input.key,
            initialValue: input.value,
            builder: (FormFieldState field) {
              if(field.value != null) {
                pinController.text = trNmuber(field.value);
              }
              return ListBody(
                children: [
                  Container(
                    margin: const EdgeInsets.only(top: 5, bottom: 15),
                    padding: const EdgeInsets.fromLTRB(8, 8, 8, 8),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      color: Colors.white,
                      border: Border.all(
                          color: Colors.red
                      ),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        // Pinput(
                        //   controller: pinController,
                        //   length: 5,
                        //   inputFormatters: [
                        //     FilteringTextInputFormatter.allow(RegExp(r'[0-9]'))
                        //   ],
                        //   defaultPinTheme: PinTheme(
                        //     width: 56,
                        //     height: 56,
                        //     textStyle: const TextStyle(fontSize: 25, color: Color.fromRGBO(30, 60, 87, 1), fontWeight: FontWeight.w600),
                        //     decoration: BoxDecoration(
                        //       border: Border.all(color: Colors.grey),
                        //       borderRadius: BorderRadius.circular(5),
                        //     ),
                        //   ),
                        //   onChanged: (pin) {
                        //     if(pin.length >= 5) {
                        //       form.currentState!.fields[input.key]?.didChange(pin.toString());
                        //     }
                        //   },
                        //   onCompleted: (pin) {},
                        // )
                        FormBuilderTextField(
                          name: input.key,
                          textAlign: TextAlign.center,
                          inputFormatters: [
                            FilteringTextInputFormatter.allow(RegExp(r'[0-9]'))
                          ],
                          maxLength: 5,
                          style: const TextStyle(
                              letterSpacing: 25, fontSize: 25, color: Color.fromRGBO(30, 60, 87, 1), fontWeight: FontWeight.w600
                          ),
                          keyboardType: TextInputType.number,
                          onChanged: (pin) {
                            if(pin != null && pin.length == 5) {
                              field.didChange(pin);
                            } else {
                              field.didChange(null);
                            }
                          },
                        ),
                        if(field.errorText != null) const SizedBox(height: 10,),
                        if(field.errorText != null) Text(field.errorText!, style: const TextStyle(color: Colors.red, fontSize: 12),),
                      ],
                    ),
                  ),
                ],
              );
            },
            valueTransformer: (val) => bngToEng(val.toString()),
            autovalidateMode: AutovalidateMode.onUserInteraction,
            validator: (input.isRequired) ? FormBuilderValidators.compose([
              FormBuilderValidators.required(errorText: 'az_required'.tr),
            ]) : null
        )
      ]
  );
}


InputRadioWithFigure(InputData input) {
  return ListBody(
      children: [
        Container(
          margin: const EdgeInsets.only(top: 5, bottom: 15),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5),
            color: Colors.white,
            border: Border.all(
                color: Colors.red
            ),
          ),
          child: Column(
            children: [
              if(input.figure != null) InkWell(
                onTap:()=> Navigator.of(Get.overlayContext!).push(PhotoZoomOverlay(assetImageUrl: OptionEnum.getImageUrlFromText(input.figure!))),
                child: Image.asset(OptionEnum.getImageUrlFromText(input.figure!), width: Get.width,)
              ),
              Padding(
                padding: const EdgeInsets.only(left: 8, bottom: 8),
                child: FormBuilderRadioGroup<String>(
                    initialValue: input.value,
                    decoration: InputDecoration(
                      hintText:  input.hint,
                      border: InputBorder.none,
                    ),
                    wrapAlignment: WrapAlignment.spaceEvenly,
                    name: input.key,
                    options: input.options!.map((element) => FormBuilderFieldOption(
                      value: element.toString(),
                      child: Text(element.toString()),
                    )).toList(growable: false),
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    validator: (input.isRequired) ? FormBuilderValidators.compose([
                      FormBuilderValidators.required(errorText: 'az_select'.tr),
                    ]) : null
                ),
              ),
            ],
          ),
        ),
      ]
  );
}

InputSelectWithFigure(InputData input, form) {
  return ListBody(
      children: [
        Container(
          margin: const EdgeInsets.only(top: 5, bottom: 15),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5),
            color: Colors.white,
            border: Border.all(
                color: Colors.red
            ),
          ),
          child: Column(
            children: [
              if(input.figure != null) InkWell(
                child: Image.asset(OptionEnum.getImageUrlFromText(input.figure!), height: 150,),
                onTap:()=> Navigator.of(Get.overlayContext!).push(PhotoZoomOverlay(assetImageUrl: OptionEnum.getImageUrlFromText(input.figure!))),
                // onTap: () {
                //   showDialog(
                //       context: Get.context!,
                //       builder: (BuildContext context) {
                //         return AlertDialog(
                //           scrollable: true,
                //           content: Column(
                //             mainAxisSize: MainAxisSize.min,
                //             children: input.options!.map((eachItem) => ListTile(
                //               title: Text(eachItem),
                //               onTap: () {
                //                 form.currentState!.fields[input.key]?.didChange(eachItem.toString());
                //                 Navigator.of(context).pop();
                //               },
                //             )).toList(),
                //           ),
                //         );
                //       }
                //   );
                // },
              ),
              Padding(
                padding: const EdgeInsets.only(left: 8, right: 8,  bottom: 8),
                child: FormBuilderDropdown<String>(
                    initialValue: input.value,
                    name: input.key,
                    decoration: InputDecoration(
                      hintText:  input.hint,
                      border: InputBorder.none,
                    ),
                    items: input.options!.map((eachItem) => DropdownMenuItem<String>(
                      alignment: AlignmentDirectional.centerStart,
                      value: eachItem,
                      child: Text(eachItem),
                    )).toList(),
                    onChanged: (val) {},
                    valueTransformer: (val) => val?.toString(),
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    validator: (input.isRequired) ? FormBuilderValidators.compose([
                      FormBuilderValidators.required(errorText: 'az_required'.tr),
                    ]) : null
                ),
              ),
            ],
          ),
        ),
      ]
  );
}

InputTrueFalseWithFigureAndTakePicture(InputData input, form) {
  return ListBody(
      children: [
        Column(mainAxisAlignment:MainAxisAlignment.center, children: [
          InkWell(
              onTap:()=> Navigator.of(Get.overlayContext!).push(PhotoZoomOverlay(assetImageUrl: OptionEnum.getImageUrlFromText(input.figure!))),
              child: Image.asset(OptionEnum.getImageUrlFromText(input.figure!),),
          ),
          Container(
            margin: const EdgeInsets.only(top:40, bottom: 10),
            padding: const EdgeInsets.only(left: 10, bottom: 5),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5),
              color: Colors.white,
              border: Border.all(
                  color: Colors.red
              ),
            ),
            child: FormBuilderImagePicker(
              previewWidth: Get.width-60,
              previewHeight: 80,
              name: '${input.key}_image',
              //initialValue: [input.value == null ? null : '${Urls.getBaseUrl()}${input.value!}'],
              decoration: const InputDecoration(labelText: 'Photo', border: InputBorder.none,),
              maxImages: 1,
              fit: BoxFit.contain,
              autovalidateMode: AutovalidateMode.onUserInteraction,
              validator: null,
              /*validator: (input.isRequired) ? FormBuilderValidators.compose([
                  FormBuilderValidators.required(errorText: 'az_required'.tr),
                ]) : null*/
            ),
          ),
        ],),
        InputTrueFalse(input),
      ]
  );
}
