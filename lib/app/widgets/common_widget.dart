import 'package:cached_network_image/cached_network_image.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:get/get.dart';
import 'package:responsive_grid/responsive_grid.dart';

import '../../core/dto/input_data.dart';

ResponsiveGridCol CardWithImageText({required String image, required String text, int? size, GestureTapCallback? onTap}) {
  return ResponsiveGridCol(
    xs: size??12,
    child: InkWell(
      onTap: onTap,
      child: Card(
        child: Container(
          padding: const EdgeInsets.only(top: 10, bottom: 10),
          alignment: const Alignment(0, 0),
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 40, right: 40, top: 20),
                child: Image.asset(image, height: 60,),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 0, bottom: 30),
                child: Text(text, style: const TextStyle(fontSize: 16, fontWeight: FontWeight.bold),),
              ),
            ],
          ),
        ),
      ),
    ),
  );
}

ResponsiveGridCol PageImage({required String image, EdgeInsetsGeometry? margin, double? height}) {
  return ResponsiveGridCol(
    xs: 12,
    child: Container(
      margin: margin??const EdgeInsets.fromLTRB(0, 10, 0, 10),
      child: Image.asset(image, height: height??150,),
    ),
  );
}

Card ProfileHeader({required BuildContext context, String? image, required String name, required String mobile}) {
  return Card(
    child: Padding(
      padding: const EdgeInsets.all(1),
      child: ListTile(
        leading: CachedNetworkImage(
          width: Get.width * 0.10,
          imageUrl: image??'',
          imageBuilder: (context, imageProvider) =>
              CircleAvatar(
                radius: Get.width * 0.05,
                backgroundImage: imageProvider,
              ),
          placeholder: (context, url) => const Center(child: CircularProgressIndicator(strokeWidth: 1,)),
          errorWidget: (context, url, error) => Image.asset('assets/images/alzheimer/avatar.png'),
        ),
        title: Text(name, style: const TextStyle(fontWeight: FontWeight.bold),),
        subtitle: Text(mobile),
      ),
    ),
  );
}

Card ScreeningListItem({required BuildContext context, Color? color, required String title, required String subtitle, required String date, required GestureTapCallback onTap}) {
  return Card(
    child: ListTile(
      leading: CircleAvatar(
        radius: Get.width * 0.07,
        backgroundColor: color??Colors.red,
        child: Padding(
          padding: const EdgeInsets.all(1), // Border radius
          child: ClipOval(child: Image.asset('assets/images/alzheimer/patient.png')),
        ),
      ),
      title: Text(title, style: TextStyle(fontWeight: FontWeight.bold, color: color??Colors.black),),
      subtitle: Text(subtitle),
      trailing: Text(date),
      onTap: onTap,
    ),
  );
}

Widget SimpleButton(String text, {VoidCallback? onPressed, EdgeInsetsGeometry? padding, TextStyle? style}) {
  return Padding(
    padding: const EdgeInsets.fromLTRB(10, 0, 10, 10),
    child: ElevatedButton(

      onPressed: onPressed,
      style: ElevatedButton.styleFrom(backgroundColor: const Color(0xFFc51c6b), shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10))),
      child: Padding(padding: padding??const EdgeInsets.symmetric(vertical: 0, horizontal: 0),
          child: Text(text, style: style??const TextStyle(color: Colors.white, fontSize: 18))
      ),
    ),
  );
}

Widget SimpleWideButton(String text, {VoidCallback? onPressed, EdgeInsetsGeometry? padding, TextStyle? style}) {
  return Padding(
    padding: const EdgeInsets.fromLTRB(10, 0, 10, 10),
    child: ElevatedButton(

      onPressed: onPressed,
      style: ElevatedButton.styleFrom(backgroundColor: const Color(0xFFc51c6b), minimumSize: const Size.fromHeight(40), shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10))),
      child: Padding(padding: padding??const EdgeInsets.symmetric(vertical: 0, horizontal: 0),
          child: Text(text, style: style??const TextStyle(color: Colors.white, fontSize: 18))
      ),
    ),
  );
}

ResponsiveGridCol ResponsiveButton({required String text, int? size, VoidCallback? onPressed}) {
  return ResponsiveGridCol(
      xs: size??12,
      child: SimpleButton(text, onPressed:onPressed)
  );
}

Widget FlexibleButton({required String text, int? flex, VoidCallback? onPressed}) {
  return Flexible(
      flex: flex??1,
      child: SimpleWideButton(text, onPressed:onPressed)
  );
}


ResponsiveGridCol NumbeDropdown({required String key, int? size, String? answer, required Function(String?) onChanged}) {
  var numberOptions = List<String>.generate(10, (index) => index.toString());

  return ResponsiveGridCol(
      xs: size??12,
      child: Card(
        child: FormBuilderDropdown<String>(
          name: key,
          initialValue: answer??'0',
          decoration: const InputDecoration(enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.red, width: 1),
          )),
          items: numberOptions.map((eachItem) => DropdownMenuItem(
            alignment: AlignmentDirectional.center,
            value: eachItem,
            child: Text(eachItem),
          )).toList(),
          onChanged: (val) => onChanged(val),
        ),
      )
  );
}

SnackBar() {
  Get.snackbar(
    "Error !",
    "Please, answer all the questions !",
    icon: const Icon(Icons.close, color: Colors.white),
    snackPosition: SnackPosition.BOTTOM,
    backgroundColor: Colors.red,
    borderRadius: 20,
    margin: EdgeInsets.all(15),
    colorText: Colors.white,
    duration: const Duration(seconds: 2),
    isDismissible: true,
    forwardAnimationCurve: Curves.easeOutBack,
  );
}

Container IndicatorContainer({required String text, Color? background, Color? textColor}) {
  return Container(
    decoration: BoxDecoration(
        borderRadius: const BorderRadius.all(Radius.circular(30)),
        color: background??Colors.pink,
        border: Border.all(color: Colors.pinkAccent)
    ),
    alignment: const Alignment(0,0),
    padding: const EdgeInsets.all(4),
    child: Text(text, style: TextStyle(color: textColor??Colors.white),),
  );
}

Padding QuestionPageTitle(String text) {
  return Padding(
    padding: const EdgeInsets.only(bottom: 20),
    child: Text(text, style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 20),),
  );
}

Text QuestionTextField(String text) {
  return Text(text, style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 17),);
}

Text QuestionSelect(String text) {
  return Text(text, style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 20),);
}

Widget QuestionPage(controller, item, index) {
  List<Widget> children = [];

  var pageTitle = item['page_title'];
  var contents = item['contents'];

  children.add(QuestionPageTitle(pageTitle));

  for(var i=0; i < contents.length; i++) {
    var type = contents[i]['type'];
    var options = contents[i]['options'];
    var key = contents[i]['key'];
    var label = contents[i]['label'];
    var hints = contents[i]['hints'];
    var answer = contents[i]['answer'];
    var secondAnswer = contents[i]['second_answer'];

    switch(type) {
      case 'az_select': {
        children.add(QuestionTextField(label));
        List<String> selectItems = [];
        options.forEach((e) => selectItems.add(e.toString()));

        children.add(
          Padding(
            padding: const EdgeInsets.only(top:5, bottom: 30),
            child: FormBuilderDropdown<String>(
              initialValue: answer,
              name: key,
              decoration: InputDecoration(hintText:  hints, enabledBorder: const OutlineInputBorder(
                borderSide: BorderSide(color: Colors.red, width: 1),
              )),
              items: selectItems.map((eachItem) => DropdownMenuItem(
                alignment: AlignmentDirectional.centerStart,
                value: eachItem,
                child: Text(eachItem),
              )).toList(),
              onChanged: (val) {
                item['contents'][i]['answer'] = val;
              },
              valueTransformer: (val) => val?.toString(),
              validator: FormBuilderValidators.compose([FormBuilderValidators.required()]),
            ),
          ),
        );
      }
      break;
      case 'input_select': {
        children.add(
            InputSelect1(InputData(key: 'type_id', label: 'aapointment_type'.tr, hint: 'az_select'.tr, value: null, options: [{"id":1, "name": "eng", "nameBn": "Bng"}], type:InputData.select, isRequired: true))
        );
      }
      break;
      case 'correct_incorrect': {
        if(label!=null) children.add(QuestionTextField(label));
        children.add(
          Padding(
            padding: const EdgeInsets.only(top:5, bottom: 30),
            child: FormBuilderChoiceChip<String>(
              initialValue: answer,
              decoration: const InputDecoration(enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.red, width: 1),
              )),
              alignment: WrapAlignment.spaceEvenly,
              autovalidateMode: AutovalidateMode.onUserInteraction,
              name: key,
              selectedColor: Colors.pinkAccent,
              options: [
                FormBuilderChipOption(
                  value: options[0],
                  avatar: const CircleAvatar(backgroundColor: Colors.white,child: Icon(Icons.check, color: Colors.green,),),
                  child: Text(options[0], style: const TextStyle(color:Colors.white)),
                ),
                FormBuilderChipOption(
                  value: options[1],
                  avatar: const CircleAvatar(backgroundColor: Colors.white,child: Icon(Icons.close, color: Colors.red,),),
                  child: Text(options[1], style: const TextStyle(color:Colors.white)),
                ),
              ],
              onChanged: (val) {
                item['contents'][i]['answer'] = val;
              },
              validator: FormBuilderValidators.compose([FormBuilderValidators.required(errorText: 'Please, select !')]),
            ),
          ),
        );
      }
      break;
      case 'conditional_correct_incorrect': {
        children.add(ConditionalQuestionAndImageSelection(item: item, onPressedYesNo: (val){item['contents'][i]['answer'] = val;}, onChangeSelections: (val){item['contents'][i]['second_answer'] = val;}));
      }
      break;
      case 'conditional_question_for_education': {
        children.add(ConditionalQuestionForEducation(item: item, onPressedYesNo: (val){item['contents'][i]['answer'] = val;}, onChangeSelection: (val){item['contents'][i]['second_answer'] = val;}));
      }
      break;
      case 'multiple_select_readonly': {
        List<FormBuilderChipOption<String>> optionWidgets = [];
        for(var j=0; j < options.length; j++) {
          optionWidgets.add(FormBuilderChipOption(
            value: options[j].toString(),
            avatar: const CircleAvatar(backgroundColor: Colors.white,child: Icon(Icons.adjust_rounded, color:  Color(0xffdcdcdc),),),
          ));
        }
        children.add(
          Padding(
            padding: const EdgeInsets.only(top:5, bottom: 30),
            child: IgnorePointer(
              child: FormBuilderFilterChip<String>(
                initialValue: answer,
                selectedColor: Colors.pink,
                labelPadding: const EdgeInsets.fromLTRB(10, 2, 10, 4),
                shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(5))),
                decoration: const InputDecoration(enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.red, width: 1),
                )),
                alignment: WrapAlignment.spaceBetween,
                name: key,
                labelStyle: const TextStyle(fontSize: 18, color: Colors.white),
                options:optionWidgets,
                onChanged: (val) {
                  item['contents'][i]['answer'] = val;
                },
              ),
            ),
          ),
        );
      }
      break;
      case 'multiple_select': {
        List<FormBuilderChipOption<String>> optionWidgets = [];
        for(var j=0; j < options.length; j++) {
          optionWidgets.add(FormBuilderChipOption(
            value: options[j].toString(),
            avatar: const CircleAvatar(backgroundColor: Colors.white,child: Icon(Icons.check, color:  Color(0xffdcdcdc),),),
          ));
        }
        children.add(
          Padding(
            padding: const EdgeInsets.only(top:5, bottom: 30),
            child: FormBuilderFilterChip<String>(
              initialValue: answer,
              selectedColor: Colors.pink,
              labelPadding: const EdgeInsets.fromLTRB(10, 2, 10, 4),
              shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(5))),
              decoration: const InputDecoration(enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.red, width: 1),
              )),
              alignment: WrapAlignment.spaceBetween,
              name: key,
              labelStyle: const TextStyle(fontSize: 18, color: Colors.white),
              options: optionWidgets,
              onChanged: (val) {
                item['contents'][i]['answer'] = val;
              },
              validator: FormBuilderValidators.compose([FormBuilderValidators.required(errorText: 'Please, select !')]),
            ),
          ),
        );
      }
      break;
      case 'single_select': {
        List<FormBuilderChipOption<String>> optionWidgets = [];
        for(var j=0; j < options.length; j++) {
          optionWidgets.add(FormBuilderChipOption(
            value: options[j].toString(),
            avatar: const CircleAvatar(backgroundColor: Colors.white,child: Icon(Icons.adjust_rounded, color:  Color(0xffdcdcdc),),),
          ));
        }
        children.add(
          Padding(
            padding: const EdgeInsets.only(top:5, bottom: 30),
            child: FormBuilderChoiceChip<String>(
              initialValue: answer,
              selectedColor: Colors.pink,
              labelPadding: const EdgeInsets.fromLTRB(10, 2, 10, 4),
              shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(5))),
              decoration: const InputDecoration(enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.red, width: 1),
              )),
              alignment: WrapAlignment.spaceBetween,
              name: key+'_select',
              labelStyle: const TextStyle(fontSize: 18, color: Colors.white),
              options:optionWidgets,
              onChanged: (val) {
                item['contents'][i]['answer'] = val;
              },
              validator: FormBuilderValidators.compose([FormBuilderValidators.required(errorText: 'Please, select !')]),
            ),
          ),
        );
      }
      break;
      case 'select_digit_readonly': {
        children.add(
          IgnorePointer(
            ignoring: true,
            child: Padding(
              padding: const EdgeInsets.only(top:5, bottom: 30),
              child: Row(
                  children: [
                    FlexibleButton(text: options[0].toString(), flex: 1, onPressed: (){}),
                    FlexibleButton(text: options[1].toString(), flex: 1, onPressed: (){}),
                    FlexibleButton(text: options[2].toString(), flex: 1, onPressed: (){}),
                    FlexibleButton(text: options[3].toString(), flex: 1, onPressed: (){}),
                    FlexibleButton(text: options[4].toString(), flex: 1, onPressed: (){}),
                  ]
              ),
            ),
          ),
        );
      }
      break;
      case 'select_digit': {
        children.add(QuestionTextField('Now please repeat them in forward'));
        children.add(
          Padding(
            padding: const EdgeInsets.only(top:5, bottom: 30),
            child:
            ResponsiveGridRow(
                children: [
                  NumbeDropdown(key: key+'_f0', size:3, answer: answer[0], onChanged:(val){
                    item['contents'][i]['answer'][0] = val;
                  }),
                  NumbeDropdown(key: key+'_f1',size:3, answer: answer[1], onChanged:(val){
                    item['contents'][i]['answer'][1] = val;
                  }),
                  NumbeDropdown(key: key+'_f2',size:3, answer: answer[2], onChanged:(val){
                    item['contents'][i]['answer'][2] = val;
                  }),
                  NumbeDropdown(key: key+'_f3',size:3, answer: answer[3], onChanged:(val){
                    item['contents'][i]['answer'][3] = val;
                  }),
                ]
            ),
          ),
        );
        children.add(QuestionTextField('And now in backward order'));
        children.add(
          Padding(
            padding: const EdgeInsets.only(top:5, bottom: 30),
            child:ResponsiveGridRow(
                children: [
                  NumbeDropdown(key: key+'_r0', size:3, answer: secondAnswer[0], onChanged:(val){
                    item['contents'][i]['second_answer'][0] = val;
                  }),
                  NumbeDropdown(key: key+'_r1',size:3, answer: secondAnswer[1], onChanged:(val){
                    item['contents'][i]['second_answer'][1] = val;
                  }),
                  NumbeDropdown(key: key+'_r2',size:3, answer: secondAnswer[2], onChanged:(val){
                    item['contents'][i]['second_answer'][2] = val;
                  }),
                  NumbeDropdown(key: key+'_r3',size:3, answer: secondAnswer[3], onChanged:(val){
                    item['contents'][i]['second_answer'][3] = val;
                  }),
                ]
            ),
          ),
        );
      }
      break;
      case 'select_with_image': {
        List<String> optionWidgets = [];
        options.forEach((e) => optionWidgets.add(e));

        children.add(
            Card(
              color: Colors.pink[800],
                child: Container(
                  alignment: const Alignment(0, 0),
                  child: Column(
                    children: [
                      Image.asset(label, height: 150,),
                      Container(
                        color: Colors.white,
                        child: FormBuilderDropdown<String>(
                          initialValue: answer,
                          name: key,
                          decoration: InputDecoration(hintText:  hints, enabledBorder: const OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.red, width: 1),
                          )),
                          items: optionWidgets.map((eachItem) => DropdownMenuItem(
                            alignment: AlignmentDirectional.center,
                            value: eachItem,
                            child: Text(eachItem),
                          )).toList(),
                          onChanged: (val) {
                            item['contents'][i]['answer'] = val;
                          },
                          validator: FormBuilderValidators.compose([FormBuilderValidators.required(errorText: 'Please, select !')]),
                        ),
                      ),
                    ],
                  ),
                )
            )
        );
      }
      break;
      case 'correct_incorrect_with_image': {

        List<String> optionWidgets = [];
        options.forEach((e) => optionWidgets.add(e));

        children.add(
          Card(
            color: Colors.pink[800],
            child: Container(
              alignment: const Alignment(0, 0),
              child: Column(
                children: [
                  Container(
                    color: Colors.white,
                    child: FormBuilderChoiceChip<String>(
                      initialValue: answer,
                      decoration: const InputDecoration(enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.red, width: 1),
                      )),
                      alignment: WrapAlignment.spaceEvenly,
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      name: key,
                      selectedColor: Colors.pinkAccent,
                      options: [
                        FormBuilderChipOption(
                          value: options[0],
                          avatar: const CircleAvatar(backgroundColor: Colors.white,child: Icon(Icons.check, color: Colors.green,),),
                          child: Text(options[0], style: const TextStyle(color: Colors.white))
                        ),
                        FormBuilderChipOption(
                          value: options[1],
                          avatar: const CircleAvatar(backgroundColor: Colors.white,child: Icon(Icons.close, color: Colors.red,),),
                          child: Text(options[1], style: const TextStyle(color: Colors.white))
                        ),
                      ],
                      onChanged: (val) {
                        item['contents'][i]['answer'] = val;
                      },
                      validator: FormBuilderValidators.compose([FormBuilderValidators.required(errorText: 'Please, select !')]),
                    ),
                  ),
                  Image.asset(label, width: Get.width,),
                ],
              ),
            )
          )
        );
      }
      break;
      case 'radio_with_image': {
        List<String> optionWidgets = [];
        options.forEach((e) => optionWidgets.add(e));

        children.add(
            Card(
                color: Colors.pink[800],
                child: Container(
                  alignment: const Alignment(0, 0),
                  child: Column(
                    children: [
                      Image.asset(label, width: Get.width,),
                      Container(
                        color: Colors.white,
                        child: FormBuilderRadioGroup<String>(
                          initialValue: answer,
                          wrapAlignment: WrapAlignment.spaceEvenly,
                          decoration: const InputDecoration(enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.red, width: 1),
                          )),
                          name: key,
                          onChanged: (val) {
                            item['contents'][i]['answer'] = val;
                          },
                          options: optionWidgets.map((lang) => FormBuilderFieldOption(
                            value: lang,
                            child: Text(lang),
                          )).toList(growable: false),
                          controlAffinity: ControlAffinity.trailing,
                          validator: FormBuilderValidators.compose([FormBuilderValidators.required(errorText: 'Please, select !')]),
                        ),
                      ),
                    ],
                  ),
                )
            )
        );
      }
      break;
      default: {
        //children.add(QuestionTextField(item['content'][i]['label']));
        children.add(
            FormBuilderTextField(
              decoration: InputDecoration(labelText: label),
              name: key,
              onChanged: (val) {
                item['contents'][i]['answer'] = val;
              },
            )
        );
      }
    }
  }

  children.add(const SizedBox(height: 20,));

  return Column(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.start, children: children);

}


class ConditionalQuestionAndImageSelection extends StatefulWidget {
  final dynamic item;
  final Function(String?) onPressedYesNo;
  final Function(String?) onChangeSelections;

  const ConditionalQuestionAndImageSelection({Key? key, this.item, required this.onPressedYesNo, required this.onChangeSelections}) : super(key: key);

  @override
  State<ConditionalQuestionAndImageSelection> createState() => _ConditionalQuestionAndImageSelectionState();
}
class _ConditionalQuestionAndImageSelectionState extends State<ConditionalQuestionAndImageSelection> {
  var type ;
  var options;
  var key ;
  var label;
  var hints ;
  var answer ;
  var secondAnswer ;

  var isPressedYes = false;

  List<String> optionWidgets = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();


    var contents = widget.item['contents'];
    type = contents[0]['type'];
    options = contents[0]['options'];
    key = contents[0]['key'];
    label = contents[0]['label'];
    hints = contents[0]['hints'];
    answer = contents[0]['answer'];
    secondAnswer = contents[0]['second_answer'];


    options.forEach((e) => optionWidgets.add(e));

    if(answer == 'Yes') {
      isPressedYes = true;
    } else if(answer == 'No') {
      isPressedYes = false;
    }

  }

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Padding(
        padding: const EdgeInsets.only(top:5, bottom: 0),
        child: FormBuilderChoiceChip<String>(
          initialValue: answer,
          decoration: const InputDecoration(enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.red, width: 1),
          )),
          alignment: WrapAlignment.spaceEvenly,
          autovalidateMode: AutovalidateMode.onUserInteraction,
          name: key,
          selectedColor: Colors.pinkAccent,
          options: const [
            FormBuilderChipOption(
              value: 'Yes',
              avatar: CircleAvatar(backgroundColor: Colors.white,child: Icon(Icons.check, color: Colors.green,),),
              child: Text('Yes', style: TextStyle(color: Colors.white))
            ),
            FormBuilderChipOption(
              value: 'No',
              avatar: CircleAvatar(backgroundColor: Colors.white,child: Icon(Icons.close, color: Colors.red,),),
              child: Text('No', style: TextStyle(color: Colors.white))
            ),
          ],
          onChanged: (val) {
            setState(() {
              isPressedYes = (val == 'Yes');
              widget.onPressedYesNo(val);
              widget.onChangeSelections(null);
            });
          },
          validator: FormBuilderValidators.compose([FormBuilderValidators.required(errorText: 'Please, select !')]),
        ),
      ),
      Visibility(visible: isPressedYes, child: QuestionTextField(label)),
      IgnorePointer(
        ignoring: !isPressedYes,
        child: Container(
          alignment: const Alignment(0, 0),
          child: Column(
            children: [
              Container(
                color: Colors.white,
                child: FormBuilderRadioGroup<String>(
                  initialValue: secondAnswer,
                  wrapAlignment: WrapAlignment.spaceBetween,
                  decoration: const InputDecoration(enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.red, width: 1),
                  )),
                  name: key+'_select',
                  onChanged: (val) {
                    widget.onChangeSelections(val);
                  },
                  options: optionWidgets.map((eachItem) => FormBuilderFieldOption(
                    value: eachItem,
                    child: Column(
                      children: [
                        Card(
                          color: Colors.pink[800],
                          child: Container(
                              width: Get.width/5,
                              height: Get.width/5,
                              child: Image.asset('assets/images/alzheimer/'+eachItem.toLowerCase()+'.png',)
                          ),
                        )
                      ],
                    ),
                  )).toList(growable: false),
                  controlAffinity: ControlAffinity.trailing,
                  validator: isPressedYes ? FormBuilderValidators.compose([FormBuilderValidators.required(errorText: 'Please, select !')]): FormBuilderValidators.compose([]),
                ),
              ),
            ],
          ),
        ),
      )
    ]);
  }
}



class ConditionalQuestionAndImageMultipleSelection extends StatefulWidget {
  final dynamic item;
  final Function(String?) onPressedYesNo;
  final Function(List<String>?) onChangeSelections;

  const ConditionalQuestionAndImageMultipleSelection({Key? key, this.item, required this.onPressedYesNo, required this.onChangeSelections}) : super(key: key);

  @override
  State<ConditionalQuestionAndImageMultipleSelection> createState() => _ConditionalQuestionAndImageMultipleSelectionState();
}
class _ConditionalQuestionAndImageMultipleSelectionState extends State<ConditionalQuestionAndImageMultipleSelection> {
  var type ;
  var options;
  var key ;
  var label;
  var hints ;
  var answer ;
  var secondAnswer ;

  var isPressedYes = false;

  List<String> optionWidgets = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();


    var contents = widget.item['contents'];
    type = contents[0]['type'];
    options = contents[0]['options'];
    key = contents[0]['key'];
    label = contents[0]['label'];
    hints = contents[0]['hints'];
    answer = contents[0]['answer'];
    secondAnswer = contents[0]['second_answer'];


    options.forEach((e) => optionWidgets.add(e));

    if(answer == 'Yes') {
      isPressedYes = true;
    } else if(answer == 'No') {
      isPressedYes = false;
    }

  }

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Padding(
        padding: const EdgeInsets.only(top:5, bottom: 0),
        child: FormBuilderChoiceChip<String>(
          initialValue: answer,
          decoration: const InputDecoration(enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.red, width: 1),
          )),
          alignment: WrapAlignment.spaceEvenly,
          autovalidateMode: AutovalidateMode.onUserInteraction,
          name: key,
          selectedColor: Colors.pinkAccent,
          options: const [
            FormBuilderChipOption(
              value: 'Yes',
              avatar: CircleAvatar(backgroundColor: Colors.white,child: Icon(Icons.check, color: Colors.pinkAccent,),),
            ),
            FormBuilderChipOption(
              value: 'No',
              avatar: CircleAvatar(backgroundColor: Colors.white,child: Icon(Icons.close, color: Color(0xffdcdcdc),),),
            ),
          ],
          onChanged: (val) {
            setState(() {
              isPressedYes = (val == 'Yes');
              widget.onPressedYesNo(val);
            });
          },
          validator: FormBuilderValidators.compose([FormBuilderValidators.required(errorText: 'Please, select !')]),
        ),
      ),
      Visibility(visible: isPressedYes, child: QuestionTextField(label)),
      IgnorePointer(
        ignoring: !isPressedYes,
        child: Container(
          alignment: const Alignment(0, 0),
          child: Column(
            children: [
              Container(
                color: Colors.white,
                child: FormBuilderCheckboxGroup<String>(
                  initialValue: secondAnswer,
                  wrapAlignment: WrapAlignment.spaceBetween,
                  decoration: const InputDecoration(enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.red, width: 1),
                  )),
                  name: key+'_select',
                  onChanged: (val) {
                    widget.onChangeSelections(val);
                  },
                  options: optionWidgets.map((eachItem) => FormBuilderFieldOption(
                    value: eachItem,
                    child: Column(
                      children: [
                        Card(
                          color: Colors.pink[800],
                          child: Container(
                              width: Get.width/5,
                              height: Get.width/5,
                              child: Image.asset('assets/images/alzheimer/'+eachItem.toLowerCase()+'.png',)
                          ),
                        )
                      ],
                    ),
                  )).toList(growable: false),
                  controlAffinity: ControlAffinity.trailing,
                  validator: isPressedYes == true ? FormBuilderValidators.compose([FormBuilderValidators.required(errorText: 'Please, select !')]) : FormBuilderValidators.compose([]),
                ),
              ),
            ],
          ),
        ),
      )
    ]);
  }
}



class ConditionalQuestionForEducation extends StatefulWidget {
  final dynamic item;
  final Function(String?) onPressedYesNo;
  final Function(String?) onChangeSelection;

  const ConditionalQuestionForEducation({Key? key, this.item, required this.onPressedYesNo, required this.onChangeSelection}) : super(key: key);

  @override
  State<ConditionalQuestionForEducation> createState() => _ConditionalQuestionForEducationState();
}
class _ConditionalQuestionForEducationState extends State<ConditionalQuestionForEducation> {
  var type ;
  var options;
  var key ;
  var label;
  var hints ;
  var answer ;
  var secondAnswer ;

  var isPressedYes;

  List<FormBuilderChipOption<String>> optionWidgets = [];


  @override
  void initState() {
    // TODO: implement initState
    super.initState();


    var contents = widget.item['contents'];
    type = contents[0]['type'];
    options = contents[0]['options'];
    key = contents[0]['key'];
    label = contents[0]['label'];
    hints = contents[0]['hints'];
    answer = contents[0]['answer'];
    secondAnswer = contents[0]['second_answer'];

    options.forEach((e) => optionWidgets.add(FormBuilderChipOption(
      value: e.toString(),
      avatar: const CircleAvatar(backgroundColor: Colors.white,child: Icon(Icons.adjust_rounded, color:  Color(0xffdcdcdc),),),
    )));

    if(answer == 'Yes') {
      isPressedYes = true;
    } else if(answer == 'No') {
      isPressedYes = false;
    }


  }

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Padding(
        padding: const EdgeInsets.only(top:5, bottom: 0),
        child: FormBuilderChoiceChip<String>(
          initialValue: answer,
          decoration: const InputDecoration(enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.red, width: 1),
          )),
          alignment: WrapAlignment.spaceEvenly,
          autovalidateMode: AutovalidateMode.onUserInteraction,
          name: key,
          selectedColor: Colors.pinkAccent,
          options: const [
            FormBuilderChipOption(
              value: 'Yes',
              avatar: CircleAvatar(backgroundColor: Colors.white,child: Icon(Icons.check, color: Colors.pinkAccent,),),
              child: Text('Yes', style: TextStyle(color: Colors.white))
            ),
            FormBuilderChipOption(
              value: 'No',
              avatar: CircleAvatar(backgroundColor: Colors.white,child: Icon(Icons.close, color: Color(0xffdcdcdc),),),
              child: Text('No', style: TextStyle(color: Colors.white))
            ),
          ],
          onChanged: (val) {
            setState(() {
              isPressedYes = (val == 'Yes');
              widget.onPressedYesNo(val);
            });
          },
          validator: FormBuilderValidators.compose([FormBuilderValidators.required(errorText: 'Please, select !')]),
        ),
      ),

      const SizedBox(height: 10,),
      Visibility(visible: (isPressedYes == true), child: QuestionTextField("I am going to give you a letter alphabet, I would like you to generate as much as word you can beginning with that letter in but not name of people or places.")),
      Visibility(visible: (isPressedYes == true), child:const SizedBox(height: 20,)),
      Visibility(visible: (isPressedYes == true), child: SimpleButton(hints, padding: const EdgeInsets.all(20), style: const TextStyle(color: Colors.white, fontSize: 22))),
      Visibility(visible: (isPressedYes == false), child: QuestionTextField("Can you name as many fruits and vegetables as possible?")),
      const SizedBox(height: 10,),
      Visibility(
        visible: (isPressedYes == true || isPressedYes == false),
        child: Container(
          alignment: const Alignment(0, 0),
          child: Column(
            children: [
              Container(
                color: Colors.pink[50],
                child: Padding(
                  padding: const EdgeInsets.only(top:5, bottom: 30),
                  child: FormBuilderChoiceChip<String>(
                    initialValue: secondAnswer,
                    selectedColor: Colors.pink,
                    labelPadding: const EdgeInsets.fromLTRB(10, 2, 10, 4),
                    shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(5))),
                    decoration: const InputDecoration(enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.red, width: 1),
                    )),
                    alignment: WrapAlignment.spaceBetween,
                    name: key+'_select',
                    labelStyle: const TextStyle(fontSize: 18, color: Colors.white),
                    options:optionWidgets,
                    onChanged: (val) {
                      widget.onChangeSelection(val);
                    },
                    validator: isPressedYes == true ? FormBuilderValidators.compose([FormBuilderValidators.required(errorText: 'Please, select !')]) : FormBuilderValidators.compose([]),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    ]);
  }
}


Future<void> showYesNoPopup(context, {title, subTitle, onPressedYes, onPressedNo}) async{
  return await showDialog(
    context: context,
    barrierDismissible: true,
    builder: (BuildContext context) {
      return AlertDialog(
        title: title == null ? null : Text(title),
        content: Container(
          child: SingleChildScrollView(
            child: ListBody(
              children: [
                Text(subTitle??''),
              ],
            ),
          ),
        ),
        actions: <Widget>[
          OutlinedButton(
            child: Text('no'.tr),
            onPressed: () {
              onPressedNo();
            },
          ),
          OutlinedButton(
            child: Text('az_yes'.tr),
            onPressed: () {
              onPressedYes();
            },
          ),
        ],
      );
    },
  );
}

InputSelect1(InputData input) {
  var label = input.label;
  if(label != null && input.isRequired) label = '$label *';
  var selectedItem;
  var selectedItemId;
  if(input.value != null) {
    try{
      selectedItem = input.options!.where((element) => element.id == input.value).first;
      selectedItemId = selectedItem.id;
    } catch(e){
      selectedItem = null;
      selectedItemId = null;
    }
  }
  return ListBody(
    children: [

      if(input.label != null) QuestionTextField(label!),
      FormBuilderField<int>(
          name: input.key,
          initialValue: selectedItemId,
          //onChanged: (val) => debugPrint(val.toString()),
          builder: (FormFieldState field) {
            return ListBody(
              children: [
                Container(
                  margin: const EdgeInsets.only(bottom: 5, top: 5),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.white,
                    border: Border.all(
                      color: Colors.red
                    ),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: DropdownSearch<dynamic>(
                      dropdownDecoratorProps: DropDownDecoratorProps(
                        dropdownSearchDecoration: InputDecoration(hintText: input.hint, border: InputBorder.none),
                      ),
                      popupProps: (input.type == InputData.selectSearch) ? const PopupProps.dialog(showSearchBox: true) : const PopupProps.menu(),
                      items: input.options!,
                      itemAsString: (dynamic item) => Get.locale.toString() == 'bn_BD' ? item.nameBn??item.name : item.name,
                      selectedItem: selectedItem,
                      dropdownBuilder: (BuildContext context, dynamic item) => field.value == null ? Text(input.hint) : Get.locale.toString() == 'bn_BD' ? Text(item.nameBn??item.name) : Text(item.name) ,
                      //onChanged: (dynamic data) =>  controller.selectEvent(input, data),//_onSelectedReligion(value),
                    ),
                  ),
                ),
                if(field.errorText != null && input.options!.isNotEmpty) Text(field.errorText!, style: const TextStyle(color: Colors.red, fontSize: 12),),
                const SizedBox(height: 20,),
              ],
            );
          },
          autovalidateMode: AutovalidateMode.onUserInteraction,
          validator: (input.isRequired) ? FormBuilderValidators.compose([
            FormBuilderValidators.required(),
          ]) : null
      )
    ]
  );
}