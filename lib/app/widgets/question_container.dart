import 'package:alzheimer_flutter/app/widgets/question_item.dart';
import 'package:flutter/material.dart';

import '../dto/screening/screening_page.dart';
import 'common_widget.dart';

buildEachQuestionContainer(ScreeningPage screeningPage, context, form) {
  List<Widget> child = [];
  child.add(QuestionPageTitle(screeningPage.pageTitle!));
  child.add(buildEachQuestion(screeningPage.inputContents!, context, form));
  child.add(const SizedBox(height: 20,));
  return Column(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.start, children: child);
}
