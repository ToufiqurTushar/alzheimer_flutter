
import 'package:flutter/material.dart';

import '../../core/dto/input_data.dart';
import '../dto/screening/input_content.dart';
import '../enum/Input_type_enum.dart';
import 'form.dart';

buildEachQuestion(List<InputContent> inputContents, context, form) {
  List<Widget> child = [];
  for(var i=0; i < inputContents.length; i++) {
    final question = inputContents[i];
    switch(inputContents[i].type) {
      case InputTypeEnum.DATE:
        child.add(
            InputDate(InputData(key: question.key!, value: question.answer, label: question.label, hint: question.hint), form)
        );
        break;
      case InputTypeEnum.SELECT:
        child.add(
            InputSelect(InputData(key: question.key!, value: question.answer, label: question.label, hint: question.hint, options: question.options))
        );
        break;
      case InputTypeEnum.SELECT_SEARCH:
        child.add(
            InputSelectSearch(InputData(key: question.key!, value: question.answer, label: question.label, hint: question.hint, options: question.options, type: InputData.selectSearch), form)
        );
        break;
      case InputTypeEnum.YEAR:
        child.add(
            InputYear(InputData(key: question.key!, value: question.answer, label: question.label, hint: question.hint, options: question.options, type: InputData.selectSearch), form)
        );
        break;
      case InputTypeEnum.SINGLE_SELECT:
        child.add(
            InputSingleSelect(InputData(key: question.key!, value: question.answer, label: question.label, hint: question.hint, options: question.options, isReadOnly: question.readOnly, isRequired: !question.readOnly,))
        );
        break;
      case InputTypeEnum.MULTIPLE_SELECT:
        child.add(
            IgnorePointer(
                ignoring: question.readOnly,
                child: InputMultipleSelect(InputData(key: question.key!, value: question.answer, label: question.label, hint: question.hint, options: question.options, isReadOnly: question.readOnly, isRequired: !question.readOnly,))
            )
        );
        break;
      case InputTypeEnum.TRUE_FALSE:
        if(!question.hidden) {
          child.add(
              InputTrueFalse(InputData(key: question.key!, value: question.answer, label: question.label, hint: question.hint, options: question.options, labelAtCenter: question.labelAtCenter))
          );
        }
        break;
      case InputTypeEnum.TRUE_FALSE_WITH_FIGURE:
        child.add(
            InputTrueFalseWithFigure(InputData(key: question.key!, value: question.answer, figure: question.figure!, hint: question.hint, options: question.options,))
        );
        break;
      case InputTypeEnum.OTP:
        child.add(
            IgnorePointer(
                ignoring: question.readOnly,
                child: InputOtp(InputData(key: question.key!, value: question.answer, label: question.label, hint: question.hint), context, form)
            )
        );
        break;
      case InputTypeEnum.RADIO_WITH_FIGURE:
        if(!question.hidden) {
          child.add(
              InputRadioWithFigure(InputData(key: question.key!, value: question.answer, figure: question.figure, hint: question.hint, options: question.options))
          );
        }
        break;
      case InputTypeEnum.SELECT_WITH_FIGURE:
        if(!question.hidden) {
          child.add(
              InputSelectWithFigure(InputData(key: question.key!, value: question.answer, figure: question.figure, hint: question.hint, options: question.options), form,)
          );
        }
        break;
      case InputTypeEnum.TRUE_FALSE_WITH_SELECT_IMAGE:
        child.add(
            InputTrueFalseWithSelectImage(
                InputData(key: question.key!, value: question.answer, label: question.label, hint: question.hint, options: question.options),
                InputData(key: question.child!.key!, value: question.child!.answer, label: question.child!.label, options: question.child!.options, isVisible: !question.child!.hidden),
                form
            )
        );
        break;
      case InputTypeEnum.TRUE_FALSE_WITH_SELECT_FOR_TRUE_AND_SELECT_FOR_FALSE:
          child.add(
              InputTrueFalseWithSelectForTrueAndSelectForFalse(
                  InputData(key: question.key!, value: question.answer, label: question.label, hint: question.hint, options: question.options),
                  InputData(key: question.child!.key!, value: question.child!.answer, label: question.child!.label, figure: question.child!.figure, options: question.child!.options, isVisible: !question.child!.hidden),
                  InputData(key: question.secondChild!.key!, value: question.secondChild!.answer, label: question.secondChild!.label, options: question.secondChild!.options, isVisible: !question.secondChild!.hidden),
                  form
              )
          );
        break;
      case InputTypeEnum.TRUE_FALSE_WITH_FIGURE_TAKE_PICTURE:
        child.add(
            InputTrueFalseWithFigureAndTakePicture(InputData(key: question.key!, value: question.answer, figure: question.figure!, hint: question.hint, options: question.options,), form)
        );
        break;

      default: Container();
    }
  }
  return Column(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.start, children: child);
}