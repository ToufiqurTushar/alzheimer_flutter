import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:form_builder_validators/localization/l10n.dart';
import 'package:get/get.dart';
import 'app/localization/localization.dart';
import 'app/routes/app_pages.dart';

void main() {
  runApp(AlzheimerApp());
}

class AlzheimerApp extends StatelessWidget {

  const AlzheimerApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: "Alzheimer",
      initialRoute: AppPages.INITIAL,
      getPages: AppPages.routes,

      debugShowCheckedModeBanner: false,
      locale: const Locale('bn', 'BD'),
      fallbackLocale: const Locale('en', 'US'),
      translations: Localization(),
      theme: ThemeData(
        primarySwatch: Colors.pink,
        scaffoldBackgroundColor: const Color(0xFFf5eaf0),
        appBarTheme: const AppBarTheme(
          iconTheme: IconThemeData(color: Colors.white),
          foregroundColor: Colors.white,
        ),
      ),
      localizationsDelegates: const [
        FormBuilderLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      defaultTransition: Transition.native,
    );
  }
}
